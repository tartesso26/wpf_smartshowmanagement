﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalConfiguracionesCorreo
    {
        public int Id { get; set; }
        public int? IdConfiguracionCorreo { get; set; }
        public string Direccion { get; set; }
        public string ServidorSmtp { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public short? Puerto { get; set; }
        public bool? EnabledSsl { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
