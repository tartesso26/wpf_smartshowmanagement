﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class SuplementosButaca
    {
        public int IdSuplementoButaca { get; set; }
        public int? IdZonaSala { get; set; }
        public short? FilaMatriz { get; set; }
        public short? ColumnaMatriz { get; set; }
        public int? IdSuplemento { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
