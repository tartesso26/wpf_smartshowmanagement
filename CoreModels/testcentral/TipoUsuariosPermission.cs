﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class TipoUsuariosPermission
    {
        public int? IdTipoUsuario { get; set; }
        public int? IdPermission { get; set; }
        public int Id { get; set; }
    }
}
