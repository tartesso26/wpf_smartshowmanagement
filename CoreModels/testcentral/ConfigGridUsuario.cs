﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class ConfigGridUsuario
    {
        public int IdConfigGridUsuarios { get; set; }
        public int? IdUsuario { get; set; }
        public string Formulario { get; set; }
        public string Campo { get; set; }
        public double? Ancho { get; set; }
        public string Encabezado { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
