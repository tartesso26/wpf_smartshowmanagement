﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalPreciosTemporada
    {
        public int Id { get; set; }
        public int? IdPrecioTemporada { get; set; }
        public int? IdTemporada { get; set; }
        public int? IdPrecio { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
