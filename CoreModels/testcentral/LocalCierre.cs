﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalCierre
    {
        public int Id { get; set; }
        public int? IdCierre { get; set; }
        public int? IdCentro { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdPuntoVenta { get; set; }
        public DateTime? FechaHoraCierre { get; set; }
        public DateTime? FechaEfectiva { get; set; }
        public decimal? ImporteRecaudado { get; set; }
        public decimal? ImporteVendido { get; set; }
        public decimal? ImporteMetalico { get; set; }
        public decimal? ImporteTarjeta { get; set; }
        public decimal? ImportePromocional { get; set; }
        public decimal? Descuadre { get; set; }
        public decimal? ImporteMetalicoTaquilla { get; set; }
        public decimal? ImporteMetalicoBarra { get; set; }
        public decimal? ImporteTarjetaTaquilla { get; set; }
        public decimal? ImporteTarjetaBarra { get; set; }
        public decimal? ImportePromocionalTaquilla { get; set; }
        public decimal? ImportePromocionalBarra { get; set; }
        public short? NumeroVentas { get; set; }
        public string FechaActualizacion { get; set; }
        public decimal? Retirada { get; set; }
        public decimal? Remanente { get; set; }
        public short? Final { get; set; }
        public int? Billetes500 { get; set; }
        public int? Billetes200 { get; set; }
        public int? Billetes100 { get; set; }
        public int? Billetes50 { get; set; }
        public int? Billetes20 { get; set; }
        public int? Billetes10 { get; set; }
        public int? Billetes5 { get; set; }
        public decimal? TotalBilletes { get; set; }
        public int? Monedas2 { get; set; }
        public int? Monedas1 { get; set; }
        public int? Monedas050 { get; set; }
        public int? Monedas020 { get; set; }
        public int? Monedas010 { get; set; }
        public int? Monedas005 { get; set; }
        public int? Monedas002 { get; set; }
        public int? Monedas001 { get; set; }
        public decimal? TotalMonedas { get; set; }
        public string Observaciones { get; set; }
        public decimal? ImporteEntradaEfectivo { get; set; }
        public decimal? ImporteSalidaEfectivo { get; set; }
        public decimal? ImporteDevoluciones { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
