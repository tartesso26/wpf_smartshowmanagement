﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class ArticulosLinea
    {
        public int IdArticuloLinea { get; set; }
        public int? IdArticuloPadre { get; set; }
        public int? IdArticulo { get; set; }
        public decimal? Precio { get; set; }
        public int? IdImpuesto { get; set; }
        public bool? Seleccionable { get; set; }
        public bool? MostrarEnTicket { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public decimal? Total { get; set; }
    }
}
