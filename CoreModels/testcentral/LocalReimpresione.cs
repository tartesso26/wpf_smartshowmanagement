﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalReimpresione
    {
        public int Id { get; set; }
        public int? IdReimpresion { get; set; }
        public int? IdVenta { get; set; }
        public int? IdUsuarioReimpresion { get; set; }
        public DateTime? FechaHoraReimpresion { get; set; }
        public string Observaciones { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
