﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FormatosVersionAudio
    {
        public int IdVersionAudio { get; set; }
        public string NombreVersionAudio { get; set; }
        public string LetraVersionAudio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
