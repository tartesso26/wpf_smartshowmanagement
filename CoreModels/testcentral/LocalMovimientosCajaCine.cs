﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalMovimientosCajaCine
    {
        public int Id { get; set; }
        public int? IdMovimientoCajaCine { get; set; }
        public bool? Ingreso { get; set; }
        public string Descripcion { get; set; }
        public decimal? Importe { get; set; }
        public DateTime? FechaHoraMovimiento { get; set; }
        public DateTime? FechaValidez { get; set; }
        public int? IdUsuario { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdCierre { get; set; }
        public int? IdCentro { get; set; }
        public int? IdPuntoVenta { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
