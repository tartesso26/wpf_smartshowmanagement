﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class PromocionesCine
    {
        public int IdPromocionCine { get; set; }
        public int? IdPromocion { get; set; }
        public int? IdCentro { get; set; }
        public decimal? DtoPorcentaje { get; set; }
        public decimal? DtoPrecioFijo { get; set; }
        public decimal? DtoEuros { get; set; }
        public decimal? ValorContable { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public decimal? PrecioArticulo { get; set; }
        public int? IdImpuesto { get; set; }
        public bool? CentroExcluido { get; set; }
    }
}
