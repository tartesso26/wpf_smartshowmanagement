﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalMacrosArticulo
    {
        public int Id { get; set; }
        public int? IdMacroArticulo { get; set; }
        public int? IdMacro { get; set; }
        public int? IdArticulo { get; set; }
        public decimal? Precio { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
