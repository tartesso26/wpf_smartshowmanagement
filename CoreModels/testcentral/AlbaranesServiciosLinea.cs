﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class AlbaranesServiciosLinea
    {
        public int IdAlbaranServicioLinea { get; set; }
        public int? IdAlbaran { get; set; }
        public int? IdArticulo { get; set; }
        public string Descripcion { get; set; }
        public short? Unidades { get; set; }
        public decimal? Descuento { get; set; }
        public decimal? DtoPorcentaje { get; set; }
        public decimal? ImpuestosPorcentaje { get; set; }
        public decimal? Base { get; set; }
        public int? IdImpuesto { get; set; }
        public decimal? PuntoVerde { get; set; }
        public decimal? Total { get; set; }
        public int? IdMovimientoAlmacen { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public decimal? PorcentajeDescuentoCompra { get; set; }
        public decimal? PorcentajeBonificado { get; set; }
        public decimal? Impuestos { get; set; }
    }
}
