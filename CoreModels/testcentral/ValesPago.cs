﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class ValesPago
    {
        public int IdValePago { get; set; }
        public int? IdVenta { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? FechaVale { get; set; }
        public decimal? Importe { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string CodigoBarras { get; set; }
        public int? NumeroVale { get; set; }
        public int? IdCentro { get; set; }
        public byte? Usado { get; set; }
        public DateTime? FechaUsado { get; set; }
        public int? IdPuntoVenta { get; set; }
        public int? IdCliente { get; set; }
        public string CodigoValeAntiguo { get; set; }
        public DateTime? FechaValidez { get; set; }
        public int? IdPromocion { get; set; }
    }
}
