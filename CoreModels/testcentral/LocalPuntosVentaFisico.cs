﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalPuntosVentaFisico
    {
        public int Id { get; set; }
        public int? IdPuntoVentaFisico { get; set; }
        public string NombreFisico { get; set; }
        public bool? MostrarSeleccionOtros { get; set; }
        public bool? ReproducirVoces { get; set; }
        public bool? UsarVisor { get; set; }
        public string VisorPuerto { get; set; }
        public string VisorInicializar { get; set; }
        public string VisorLineaSuperior { get; set; }
        public string VisorLineaInferior { get; set; }
        public string VisorFinLinea { get; set; }
        public string PuertoImpresora { get; set; }
        public byte? FormatoImpresion { get; set; }
        public bool? UsarCobradorBanco { get; set; }
        public string PinpadNumeroSerie { get; set; }
        public string PinpadNombreBanco { get; set; }
        public string PinpadCodComercio { get; set; }
        public string PinpadCodTerminal { get; set; }
        public string PinpadClaveFirma { get; set; }
        public string PinpadConfigPuerto { get; set; }
        public string PinpadVersion { get; set; }
        public bool? SistemaTactil { get; set; }
        public bool? PermitirAbrirCajon { get; set; }
        public string ComandoAperturaCajon { get; set; }
        public string PuertoSerieCajon { get; set; }
        public string ComandoGuillotina { get; set; }
        public string TipoBalanza { get; set; }
        public string Mac { get; set; }
        public string IdTeamviewer { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string DireccionRedImpresora { get; set; }
        public string PrinterTickets { get; set; }
        public string PrinterLabel { get; set; }
        public bool? UsarSegundaPantalla { get; set; }
        public string BalanzaConexion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
