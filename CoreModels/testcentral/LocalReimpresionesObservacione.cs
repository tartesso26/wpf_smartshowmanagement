﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalReimpresionesObservacione
    {
        public int Id { get; set; }
        public int? IdReimpresionObservacion { get; set; }
        public string Texto { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
