﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalFamilia
    {
        public int Id { get; set; }
        public short? IdFamilia { get; set; }
        public string NombreFamilia { get; set; }
        public string NombreFamiliaAbreviado { get; set; }
        public string Imagen { get; set; }
        public bool? MostrarEnPos { get; set; }
        public byte? OrdenEnPos { get; set; }
        public bool? FamiliaWeb { get; set; }
        public bool? FamiliaKiosko { get; set; }
        public bool? FamiliaConDiseno { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
