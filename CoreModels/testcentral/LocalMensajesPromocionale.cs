﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalMensajesPromocionale
    {
        public int Id { get; set; }
        public int? IdMensajesPromocionales { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public string Nombre { get; set; }
        public bool? Taquilla { get; set; }
        public bool? Barra { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string Texto { get; set; }
        public string Imagen { get; set; }
        public bool? Multiple { get; set; }
        public bool? Vale { get; set; }
        public int? IdPromocion { get; set; }
        public short? DiasCaducidad { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
