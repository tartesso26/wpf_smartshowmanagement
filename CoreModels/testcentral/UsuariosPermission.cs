﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class UsuariosPermission
    {
        public int? PermissionId { get; set; }
        public int? IdUsuario { get; set; }
        public int Id { get; set; }
    }
}
