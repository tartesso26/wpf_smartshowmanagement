﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalFestivo
    {
        public int Id { get; set; }
        public int? IdFestivo { get; set; }
        public DateTime? FechaFestivo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
