﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class TarjetasPrepagoOperacione
    {
        public int IdTarjetaPrepagoOperacion { get; set; }
        public int? IdTarjetaPrepago { get; set; }
        public int? IdCentro { get; set; }
        public DateTime? FechaHoraOperacion { get; set; }
        public short? TotalEntradas { get; set; }
        public decimal? PrecioEntradas { get; set; }
        public byte? CanalUso { get; set; }
        public int? IdVenta { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
