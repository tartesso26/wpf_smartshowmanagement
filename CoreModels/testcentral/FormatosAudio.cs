﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FormatosAudio
    {
        public int IdFormatoAudio { get; set; }
        public string NombreFormatoAudio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
