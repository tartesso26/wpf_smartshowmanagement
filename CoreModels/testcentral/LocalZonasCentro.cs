﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalZonasCentro
    {
        public int Id { get; set; }
        public int? IdZonaCentro { get; set; }
        public int? IdCentro { get; set; }
        public string Nombre { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
