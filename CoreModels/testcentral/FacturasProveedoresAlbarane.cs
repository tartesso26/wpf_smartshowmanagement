﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FacturasProveedoresAlbarane
    {
        public int IdFacturaProveedorAlbaran { get; set; }
        public int? IdFactura { get; set; }
        public int? IdAlbaran { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
