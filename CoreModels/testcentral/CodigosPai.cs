﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class CodigosPai
    {
        public int IdCodigoPais { get; set; }
        public string NombrePais { get; set; }
        public string Zona { get; set; }
        public string Codigo { get; set; }
    }
}
