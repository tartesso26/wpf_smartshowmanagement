﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class ArticulosEscandallo
    {
        public int IdArticuloEscandallo { get; set; }
        public int? IdArticuloPadre { get; set; }
        public decimal? Cantidad { get; set; }
        public int? IdArticuloDescontar { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
