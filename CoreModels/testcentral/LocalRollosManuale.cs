﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalRollosManuale
    {
        public int Id { get; set; }
        public short? IdRolloManual { get; set; }
        public string Descripcion { get; set; }
        public int? NumeroActual { get; set; }
        public int? NumeroInicial { get; set; }
        public int? NumeroFinal { get; set; }
        public int? IdSala { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
