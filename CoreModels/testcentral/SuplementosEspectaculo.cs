﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class SuplementosEspectaculo
    {
        public int IdSuplementoEspectaculo { get; set; }
        public int? IdSuplemento { get; set; }
        public int? IdEspectaculo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
