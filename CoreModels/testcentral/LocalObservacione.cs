﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalObservacione
    {
        public int Id { get; set; }
        public int? IdObservacion { get; set; }
        public string Detalle { get; set; }
        public DateTime? FechaHora { get; set; }
        public DateTime? DiaObservacion { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
