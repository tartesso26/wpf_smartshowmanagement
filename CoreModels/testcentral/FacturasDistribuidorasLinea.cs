﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FacturasDistribuidorasLinea
    {
        public int IdFacturaDistribuidoraLinea { get; set; }
        public int? IdFacturaDistribuidora { get; set; }
        public byte? TipoLinea { get; set; }
        public decimal? Porcentaje { get; set; }
        public decimal? ImporteEspectador { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public int? Espectadores { get; set; }
        public decimal? RecaudacionBruta { get; set; }
        public decimal? RecaudacionNeta { get; set; }
        public decimal? PorcentajeImpuestos { get; set; }
        public decimal? Impuestos { get; set; }
        public decimal? TotalBase { get; set; }
        public decimal? TotalLinea { get; set; }
        public string Descripcion { get; set; }
        public string Descripcion2 { get; set; }
        public int? IdEspectaculo { get; set; }
        public int? IdFormato { get; set; }
        public int? IdTablaPorcentajeDistribuidora { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public decimal? PorcentajeImpuestosTickets { get; set; }
    }
}
