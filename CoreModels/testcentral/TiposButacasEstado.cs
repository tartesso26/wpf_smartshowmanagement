﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class TiposButacasEstado
    {
        public byte IdTipoButacaEstado { get; set; }
        public string NombreEstado { get; set; }
        public int? Estado { get; set; }
        public string IconoButaca { get; set; }
        public bool? TipoCambiable { get; set; }
        public bool? Vendible { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
