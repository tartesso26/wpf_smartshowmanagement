﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class SeriesTicket
    {
        public int IdSerieTicket { get; set; }
        public string SerieTaquilla { get; set; }
        public string SerieBarra { get; set; }
        public string SerieSuplementos { get; set; }
        public string SerieMermas { get; set; }
    }
}
