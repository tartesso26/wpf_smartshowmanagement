﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class EspectaculosMultiple
    {
        public int IdEspectaculoMultiple { get; set; }
        public int? IdEspectaculoPadre { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdEspectaculoHijo { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
