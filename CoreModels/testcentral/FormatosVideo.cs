﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FormatosVideo
    {
        public int IdFormatoVideo { get; set; }
        public string NombreFormatoVideo { get; set; }
        public string CaracterFormatoVideo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
