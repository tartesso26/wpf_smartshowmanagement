﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class SuplementosPrecio
    {
        public int IdSuplementoPrecio { get; set; }
        public int? IdPrecio { get; set; }
        public int? IdSuplemento { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
