﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalVentasManuale
    {
        public int Id { get; set; }
        public int? IdVentaManual { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdRolloManual { get; set; }
        public int? IdPase { get; set; }
        public decimal? Precio { get; set; }
        public DateTime? FechaHoraVenta { get; set; }
        public short? NumeroEntradas { get; set; }
        public short? NumeroInicio { get; set; }
        public short? NumeroFin { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
