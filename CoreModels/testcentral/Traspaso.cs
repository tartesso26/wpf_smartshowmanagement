﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class Traspaso
    {
        public int IdTraspasos { get; set; }
        public int? IdCentroOrigen { get; set; }
        public int? IdCentroDestino { get; set; }
        public string NumeroTraspaso { get; set; }
        public DateTime? FechaTraspaso { get; set; }
        public DateTime? FechaAceptado { get; set; }
        public string Observaciones { get; set; }
        public short? Aceptado { get; set; }
        public int? AceptadoPor { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string CodigoBarras { get; set; }
        public byte? Estado { get; set; }
    }
}
