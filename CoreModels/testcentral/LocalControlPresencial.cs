﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalControlPresencial
    {
        public int Id { get; set; }
        public int? IdControlPresencial { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? Inicio { get; set; }
        public short? MotivoInicio { get; set; }
        public DateTime? Final { get; set; }
        public short? MotivoFin { get; set; }
        public string FechaActualizacion { get; set; }
        public string Comentarios { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
