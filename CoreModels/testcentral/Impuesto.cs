﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class Impuesto
    {
        public int IdImpuesto { get; set; }
        public string NombreImpuesto1 { get; set; }
        public string Descripcion1 { get; set; }
        public decimal? Porcentaje1 { get; set; }
        public string NombreImpuesto2 { get; set; }
        public string Descripcion2 { get; set; }
        public decimal? Porcentaje2 { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
