﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class ClientesFactura
    {
        public int IdClienteFactura { get; set; }
        public string NombreCompleto { get; set; }
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Cp { get; set; }
        public string Mail { get; set; }
        public string Tlfno { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string Cif { get; set; }
    }
}
