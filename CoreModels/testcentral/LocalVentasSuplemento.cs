﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalVentasSuplemento
    {
        public int Id { get; set; }
        public int? IdVentaSuplemento { get; set; }
        public int? IdVenta { get; set; }
        public int? IdSuplemento { get; set; }
        public decimal? Precio { get; set; }
        public decimal? Impuestos { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
