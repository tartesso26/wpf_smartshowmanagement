﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class Sala
    {
        public int IdSala { get; set; }
        public string NombreSala { get; set; }
        public string SubNombreCentro { get; set; }
        public string CodigoSala { get; set; }
        public string CodigoIne { get; set; }
        public byte? Nplantas { get; set; }
        public short? Aforo { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
