﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class MyDbContext : DbContext
    {
        public MyDbContext()
        {
        }

        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Agenda> Agendas { get; set; }
        public virtual DbSet<AlbaranesPedido> AlbaranesPedidos { get; set; }
        public virtual DbSet<AlbaranesPedidosLinea> AlbaranesPedidosLineas { get; set; }
        public virtual DbSet<AlbaranesProveedore> AlbaranesProveedores { get; set; }
        public virtual DbSet<AlbaranesProveedoresLinea> AlbaranesProveedoresLineas { get; set; }
        public virtual DbSet<AlbaranesServicio> AlbaranesServicios { get; set; }
        public virtual DbSet<AlbaranesServiciosLinea> AlbaranesServiciosLineas { get; set; }
        public virtual DbSet<Articulo> Articulos { get; set; }
        public virtual DbSet<ArticulosCine> ArticulosCines { get; set; }
        public virtual DbSet<ArticulosEscandallo> ArticulosEscandallos { get; set; }
        public virtual DbSet<ArticulosInmobilizado> ArticulosInmobilizados { get; set; }
        public virtual DbSet<ArticulosLinea> ArticulosLineas { get; set; }
        public virtual DbSet<ArticulosSeries> ArticulosSeries { get; set; }
        public virtual DbSet<ButacasPlano> ButacasPlanos { get; set; }
        public virtual DbSet<Calificacione> Calificaciones { get; set; }
        public virtual DbSet<Centro> Centros { get; set; }
        public virtual DbSet<ClientesCadena> ClientesCadenas { get; set; }
        public virtual DbSet<ClientesFactura> ClientesFacturas { get; set; }
        public virtual DbSet<CodigosPai> CodigosPais { get; set; }
        public virtual DbSet<ConfigGridUsuario> ConfigGridUsuarios { get; set; }
        public virtual DbSet<ContratosDistribuidora> ContratosDistribuidoras { get; set; }
        public virtual DbSet<ContratosDistribuidorasLinea> ContratosDistribuidorasLineas { get; set; }
        public virtual DbSet<DatosCadena> DatosCadenas { get; set; }
        public virtual DbSet<Distribuidora> Distribuidoras { get; set; }
        public virtual DbSet<Espectaculo> Espectaculos { get; set; }
        public virtual DbSet<EspectaculosMultiple> EspectaculosMultiples { get; set; }
        public virtual DbSet<EtlconfigTableTransform> EtlconfigTableTransforms { get; set; }
        public virtual DbSet<EtlconfigTableTransformAlcala> EtlconfigTableTransformAlcalas { get; set; }
        public virtual DbSet<EtlconfigTableTransformAlfare> EtlconfigTableTransformAlfares { get; set; }
        public virtual DbSet<EtlconfigTableTransformTelde> EtlconfigTableTransformTeldes { get; set; }
        public virtual DbSet<FacturasCliente> FacturasClientes { get; set; }
        public virtual DbSet<FacturasClientesLinea> FacturasClientesLineas { get; set; }
        public virtual DbSet<FacturasDistribuidora> FacturasDistribuidoras { get; set; }
        public virtual DbSet<FacturasDistribuidorasLinea> FacturasDistribuidorasLineas { get; set; }
        public virtual DbSet<FacturasProveedore> FacturasProveedores { get; set; }
        public virtual DbSet<FacturasProveedoresAlbarane> FacturasProveedoresAlbaranes { get; set; }
        public virtual DbSet<FacturasProveedoresLinea> FacturasProveedoresLineas { get; set; }
        public virtual DbSet<FamiliaAnalisi> FamiliaAnalises { get; set; }
        public virtual DbSet<Formato> Formatos { get; set; }
        public virtual DbSet<Formatos3D> Formatos3Ds { get; set; }
        public virtual DbSet<FormatosAudio> FormatosAudios { get; set; }
        public virtual DbSet<FormatosSubtitulo> FormatosSubtitulos { get; set; }
        public virtual DbSet<FormatosVersionAudio> FormatosVersionAudios { get; set; }
        public virtual DbSet<FormatosVideo> FormatosVideos { get; set; }
        public virtual DbSet<Genero> Generos { get; set; }
        public virtual DbSet<Impuesto> Impuestos { get; set; }
        public virtual DbSet<LocalArticulosCinesStock> LocalArticulosCinesStocks { get; set; }
        public virtual DbSet<LocalCierre> LocalCierres { get; set; }
        public virtual DbSet<LocalConfiguracionesCorreo> LocalConfiguracionesCorreos { get; set; }
        public virtual DbSet<LocalControlPresencial> LocalControlPresencials { get; set; }
        public virtual DbSet<LocalErrore> LocalErrores { get; set; }
        public virtual DbSet<LocalFamilia> LocalFamilias { get; set; }
        public virtual DbSet<LocalFamiliasArticulo> LocalFamiliasArticulos { get; set; }
        public virtual DbSet<LocalFestivo> LocalFestivos { get; set; }
        public virtual DbSet<LocalInventario> LocalInventarios { get; set; }
        public virtual DbSet<LocalInventariosLinea> LocalInventariosLineas { get; set; }
        public virtual DbSet<LocalMacro> LocalMacros { get; set; }
        public virtual DbSet<LocalMacrosArticulo> LocalMacrosArticulos { get; set; }
        public virtual DbSet<LocalMacrosPromo> LocalMacrosPromos { get; set; }
        public virtual DbSet<LocalMensajesPromocionale> LocalMensajesPromocionales { get; set; }
        public virtual DbSet<LocalMovimientosAlmacen> LocalMovimientosAlmacens { get; set; }
        public virtual DbSet<LocalMovimientosCajaCine> LocalMovimientosCajaCines { get; set; }
        public virtual DbSet<LocalMovimientosCajaPv> LocalMovimientosCajaPvs { get; set; }
        public virtual DbSet<LocalMovimientosPromosCine> LocalMovimientosPromosCines { get; set; }
        public virtual DbSet<LocalMovimientosTpv> LocalMovimientosTpvs { get; set; }
        public virtual DbSet<LocalObservacione> LocalObservaciones { get; set; }
        public virtual DbSet<LocalPase> LocalPases { get; set; }
        public virtual DbSet<LocalPreciosPasesZona> LocalPreciosPasesZonas { get; set; }
        public virtual DbSet<LocalPreciosTemporada> LocalPreciosTemporadas { get; set; }
        public virtual DbSet<LocalPuntosVentaFisico> LocalPuntosVentaFisicos { get; set; }
        public virtual DbSet<LocalPuntosVentaPinpad> LocalPuntosVentaPinpads { get; set; }
        public virtual DbSet<LocalReimpresione> LocalReimpresiones { get; set; }
        public virtual DbSet<LocalReimpresionesObservacione> LocalReimpresionesObservaciones { get; set; }
        public virtual DbSet<LocalReserva> LocalReservas { get; set; }
        public virtual DbSet<LocalReservasLinea> LocalReservasLineas { get; set; }
        public virtual DbSet<LocalReubicacione> LocalReubicaciones { get; set; }
        public virtual DbSet<LocalReubicacionesLinea> LocalReubicacionesLineas { get; set; }
        public virtual DbSet<LocalRollosManuale> LocalRollosManuales { get; set; }
        public virtual DbSet<LocalSocio> LocalSocios { get; set; }
        public virtual DbSet<LocalTemporada> LocalTemporadas { get; set; }
        public virtual DbSet<LocalTemporadasLinea> LocalTemporadasLineas { get; set; }
        public virtual DbSet<LocalTemporadasSocio> LocalTemporadasSocios { get; set; }
        public virtual DbSet<LocalVenta> LocalVentas { get; set; }
        public virtual DbSet<LocalVentasLinea> LocalVentasLineas { get; set; }
        public virtual DbSet<LocalVentasManuale> LocalVentasManuales { get; set; }
        public virtual DbSet<LocalVentasPago> LocalVentasPagos { get; set; }
        public virtual DbSet<LocalVentasSuplemento> LocalVentasSuplementos { get; set; }
        public virtual DbSet<LocalZonasCentro> LocalZonasCentros { get; set; }
        public virtual DbSet<LocalZonasCentrosPuntosVentum> LocalZonasCentrosPuntosVenta { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Precio> Precios { get; set; }
        public virtual DbSet<PreciosCine> PreciosCines { get; set; }
        public virtual DbSet<Promocione> Promociones { get; set; }
        public virtual DbSet<PromocionesCine> PromocionesCines { get; set; }
        public virtual DbSet<Proveedore> Proveedores { get; set; }
        public virtual DbSet<Sala> Salas { get; set; }
        public virtual DbSet<SalasZona> SalasZonas { get; set; }
        public virtual DbSet<SeriesTicket> SeriesTickets { get; set; }
        public virtual DbSet<Suplemento> Suplementos { get; set; }
        public virtual DbSet<SuplementosButaca> SuplementosButacas { get; set; }
        public virtual DbSet<SuplementosCine> SuplementosCines { get; set; }
        public virtual DbSet<SuplementosEspectaculo> SuplementosEspectaculos { get; set; }
        public virtual DbSet<SuplementosFormato> SuplementosFormatos { get; set; }
        public virtual DbSet<SuplementosPrecio> SuplementosPrecios { get; set; }
        public virtual DbSet<TablasPorcentajeDistribuidora> TablasPorcentajeDistribuidoras { get; set; }
        public virtual DbSet<TarjetasPrepago> TarjetasPrepagos { get; set; }
        public virtual DbSet<TarjetasPrepagoOperacione> TarjetasPrepagoOperaciones { get; set; }
        public virtual DbSet<TipoUsuario> TipoUsuarios { get; set; }
        public virtual DbSet<TipoUsuariosPermission> TipoUsuariosPermissions { get; set; }
        public virtual DbSet<TiposButaca> TiposButacas { get; set; }
        public virtual DbSet<TiposButacasEstado> TiposButacasEstados { get; set; }
        public virtual DbSet<Traspaso> Traspasos { get; set; }
        public virtual DbSet<TraspasosLinea> TraspasosLineas { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<UsuariosCentro> UsuariosCentros { get; set; }
        public virtual DbSet<UsuariosPermission> UsuariosPermissions { get; set; }
        public virtual DbSet<ValesPago> ValesPagos { get; set; }
        public virtual DbSet<VentasLineasTemp> VentasLineasTemps { get; set; }
        public virtual DbSet<VentasPagosTemp> VentasPagosTemps { get; set; }
        public virtual DbSet<VentasTemp> VentasTemps { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=82.223.82.121;Database=Central;User ID=sa;Password=G2st4r!0182;Initial Catalog=Centros;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agenda>(entity =>
            {
                entity.HasKey(e => e.IdAgenda)
                    .HasName("PK_Agenda");

                entity.HasComment("Datos de contacto para posibles envios de correo electrónico, el sistema podrá enviar mails directamente a las direcciones almacenadas aqui.");

                entity.Property(e => e.IdAgenda).HasColumnName("ID_Agenda");

                entity.Property(e => e.Empresa)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("company name");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Mail)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Email");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("name");

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("phone number");
            });

            modelBuilder.Entity<AlbaranesPedido>(entity =>
            {
                entity.HasKey(e => e.IdPedidoProveedor);

                entity.Property(e => e.IdPedidoProveedor).HasColumnName("ID_PedidoProveedor");

                entity.Property(e => e.Astock).HasColumnName("AStock");

                entity.Property(e => e.Base).HasColumnType("money");

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DescuentoPp)
                    .HasColumnType("smallmoney")
                    .HasColumnName("DescuentoPP");

                entity.Property(e => e.Estado).HasComment("0->No albaranado,1-> Albaranado");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaPedido).HasColumnType("date");

                entity.Property(e => e.IdAlbaran).HasColumnName("ID_Albaran");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NumeroPedido)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.RecargoEq).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("money");
            });

            modelBuilder.Entity<AlbaranesPedidosLinea>(entity =>
            {
                entity.HasKey(e => e.IdPedidoProveedorLinea)
                    .HasName("PK_AlbaranesPedidoLineas");

                entity.Property(e => e.IdPedidoProveedorLinea).HasColumnName("ID_PedidoProveedorLinea");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.ConversionUnidades).HasColumnType("smallmoney");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.IdPedido).HasColumnName("ID_Pedido");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.ImpuestosPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeBonificado).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeDescuentoCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PrecioCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PuntoVerde).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<AlbaranesProveedore>(entity =>
            {
                entity.HasKey(e => e.IdAlbaranProveedor);

                entity.HasComment("Albaranes de compra de mercancia para barra");

                entity.Property(e => e.IdAlbaranProveedor).HasColumnName("ID_AlbaranProveedor");

                entity.Property(e => e.Astock)
                    .HasColumnName("AStock")
                    .HasComment("si el albaran se pasa al stock del centro automaticamente");

                entity.Property(e => e.Base).HasColumnType("money");

                entity.Property(e => e.Descuento)
                    .HasColumnType("smallmoney")
                    .HasComment("descuento global del albaran");

                entity.Property(e => e.DescuentoPp)
                    .HasColumnType("smallmoney")
                    .HasColumnName("DescuentoPP");

                entity.Property(e => e.Estado).HasComment("0 -> normal, 1 -> facturado");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAlbaran).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdFactura).HasColumnName("ID_Factura");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NumeroAlbaran)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.RecargoEq).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("money");
            });

            modelBuilder.Entity<AlbaranesProveedoresLinea>(entity =>
            {
                entity.HasKey(e => e.IdAlbaranProveedorLinea);

                entity.HasComment("Cada una de las lineas que forma un albaran");

                entity.Property(e => e.IdAlbaranProveedorLinea).HasColumnName("ID_AlbaranProveedorLinea");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.ConversionUnidades).HasColumnType("smallmoney");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdAlbaran).HasColumnName("ID_Albaran");

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdMovimientoAlmacen)
                    .HasColumnName("ID_MovimientoAlmacen")
                    .HasComment("movimiento de almacen que actualiza el stock de esta linea de albaran");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.ImpuestosPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeBonificado).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeDescuentoCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PrecioCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PuntoVerde).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<AlbaranesServicio>(entity =>
            {
                entity.HasKey(e => e.IdAlbaranServicio)
                    .HasName("PK_AlbaranesServicio");

                entity.Property(e => e.IdAlbaranServicio).HasColumnName("ID_AlbaranServicio");

                entity.Property(e => e.Astock).HasColumnName("AStock");

                entity.Property(e => e.Base).HasColumnType("money");

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DescuentoPp)
                    .HasColumnType("smallmoney")
                    .HasColumnName("DescuentoPP");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAlbaran).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NumeroAlbaran)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.RecargoEq).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("money");
            });

            modelBuilder.Entity<AlbaranesServiciosLinea>(entity =>
            {
                entity.HasKey(e => e.IdAlbaranServicioLinea);

                entity.Property(e => e.IdAlbaranServicioLinea).HasColumnName("ID_AlbaranServicioLinea");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdAlbaran).HasColumnName("ID_Albaran");

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.ImpuestosPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeBonificado).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeDescuentoCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PuntoVerde).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<Articulo>(entity =>
            {
                entity.HasKey(e => e.IdArticulo);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.Codigo)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("Item code");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("Main Barcode for sell purpose");

                entity.Property(e => e.ControlarStock).HasComment("No use");

                entity.Property(e => e.ConversionUnidades)
                    .HasColumnType("smallmoney")
                    .HasComment("convert item buy in items to sold, for example 1 box to 12 cans");

                entity.Property(e => e.DtoMarketing)
                    .HasColumnType("smallmoney")
                    .HasComment("discount applied to buy for marketing campaing");

                entity.Property(e => e.EnBaja).HasComment("item not to sold");

                entity.Property(e => e.EsCombo).HasComment("if item is a combo");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false)
                    .HasComment("field use to control errors when multiple users works together");

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.IdArticuloAlmacen)
                    .HasColumnName("ID_ArticuloAlmacen")
                    .HasComment("when buy this item, stock stored will be of the item defined here");

                entity.Property(e => e.IdFamiliaAnalisis)
                    .HasColumnName("ID_FamiliaAnalisis")
                    .HasComment("Link to familiasnalisis (analitic family) ");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("Link to Impuestos(Taxes) table");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("image of item to use in system");

                entity.Property(e => e.MateriaPrima).HasComment("is this item raw material");

                entity.Property(e => e.ModificadoPor).HasComment("user who has edit an item");

                entity.Property(e => e.NombreCompleto)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Full item name");

                entity.Property(e => e.NombreCorto)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Short item name");

                entity.Property(e => e.NumeroArticulosSeleccionables).HasComment("if item is combo item, then will be an amount of selectables items user can include in combo, here we say how many items are selectables");

                entity.Property(e => e.NumeroSerie)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("No use");

                entity.Property(e => e.Observaciones)
                    .IsUnicode(false)
                    .HasComment("extra information");

                entity.Property(e => e.PedirPeso).HasComment("Ask to the user for the weight to calculate price");

                entity.Property(e => e.PedirPrecio).HasComment("ask the user for the price of item");

                entity.Property(e => e.PorcentajeBonificado)
                    .HasColumnType("smallmoney")
                    .HasComment("percent discount in a different concept");

                entity.Property(e => e.PorcentajeDtoCompra)
                    .HasColumnType("smallmoney")
                    .HasComment("percent discount aplied when buy item");

                entity.Property(e => e.Precio)
                    .HasColumnType("smallmoney")
                    .HasComment("sell price");

                entity.Property(e => e.PrecioCompra)
                    .HasColumnType("smallmoney")
                    .HasComment("buy price");

                entity.Property(e => e.PrecioIntercambio)
                    .HasColumnType("smallmoney")
                    .HasComment("price applied when move stock from one store to other");

                entity.Property(e => e.PuntoVerde)
                    .HasColumnType("smallmoney")
                    .HasComment("green point tax included when buy product");

                entity.Property(e => e.Rappel)
                    .HasColumnType("smallmoney")
                    .HasComment("Rappel applied to this item");

                entity.Property(e => e.StockActual)
                    .HasColumnType("smallmoney")
                    .HasComment("No use");

                entity.Property(e => e.StockMaximo)
                    .HasColumnType("smallmoney")
                    .HasComment("max allowed stock");

                entity.Property(e => e.StockMinimo)
                    .HasColumnType("smallmoney")
                    .HasComment("minimun necesary stock");

                entity.Property(e => e.TipoUnidad)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("type of unit to convert item buyed, for reporting only");

                entity.Property(e => e.UsarPeso).HasComment("use capture weight from balance to calculate price");
            });

            modelBuilder.Entity<ArticulosCine>(entity =>
            {
                entity.HasKey(e => e.IdArticuloCine);

                entity.Property(e => e.IdArticuloCine).HasColumnName("ID_ArticuloCine");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false)
                    .HasComment("field use to control errors when multiple users works together");

                entity.Property(e => e.IdArticulo)
                    .HasColumnName("ID_Articulo")
                    .HasComment("link to articulos table");

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("link to centros table");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("link to impuestos (taxes) table");

                entity.Property(e => e.ModificadoPor).HasComment("last user who edited the item");

                entity.Property(e => e.Precio)
                    .HasColumnType("smallmoney")
                    .HasComment("item price in the specificated cinema");

                entity.Property(e => e.StockActual)
                    .HasColumnType("numeric(10, 3)")
                    .HasComment("No use");

                entity.Property(e => e.StockMaximo)
                    .HasColumnType("numeric(10, 3)")
                    .HasComment("max allowed stock");

                entity.Property(e => e.StockMinimo)
                    .HasColumnType("numeric(10, 3)")
                    .HasComment("min necesary stock");
            });

            modelBuilder.Entity<ArticulosEscandallo>(entity =>
            {
                entity.HasKey(e => e.IdArticuloEscandallo);

                entity.Property(e => e.IdArticuloEscandallo).HasColumnName("ID_ArticuloEscandallo");

                entity.Property(e => e.Cantidad)
                    .HasColumnType("smallmoney")
                    .HasComment("amount of the basic item");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false)
                    .HasComment("field use to control errors when multiple users works together");

                entity.Property(e => e.IdArticuloDescontar)
                    .HasColumnName("ID_ArticuloDescontar")
                    .HasComment("basic item to use to create the new item\r\n");

                entity.Property(e => e.IdArticuloPadre)
                    .HasColumnName("ID_ArticuloPadre")
                    .HasComment("link to articulos, id of item to create");

                entity.Property(e => e.ModificadoPor).HasComment("user who has edit a row");
            });

            modelBuilder.Entity<ArticulosInmobilizado>(entity =>
            {
                entity.HasKey(e => e.IdArticuloInmobilizado);

                entity.ToTable("ArticulosInmobilizado");

                entity.Property(e => e.IdArticuloInmobilizado).HasColumnName("ID_ArticuloInmobilizado");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("item barcode");

                entity.Property(e => e.ControlarStock).HasComment("control stock of item");

                entity.Property(e => e.DtoMarketing).HasColumnType("smallmoney");

                entity.Property(e => e.EnBaja).HasComment("item not to control");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("link to centros");

                entity.Property(e => e.IdFamiliaAnalisis).HasColumnName("ID_FamiliaAnalisis");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("tax applied to the item value");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("pic of the item");

                entity.Property(e => e.Imagen2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("");

                entity.Property(e => e.Imagen3)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Localizacion)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("location of the item");

                entity.Property(e => e.NombreCompleto)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("full name");

                entity.Property(e => e.NombreCorto)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("short name");

                entity.Property(e => e.NumeroSerie)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnType("smallmoney")
                    .HasComment("price of the item");

                entity.Property(e => e.PrecioCompra)
                    .HasColumnType("smallmoney")
                    .HasComment("original price");

                entity.Property(e => e.PrecioIntercambio).HasColumnType("smallmoney");

                entity.Property(e => e.Rappel).HasColumnType("smallmoney");

                entity.Property(e => e.StockActual).HasComment("actual stock");

                entity.Property(e => e.StockMaximo).HasComment("max stock");

                entity.Property(e => e.StockMinimo).HasComment("minimum stock");

                entity.Property(e => e.TipoUnidad)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("type of unit of items");
            });

            modelBuilder.Entity<ArticulosLinea>(entity =>
            {
                entity.HasKey(e => e.IdArticuloLinea);

                entity.Property(e => e.IdArticuloLinea).HasColumnName("ID_ArticuloLinea");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false)
                    .HasComment("field use to control errors when multiple users works together");

                entity.Property(e => e.IdArticulo)
                    .HasColumnName("ID_Articulo")
                    .HasComment("id of the item used to make a combo");

                entity.Property(e => e.IdArticuloPadre)
                    .HasColumnName("ID_ArticuloPadre")
                    .HasComment("link to articulos, item to sold");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("link to impuestos (taxes)");

                entity.Property(e => e.ModificadoPor).HasComment("user who has edit a row");

                entity.Property(e => e.MostrarEnTicket).HasComment("item must be show ticket");

                entity.Property(e => e.Precio)
                    .HasColumnType("smallmoney")
                    .HasComment("price of this item once is included in combo");

                entity.Property(e => e.Seleccionable).HasComment("item can be fixed in a combo or selectable among a group of items");

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<ArticulosSeries>(entity =>
            {
                entity.HasKey(e => e.IdArticuloSeries);

                entity.Property(e => e.IdArticuloSeries).HasColumnName("ID_ArticuloSeries");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.NumeroSerie)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ButacasPlano>(entity =>
            {
                entity.HasKey(e => e.IdButacaPlano)
                    .HasName("PK_ButacasPlano");

                entity.ToTable("-ButacasPlano");

                entity.Property(e => e.IdButacaPlano).HasColumnName("ID_ButacaPlano");

                entity.Property(e => e.Butaca)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Fila)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.TipoButaca)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Calificacione>(entity =>
            {
                entity.HasKey(e => e.IdCalificacion);

                entity.HasComment("Calificaciones por edades de las peliculas, según el ministerio de cultura");

                entity.Property(e => e.IdCalificacion).HasColumnName("ID_Calificacion");

                entity.Property(e => e.AbreviaturaCalificacion)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("small qualification name, nullable");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IconoCalificacion)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("icon for qualification, nullable");

                entity.Property(e => e.LogoCalificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("image representing qualification, nullable");

                entity.Property(e => e.NombreCalificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Qualification Name");
            });

            modelBuilder.Entity<Centro>(entity =>
            {
                entity.HasKey(e => e.IdCentro);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.Banco)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica1)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica2)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica3)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica4)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica5)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica6)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica7)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica8)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Cif)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CIF");

                entity.Property(e => e.Cifbar)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CIFBar");

                entity.Property(e => e.Circuito)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodComercio)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodTerminal)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoIne)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("CodigoINE");

                entity.Property(e => e.CondicionesRecogida).IsUnicode(false);

                entity.Property(e => e.Contrasena)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cpcentro)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CPCentro");

                entity.Property(e => e.Cpempresa)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CPEmpresa");

                entity.Property(e => e.CpempresaBar)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CPEmpresaBar");

                entity.Property(e => e.Desofuscar)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DiaEspectador)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionEmpresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionEmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Empresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExportarIcaa).HasColumnName("ExportarICAA");

                entity.Property(e => e.ExportarIcaag).HasColumnName("ExportarICAAG");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Gps)
                    .HasMaxLength(260)
                    .IsUnicode(false)
                    .HasColumnName("GPS");

                entity.Property(e => e.Icono1)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono1");

                entity.Property(e => e.Icono2)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono2");

                entity.Property(e => e.Icono3)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono3");

                entity.Property(e => e.Icono4)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono4");

                entity.Property(e => e.Icono5)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono5");

                entity.Property(e => e.Icono6)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono6");

                entity.Property(e => e.Icono7)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("icono7");

                entity.Property(e => e.Icono8)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("icono8");

                entity.Property(e => e.Icono9)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("icono9");

                entity.Property(e => e.ImporteMinimoPorOperacion).HasColumnType("smallmoney");

                entity.Property(e => e.Ip)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("IP");

                entity.Property(e => e.Ivaentradas)
                    .HasColumnType("smallmoney")
                    .HasColumnName("IVAEntradas");

                entity.Property(e => e.Legacy).HasColumnName("legacy");

                entity.Property(e => e.Lengua)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LineasBus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LineasMetro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadEmpresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadEmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogoCine)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogoImpresion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NombreBd)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("NombreBD");

                entity.Property(e => e.NregEmpresa)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("NRegEmpresa");

                entity.Property(e => e.PalabraClave)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Parking)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PorcentajeImpuestosFacturacion).HasColumnType("smallmoney");

                entity.Property(e => e.ProvinciaCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinciaEmpresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinciaEmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TextoPieTicket)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Tlfno1)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Tlfno2)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WarningMsg).IsUnicode(false);
            });

            modelBuilder.Entity<ClientesCadena>(entity =>
            {
                entity.HasKey(e => e.IdClienteCadena);

                entity.ToTable("ClientesCadena");

                entity.HasComment("Socios del cine, incluye clientes de temporada, fidelizados y de tarjetas de abonado");

                entity.Property(e => e.IdClienteCadena).HasColumnName("ID_ClienteCadena");

                entity.Property(e => e.Apellidos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoSocio)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dni)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("DNI");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.Localidad)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Provincia)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Pwd)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("pwd");

                entity.Property(e => e.Tlfno)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientesFactura>(entity =>
            {
                entity.HasKey(e => e.IdClienteFactura);

                entity.HasComment("Clientes para Facturas Puntuales");

                entity.Property(e => e.IdClienteFactura).HasColumnName("ID_ClienteFactura");

                entity.Property(e => e.Cif)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("CIF");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.Localidad)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCompleto)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Provincia)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Tlfno)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodigosPai>(entity =>
            {
                entity.HasKey(e => e.IdCodigoPais)
                    .HasName("PK_CodigoPais");

                entity.Property(e => e.IdCodigoPais).HasColumnName("ID_CodigoPais");

                entity.Property(e => e.Codigo)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NombrePais)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zona)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ConfigGridUsuario>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Campo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Encabezado)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Formulario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdConfigGridUsuarios)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID_ConfigGridUsuarios");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");
            });

            modelBuilder.Entity<ContratosDistribuidora>(entity =>
            {
                entity.HasKey(e => e.IdContratoDistribuidora);

                entity.HasComment("Almacena los contratos que los centros establecen con las Distribuidoras");

                entity.Property(e => e.IdContratoDistribuidora).HasColumnName("ID_ContratoDistribuidora");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("link to centros table, if 0 means for all the cinemas");

                entity.Property(e => e.IdDistribuidora)
                    .HasColumnName("ID_Distribuidora")
                    .HasComment("link to Distribuidoras table");

                entity.Property(e => e.IdEspectaculo)
                    .HasColumnName("ID_Espectaculo")
                    .HasComment("link to Film, 0 if contract is valid for all the films");

                entity.Property(e => e.NumeroContrato)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("contract number");
            });

            modelBuilder.Entity<ContratosDistribuidorasLinea>(entity =>
            {
                entity.HasKey(e => e.IdContratoDistribuidoraLinea);

                entity.HasComment("Cada una de las lineas por fechas de un contrato");

                entity.Property(e => e.IdContratoDistribuidoraLinea).HasColumnName("ID_ContratoDistribuidoraLinea");

                entity.Property(e => e.FacturacionPorcentaje).HasComment("true means apply percent values");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin)
                    .HasColumnType("date")
                    .HasComment("Final date");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("date")
                    .HasComment("initial date");

                entity.Property(e => e.IdContrato).HasColumnName("ID_Contrato");

                entity.Property(e => e.ImporteDomingo).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteJueves).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteLunes)
                    .HasColumnType("smallmoney")
                    .HasComment("money value applied on monday");

                entity.Property(e => e.ImporteMartes)
                    .HasColumnType("smallmoney")
                    .HasComment("money value applied on tuesday");

                entity.Property(e => e.ImporteMiercoles)
                    .HasColumnType("smallmoney")
                    .HasComment("money value applied on wenesday");

                entity.Property(e => e.ImporteSabado).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteViernes).HasColumnType("smallmoney");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PorcentajeDomingo)
                    .HasColumnType("smallmoney")
                    .HasComment("percent value for sunday");

                entity.Property(e => e.PorcentajeJueves)
                    .HasColumnType("smallmoney")
                    .HasComment("percent value for thursday");

                entity.Property(e => e.PorcentajeLunes)
                    .HasColumnType("smallmoney")
                    .HasComment("percent value for monday");

                entity.Property(e => e.PorcentajeMartes)
                    .HasColumnType("smallmoney")
                    .HasComment("percent value for tuesday");

                entity.Property(e => e.PorcentajeMiercoles)
                    .HasColumnType("smallmoney")
                    .HasComment("value for wednesday");

                entity.Property(e => e.PorcentajeSabado)
                    .HasColumnType("smallmoney")
                    .HasComment("percent value for saturday");

                entity.Property(e => e.PorcentajeViernes)
                    .HasColumnType("smallmoney")
                    .HasComment("percent value for friday");
            });

            modelBuilder.Entity<DatosCadena>(entity =>
            {
                entity.ToTable("DatosCadena");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cpcadena)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("CPCadena");

                entity.Property(e => e.DireccionCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ImagenCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinciaCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TelefonoCadena)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Distribuidora>(entity =>
            {
                entity.HasKey(e => e.IdDistribuidora);

                entity.HasComment("Un registro por cada promotor/distribuidor que aporta espectaculos, se almacenan tb sus datos de facturación por si se incluyen en el ticket de cliente, en lugar de los del centro");

                entity.Property(e => e.IdDistribuidora).HasColumnName("ID_Distribuidora");

                entity.Property(e => e.Cif)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CIF")
                    .HasComment("distributor  tax id, nullable");

                entity.Property(e => e.CodigoIcaa)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CodigoICAA")
                    .HasComment("distributor code, nullable");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP")
                    .HasComment("distributor  postal code, nullable");

                entity.Property(e => e.CuentaContable)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("distributor account, nullable");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("distributor  address, nullable");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("distributor email, nullable");

                entity.Property(e => e.FacturacionPorPorcentaje).HasComment("distributors invoice by percent");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Localidad)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("distributor  city, nullable");

                entity.Property(e => e.Login)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("distributor login for distributor´s web, nullable");

                entity.Property(e => e.NombreDistribuidora)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("distributor name");

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("distributor password for distributor´s web, nullable");

                entity.Property(e => e.PorcentajeDerechosAutor)
                    .HasColumnType("smallmoney")
                    .HasComment("copyright percent, nullable");

                entity.Property(e => e.PorcentajeImpuestosFacturacion)
                    .HasColumnType("smallmoney")
                    .HasComment("distributors invoice percent, nullable");

                entity.Property(e => e.Provincia)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("distributor  state, nullable");

                entity.Property(e => e.Siglas)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("small name");
            });

            modelBuilder.Entity<Espectaculo>(entity =>
            {
                entity.HasKey(e => e.IdEspectaculo);

                entity.HasComment("Cada uno de los espectaculos programables para su venta.");

                entity.Property(e => e.IdEspectaculo).HasColumnName("ID_Espectaculo");

                entity.Property(e => e.AbreviaturaCalificacion)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("small qualification");

                entity.Property(e => e.Ano)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasComment("film production year, nullable");

                entity.Property(e => e.ApellidosDirectorPrincipal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cartel)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasComment("film poster, nullable");

                entity.Property(e => e.Cartel2)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasComment("2 film poster, nullable");

                entity.Property(e => e.Cartel3)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasComment("3 film poster, nullable");

                entity.Property(e => e.CodigoIcaadistribuidora)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CodigoICAADistribuidora")
                    .HasComment("distributors code");

                entity.Property(e => e.CodigoPaisPrincipal)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Comunitaria).HasComment("film from EU");

                entity.Property(e => e.Director)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("film´s Principal, nulable");

                entity.Property(e => e.Duracion).HasComment("films duration");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEstreno)
                    .HasColumnType("date")
                    .HasComment("release film date, nullable");

                entity.Property(e => e.IconoCalificacion)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("qualification icon, nullable");

                entity.Property(e => e.IdDistribuidora)
                    .HasColumnName("ID_Distribuidora")
                    .HasComment("distributors ID, link Espectaculos to Distributors");

                entity.Property(e => e.IdiomaOriginal)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("original language of film, nullable");

                entity.Property(e => e.Interpretes)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasComment("film´s actors, nulable");

                entity.Property(e => e.Largometraje).HasComment("largometraje/cortometraje");

                entity.Property(e => e.Lenguaje)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LetraIdiomaOriginal)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasComment("letter defining language of film, nullable");

                entity.Property(e => e.Nacionalidad)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("film´s country, nullable");

                entity.Property(e => e.Ncalificacion)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("NCalificacion")
                    .HasComment("id from goverment");

                entity.Property(e => e.Nexpediente)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("NExpediente")
                    .HasComment("expdt of the film");

                entity.Property(e => e.NombreCalificacion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("films qualification");

                entity.Property(e => e.NombreDirectorPrincipal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreDistribuidora)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("distributors Name, link Espectaculos to Distributors");

                entity.Property(e => e.NombreGenero)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Film Gender");

                entity.Property(e => e.Nrollos)
                    .HasColumnName("NRollos")
                    .HasComment("no use");

                entity.Property(e => e.Pdf)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PDF")
                    .HasComment("doc associated to film, nullable");

                entity.Property(e => e.Pelicula).HasComment("a really film or other kind of show");

                entity.Property(e => e.PeliculaX).HasComment("adult film");

                entity.Property(e => e.Siglas)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("distributors small name, link Espectaculos to Distributors, nullable");

                entity.Property(e => e.Sinopsis)
                    .IsUnicode(false)
                    .HasComment("film´s description, nullable");

                entity.Property(e => e.SinopsisAlternativa)
                    .IsUnicode(false)
                    .HasComment("2 film´s description, nullable");

                entity.Property(e => e.Sinopsisalternativa2)
                    .IsUnicode(false)
                    .HasComment("3 film´s description, nullable");

                entity.Property(e => e.TiempoLimpieza).HasComment("cleaning time after the film, nullable");

                entity.Property(e => e.TiempoTrailer).HasComment("trailers before film duration, nullable");

                entity.Property(e => e.TipoEventoCodigo)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TipoEventoDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Titulo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Film title");

                entity.Property(e => e.TituloOriginal)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Original Film title");

                entity.Property(e => e.Video)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("film trailer, nullable");

                entity.Property(e => e.VideoSecundario)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("2 film trailer, nullable");
            });

            modelBuilder.Entity<EspectaculosMultiple>(entity =>
            {
                entity.HasKey(e => e.IdEspectaculoMultiple);

                entity.HasComment("En algunas ocasiones un espectaculo se compone de varios que se exhiben de forma secuencial con una sola entrada, estos se definen en esta tabla, donde se indica que espectaculo es el padre, o primero que se emite y cuales lo siguen.");

                entity.Property(e => e.IdEspectaculoMultiple).HasColumnName("ID_EspectaculoMultiple");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdEspectaculoHijo)
                    .HasColumnName("ID_EspectaculoHijo")
                    .HasComment("film linked to");

                entity.Property(e => e.IdEspectaculoPadre)
                    .HasColumnName("ID_EspectaculoPadre")
                    .HasComment("film to link with");
            });

            modelBuilder.Entity<EtlconfigTableTransform>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_ETLConfig_TableTransform");

                entity.Property(e => e.Col1).HasColumnName("col1");

                entity.Property(e => e.Col10).HasColumnName("col10");

                entity.Property(e => e.Col11).HasColumnName("col11");

                entity.Property(e => e.Col12).HasColumnName("col12");

                entity.Property(e => e.Col13).HasColumnName("col13");

                entity.Property(e => e.Col14).HasColumnName("col14");

                entity.Property(e => e.Col15).HasColumnName("col15");

                entity.Property(e => e.Col16).HasColumnName("col16");

                entity.Property(e => e.Col17).HasColumnName("col17");

                entity.Property(e => e.Col18).HasColumnName("col18");

                entity.Property(e => e.Col19).HasColumnName("col19");

                entity.Property(e => e.Col2).HasColumnName("col2");

                entity.Property(e => e.Col20).HasColumnName("col20");

                entity.Property(e => e.Col21).HasColumnName("col21");

                entity.Property(e => e.Col22).HasColumnName("col22");

                entity.Property(e => e.Col23).HasColumnName("col23");

                entity.Property(e => e.Col24).HasColumnName("col24");

                entity.Property(e => e.Col25).HasColumnName("col25");

                entity.Property(e => e.Col26).HasColumnName("col26");

                entity.Property(e => e.Col27).HasColumnName("col27");

                entity.Property(e => e.Col28).HasColumnName("col28");

                entity.Property(e => e.Col29).HasColumnName("col29");

                entity.Property(e => e.Col3).HasColumnName("col3");

                entity.Property(e => e.Col30).HasColumnName("col30");

                entity.Property(e => e.Col31).HasColumnName("col31");

                entity.Property(e => e.Col32).HasColumnName("col32");

                entity.Property(e => e.Col33).HasColumnName("col33");

                entity.Property(e => e.Col34).HasColumnName("col34");

                entity.Property(e => e.Col35).HasColumnName("col35");

                entity.Property(e => e.Col36).HasColumnName("col36");

                entity.Property(e => e.Col37).HasColumnName("col37");

                entity.Property(e => e.Col38).HasColumnName("col38");

                entity.Property(e => e.Col39).HasColumnName("col39");

                entity.Property(e => e.Col4).HasColumnName("col4");

                entity.Property(e => e.Col40).HasColumnName("col40");

                entity.Property(e => e.Col41).HasColumnName("col41");

                entity.Property(e => e.Col42).HasColumnName("col42");

                entity.Property(e => e.Col43).HasColumnName("col43");

                entity.Property(e => e.Col44).HasColumnName("col44");

                entity.Property(e => e.Col45).HasColumnName("col45");

                entity.Property(e => e.Col46).HasColumnName("col46");

                entity.Property(e => e.Col47).HasColumnName("col47");

                entity.Property(e => e.Col48).HasColumnName("col48");

                entity.Property(e => e.Col49).HasColumnName("col49");

                entity.Property(e => e.Col5).HasColumnName("col5");

                entity.Property(e => e.Col50).HasColumnName("col50");

                entity.Property(e => e.Col51).HasColumnName("col51");

                entity.Property(e => e.Col52).HasColumnName("col52");

                entity.Property(e => e.Col53).HasColumnName("col53");

                entity.Property(e => e.Col54).HasColumnName("col54");

                entity.Property(e => e.Col55).HasColumnName("col55");

                entity.Property(e => e.Col56).HasColumnName("col56");

                entity.Property(e => e.Col57).HasColumnName("col57");

                entity.Property(e => e.Col58).HasColumnName("col58");

                entity.Property(e => e.Col59).HasColumnName("col59");

                entity.Property(e => e.Col6).HasColumnName("col6");

                entity.Property(e => e.Col60).HasColumnName("col60");

                entity.Property(e => e.Col61).HasColumnName("col61");

                entity.Property(e => e.Col62).HasColumnName("col62");

                entity.Property(e => e.Col63).HasColumnName("col63");

                entity.Property(e => e.Col64).HasColumnName("col64");

                entity.Property(e => e.Col65).HasColumnName("col65");

                entity.Property(e => e.Col66).HasColumnName("col66");

                entity.Property(e => e.Col67).HasColumnName("col67");

                entity.Property(e => e.Col68).HasColumnName("col68");

                entity.Property(e => e.Col69).HasColumnName("col69");

                entity.Property(e => e.Col7).HasColumnName("col7");

                entity.Property(e => e.Col70).HasColumnName("col70");

                entity.Property(e => e.Col71).HasColumnName("col71");

                entity.Property(e => e.Col72).HasColumnName("col72");

                entity.Property(e => e.Col73).HasColumnName("col73");

                entity.Property(e => e.Col74).HasColumnName("col74");

                entity.Property(e => e.Col75).HasColumnName("col75");

                entity.Property(e => e.Col76).HasColumnName("col76");

                entity.Property(e => e.Col77).HasColumnName("col77");

                entity.Property(e => e.Col78).HasColumnName("col78");

                entity.Property(e => e.Col79).HasColumnName("col79");

                entity.Property(e => e.Col8).HasColumnName("col8");

                entity.Property(e => e.Col80).HasColumnName("col80");

                entity.Property(e => e.Col81).HasColumnName("col81");

                entity.Property(e => e.Col82).HasColumnName("col82");

                entity.Property(e => e.Col83).HasColumnName("col83");

                entity.Property(e => e.Col84).HasColumnName("col84");

                entity.Property(e => e.Col85).HasColumnName("col85");

                entity.Property(e => e.Col86).HasColumnName("col86");

                entity.Property(e => e.Col87).HasColumnName("col87");

                entity.Property(e => e.Col88).HasColumnName("col88");

                entity.Property(e => e.Col89).HasColumnName("col89");

                entity.Property(e => e.Col9).HasColumnName("col9");

                entity.Property(e => e.Col90).HasColumnName("col90");

                entity.Property(e => e.Col91).HasColumnName("col91");

                entity.Property(e => e.Col92).HasColumnName("col92");

                entity.Property(e => e.Col93).HasColumnName("col93");

                entity.Property(e => e.Col94).HasColumnName("col94");

                entity.Property(e => e.Col95).HasColumnName("col95");

                entity.Property(e => e.Col96).HasColumnName("col96");

                entity.Property(e => e.Col97).HasColumnName("col97");

                entity.Property(e => e.Col98).HasColumnName("col98");

                entity.Property(e => e.Col99).HasColumnName("col99");
            });

            modelBuilder.Entity<EtlconfigTableTransformAlcala>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_ETLConfig_TableTransform_Alcala");

                entity.Property(e => e.Col1).HasColumnName("col1");

                entity.Property(e => e.Col10).HasColumnName("col10");

                entity.Property(e => e.Col11).HasColumnName("col11");

                entity.Property(e => e.Col12).HasColumnName("col12");

                entity.Property(e => e.Col13).HasColumnName("col13");

                entity.Property(e => e.Col14).HasColumnName("col14");

                entity.Property(e => e.Col15).HasColumnName("col15");

                entity.Property(e => e.Col16).HasColumnName("col16");

                entity.Property(e => e.Col17).HasColumnName("col17");

                entity.Property(e => e.Col18).HasColumnName("col18");

                entity.Property(e => e.Col19).HasColumnName("col19");

                entity.Property(e => e.Col2).HasColumnName("col2");

                entity.Property(e => e.Col20).HasColumnName("col20");

                entity.Property(e => e.Col21).HasColumnName("col21");

                entity.Property(e => e.Col22).HasColumnName("col22");

                entity.Property(e => e.Col23).HasColumnName("col23");

                entity.Property(e => e.Col24).HasColumnName("col24");

                entity.Property(e => e.Col25).HasColumnName("col25");

                entity.Property(e => e.Col26).HasColumnName("col26");

                entity.Property(e => e.Col27).HasColumnName("col27");

                entity.Property(e => e.Col28).HasColumnName("col28");

                entity.Property(e => e.Col29).HasColumnName("col29");

                entity.Property(e => e.Col3).HasColumnName("col3");

                entity.Property(e => e.Col30).HasColumnName("col30");

                entity.Property(e => e.Col31).HasColumnName("col31");

                entity.Property(e => e.Col32).HasColumnName("col32");

                entity.Property(e => e.Col33).HasColumnName("col33");

                entity.Property(e => e.Col34).HasColumnName("col34");

                entity.Property(e => e.Col35).HasColumnName("col35");

                entity.Property(e => e.Col36).HasColumnName("col36");

                entity.Property(e => e.Col37).HasColumnName("col37");

                entity.Property(e => e.Col38).HasColumnName("col38");

                entity.Property(e => e.Col39).HasColumnName("col39");

                entity.Property(e => e.Col4).HasColumnName("col4");

                entity.Property(e => e.Col40).HasColumnName("col40");

                entity.Property(e => e.Col41).HasColumnName("col41");

                entity.Property(e => e.Col42).HasColumnName("col42");

                entity.Property(e => e.Col43).HasColumnName("col43");

                entity.Property(e => e.Col44).HasColumnName("col44");

                entity.Property(e => e.Col45).HasColumnName("col45");

                entity.Property(e => e.Col46).HasColumnName("col46");

                entity.Property(e => e.Col47).HasColumnName("col47");

                entity.Property(e => e.Col48).HasColumnName("col48");

                entity.Property(e => e.Col49).HasColumnName("col49");

                entity.Property(e => e.Col5).HasColumnName("col5");

                entity.Property(e => e.Col50).HasColumnName("col50");

                entity.Property(e => e.Col51).HasColumnName("col51");

                entity.Property(e => e.Col52).HasColumnName("col52");

                entity.Property(e => e.Col53).HasColumnName("col53");

                entity.Property(e => e.Col54).HasColumnName("col54");

                entity.Property(e => e.Col55).HasColumnName("col55");

                entity.Property(e => e.Col56).HasColumnName("col56");

                entity.Property(e => e.Col57).HasColumnName("col57");

                entity.Property(e => e.Col58).HasColumnName("col58");

                entity.Property(e => e.Col59).HasColumnName("col59");

                entity.Property(e => e.Col6).HasColumnName("col6");

                entity.Property(e => e.Col60).HasColumnName("col60");

                entity.Property(e => e.Col61).HasColumnName("col61");

                entity.Property(e => e.Col62).HasColumnName("col62");

                entity.Property(e => e.Col63).HasColumnName("col63");

                entity.Property(e => e.Col64).HasColumnName("col64");

                entity.Property(e => e.Col65).HasColumnName("col65");

                entity.Property(e => e.Col66).HasColumnName("col66");

                entity.Property(e => e.Col67).HasColumnName("col67");

                entity.Property(e => e.Col68).HasColumnName("col68");

                entity.Property(e => e.Col69).HasColumnName("col69");

                entity.Property(e => e.Col7).HasColumnName("col7");

                entity.Property(e => e.Col70).HasColumnName("col70");

                entity.Property(e => e.Col71).HasColumnName("col71");

                entity.Property(e => e.Col72).HasColumnName("col72");

                entity.Property(e => e.Col73).HasColumnName("col73");

                entity.Property(e => e.Col74).HasColumnName("col74");

                entity.Property(e => e.Col75).HasColumnName("col75");

                entity.Property(e => e.Col76).HasColumnName("col76");

                entity.Property(e => e.Col77).HasColumnName("col77");

                entity.Property(e => e.Col78).HasColumnName("col78");

                entity.Property(e => e.Col79).HasColumnName("col79");

                entity.Property(e => e.Col8).HasColumnName("col8");

                entity.Property(e => e.Col80).HasColumnName("col80");

                entity.Property(e => e.Col81).HasColumnName("col81");

                entity.Property(e => e.Col82).HasColumnName("col82");

                entity.Property(e => e.Col83).HasColumnName("col83");

                entity.Property(e => e.Col84).HasColumnName("col84");

                entity.Property(e => e.Col85).HasColumnName("col85");

                entity.Property(e => e.Col86).HasColumnName("col86");

                entity.Property(e => e.Col87).HasColumnName("col87");

                entity.Property(e => e.Col88).HasColumnName("col88");

                entity.Property(e => e.Col89).HasColumnName("col89");

                entity.Property(e => e.Col9).HasColumnName("col9");

                entity.Property(e => e.Col90).HasColumnName("col90");

                entity.Property(e => e.Col91).HasColumnName("col91");

                entity.Property(e => e.Col92).HasColumnName("col92");

                entity.Property(e => e.Col93).HasColumnName("col93");

                entity.Property(e => e.Col94).HasColumnName("col94");

                entity.Property(e => e.Col95).HasColumnName("col95");

                entity.Property(e => e.Col96).HasColumnName("col96");

                entity.Property(e => e.Col97).HasColumnName("col97");

                entity.Property(e => e.Col98).HasColumnName("col98");

                entity.Property(e => e.Col99).HasColumnName("col99");
            });

            modelBuilder.Entity<EtlconfigTableTransformAlfare>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_ETLConfig_TableTransform_Alfares");

                entity.Property(e => e.Col1).HasColumnName("col1");

                entity.Property(e => e.Col10).HasColumnName("col10");

                entity.Property(e => e.Col11).HasColumnName("col11");

                entity.Property(e => e.Col12).HasColumnName("col12");

                entity.Property(e => e.Col13).HasColumnName("col13");

                entity.Property(e => e.Col14).HasColumnName("col14");

                entity.Property(e => e.Col15).HasColumnName("col15");

                entity.Property(e => e.Col16).HasColumnName("col16");

                entity.Property(e => e.Col17).HasColumnName("col17");

                entity.Property(e => e.Col18).HasColumnName("col18");

                entity.Property(e => e.Col19).HasColumnName("col19");

                entity.Property(e => e.Col2).HasColumnName("col2");

                entity.Property(e => e.Col20).HasColumnName("col20");

                entity.Property(e => e.Col21).HasColumnName("col21");

                entity.Property(e => e.Col22).HasColumnName("col22");

                entity.Property(e => e.Col23).HasColumnName("col23");

                entity.Property(e => e.Col24).HasColumnName("col24");

                entity.Property(e => e.Col25).HasColumnName("col25");

                entity.Property(e => e.Col26).HasColumnName("col26");

                entity.Property(e => e.Col27).HasColumnName("col27");

                entity.Property(e => e.Col28).HasColumnName("col28");

                entity.Property(e => e.Col29).HasColumnName("col29");

                entity.Property(e => e.Col3).HasColumnName("col3");

                entity.Property(e => e.Col30).HasColumnName("col30");

                entity.Property(e => e.Col31).HasColumnName("col31");

                entity.Property(e => e.Col32).HasColumnName("col32");

                entity.Property(e => e.Col33).HasColumnName("col33");

                entity.Property(e => e.Col34).HasColumnName("col34");

                entity.Property(e => e.Col35).HasColumnName("col35");

                entity.Property(e => e.Col36).HasColumnName("col36");

                entity.Property(e => e.Col37).HasColumnName("col37");

                entity.Property(e => e.Col38).HasColumnName("col38");

                entity.Property(e => e.Col39).HasColumnName("col39");

                entity.Property(e => e.Col4).HasColumnName("col4");

                entity.Property(e => e.Col40).HasColumnName("col40");

                entity.Property(e => e.Col41).HasColumnName("col41");

                entity.Property(e => e.Col42).HasColumnName("col42");

                entity.Property(e => e.Col43).HasColumnName("col43");

                entity.Property(e => e.Col44).HasColumnName("col44");

                entity.Property(e => e.Col45).HasColumnName("col45");

                entity.Property(e => e.Col46).HasColumnName("col46");

                entity.Property(e => e.Col47).HasColumnName("col47");

                entity.Property(e => e.Col48).HasColumnName("col48");

                entity.Property(e => e.Col49).HasColumnName("col49");

                entity.Property(e => e.Col5).HasColumnName("col5");

                entity.Property(e => e.Col50).HasColumnName("col50");

                entity.Property(e => e.Col51).HasColumnName("col51");

                entity.Property(e => e.Col52).HasColumnName("col52");

                entity.Property(e => e.Col53).HasColumnName("col53");

                entity.Property(e => e.Col54).HasColumnName("col54");

                entity.Property(e => e.Col55).HasColumnName("col55");

                entity.Property(e => e.Col56).HasColumnName("col56");

                entity.Property(e => e.Col57).HasColumnName("col57");

                entity.Property(e => e.Col58).HasColumnName("col58");

                entity.Property(e => e.Col59).HasColumnName("col59");

                entity.Property(e => e.Col6).HasColumnName("col6");

                entity.Property(e => e.Col60).HasColumnName("col60");

                entity.Property(e => e.Col61).HasColumnName("col61");

                entity.Property(e => e.Col62).HasColumnName("col62");

                entity.Property(e => e.Col63).HasColumnName("col63");

                entity.Property(e => e.Col64).HasColumnName("col64");

                entity.Property(e => e.Col65).HasColumnName("col65");

                entity.Property(e => e.Col66).HasColumnName("col66");

                entity.Property(e => e.Col67).HasColumnName("col67");

                entity.Property(e => e.Col68).HasColumnName("col68");

                entity.Property(e => e.Col69).HasColumnName("col69");

                entity.Property(e => e.Col7).HasColumnName("col7");

                entity.Property(e => e.Col70).HasColumnName("col70");

                entity.Property(e => e.Col71).HasColumnName("col71");

                entity.Property(e => e.Col72).HasColumnName("col72");

                entity.Property(e => e.Col73).HasColumnName("col73");

                entity.Property(e => e.Col74).HasColumnName("col74");

                entity.Property(e => e.Col75).HasColumnName("col75");

                entity.Property(e => e.Col76).HasColumnName("col76");

                entity.Property(e => e.Col77).HasColumnName("col77");

                entity.Property(e => e.Col78).HasColumnName("col78");

                entity.Property(e => e.Col79).HasColumnName("col79");

                entity.Property(e => e.Col8).HasColumnName("col8");

                entity.Property(e => e.Col80).HasColumnName("col80");

                entity.Property(e => e.Col81).HasColumnName("col81");

                entity.Property(e => e.Col82).HasColumnName("col82");

                entity.Property(e => e.Col83).HasColumnName("col83");

                entity.Property(e => e.Col84).HasColumnName("col84");

                entity.Property(e => e.Col85).HasColumnName("col85");

                entity.Property(e => e.Col86).HasColumnName("col86");

                entity.Property(e => e.Col87).HasColumnName("col87");

                entity.Property(e => e.Col88).HasColumnName("col88");

                entity.Property(e => e.Col89).HasColumnName("col89");

                entity.Property(e => e.Col9).HasColumnName("col9");

                entity.Property(e => e.Col90).HasColumnName("col90");

                entity.Property(e => e.Col91).HasColumnName("col91");

                entity.Property(e => e.Col92).HasColumnName("col92");

                entity.Property(e => e.Col93).HasColumnName("col93");

                entity.Property(e => e.Col94).HasColumnName("col94");

                entity.Property(e => e.Col95).HasColumnName("col95");

                entity.Property(e => e.Col96).HasColumnName("col96");

                entity.Property(e => e.Col97).HasColumnName("col97");

                entity.Property(e => e.Col98).HasColumnName("col98");

                entity.Property(e => e.Col99).HasColumnName("col99");
            });

            modelBuilder.Entity<EtlconfigTableTransformTelde>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("_ETLConfig_TableTransform_Telde");

                entity.Property(e => e.Col1).HasColumnName("col1");

                entity.Property(e => e.Col10).HasColumnName("col10");

                entity.Property(e => e.Col11).HasColumnName("col11");

                entity.Property(e => e.Col12).HasColumnName("col12");

                entity.Property(e => e.Col13).HasColumnName("col13");

                entity.Property(e => e.Col14).HasColumnName("col14");

                entity.Property(e => e.Col15).HasColumnName("col15");

                entity.Property(e => e.Col16).HasColumnName("col16");

                entity.Property(e => e.Col17).HasColumnName("col17");

                entity.Property(e => e.Col18).HasColumnName("col18");

                entity.Property(e => e.Col19).HasColumnName("col19");

                entity.Property(e => e.Col2).HasColumnName("col2");

                entity.Property(e => e.Col20).HasColumnName("col20");

                entity.Property(e => e.Col21).HasColumnName("col21");

                entity.Property(e => e.Col22).HasColumnName("col22");

                entity.Property(e => e.Col23).HasColumnName("col23");

                entity.Property(e => e.Col24).HasColumnName("col24");

                entity.Property(e => e.Col25).HasColumnName("col25");

                entity.Property(e => e.Col26).HasColumnName("col26");

                entity.Property(e => e.Col27).HasColumnName("col27");

                entity.Property(e => e.Col28).HasColumnName("col28");

                entity.Property(e => e.Col29).HasColumnName("col29");

                entity.Property(e => e.Col3).HasColumnName("col3");

                entity.Property(e => e.Col30).HasColumnName("col30");

                entity.Property(e => e.Col31).HasColumnName("col31");

                entity.Property(e => e.Col32).HasColumnName("col32");

                entity.Property(e => e.Col33).HasColumnName("col33");

                entity.Property(e => e.Col34).HasColumnName("col34");

                entity.Property(e => e.Col35).HasColumnName("col35");

                entity.Property(e => e.Col36).HasColumnName("col36");

                entity.Property(e => e.Col37).HasColumnName("col37");

                entity.Property(e => e.Col38).HasColumnName("col38");

                entity.Property(e => e.Col39).HasColumnName("col39");

                entity.Property(e => e.Col4).HasColumnName("col4");

                entity.Property(e => e.Col40).HasColumnName("col40");

                entity.Property(e => e.Col41).HasColumnName("col41");

                entity.Property(e => e.Col42).HasColumnName("col42");

                entity.Property(e => e.Col43).HasColumnName("col43");

                entity.Property(e => e.Col44).HasColumnName("col44");

                entity.Property(e => e.Col45).HasColumnName("col45");

                entity.Property(e => e.Col46).HasColumnName("col46");

                entity.Property(e => e.Col47).HasColumnName("col47");

                entity.Property(e => e.Col48).HasColumnName("col48");

                entity.Property(e => e.Col49).HasColumnName("col49");

                entity.Property(e => e.Col5).HasColumnName("col5");

                entity.Property(e => e.Col50).HasColumnName("col50");

                entity.Property(e => e.Col51).HasColumnName("col51");

                entity.Property(e => e.Col52).HasColumnName("col52");

                entity.Property(e => e.Col53).HasColumnName("col53");

                entity.Property(e => e.Col54).HasColumnName("col54");

                entity.Property(e => e.Col55).HasColumnName("col55");

                entity.Property(e => e.Col56).HasColumnName("col56");

                entity.Property(e => e.Col57).HasColumnName("col57");

                entity.Property(e => e.Col58).HasColumnName("col58");

                entity.Property(e => e.Col59).HasColumnName("col59");

                entity.Property(e => e.Col6).HasColumnName("col6");

                entity.Property(e => e.Col60).HasColumnName("col60");

                entity.Property(e => e.Col61).HasColumnName("col61");

                entity.Property(e => e.Col62).HasColumnName("col62");

                entity.Property(e => e.Col63).HasColumnName("col63");

                entity.Property(e => e.Col64).HasColumnName("col64");

                entity.Property(e => e.Col65).HasColumnName("col65");

                entity.Property(e => e.Col66).HasColumnName("col66");

                entity.Property(e => e.Col67).HasColumnName("col67");

                entity.Property(e => e.Col68).HasColumnName("col68");

                entity.Property(e => e.Col69).HasColumnName("col69");

                entity.Property(e => e.Col7).HasColumnName("col7");

                entity.Property(e => e.Col70).HasColumnName("col70");

                entity.Property(e => e.Col71).HasColumnName("col71");

                entity.Property(e => e.Col72).HasColumnName("col72");

                entity.Property(e => e.Col73).HasColumnName("col73");

                entity.Property(e => e.Col74).HasColumnName("col74");

                entity.Property(e => e.Col75).HasColumnName("col75");

                entity.Property(e => e.Col76).HasColumnName("col76");

                entity.Property(e => e.Col77).HasColumnName("col77");

                entity.Property(e => e.Col78).HasColumnName("col78");

                entity.Property(e => e.Col79).HasColumnName("col79");

                entity.Property(e => e.Col8).HasColumnName("col8");

                entity.Property(e => e.Col80).HasColumnName("col80");

                entity.Property(e => e.Col81).HasColumnName("col81");

                entity.Property(e => e.Col82).HasColumnName("col82");

                entity.Property(e => e.Col83).HasColumnName("col83");

                entity.Property(e => e.Col84).HasColumnName("col84");

                entity.Property(e => e.Col85).HasColumnName("col85");

                entity.Property(e => e.Col86).HasColumnName("col86");

                entity.Property(e => e.Col87).HasColumnName("col87");

                entity.Property(e => e.Col88).HasColumnName("col88");

                entity.Property(e => e.Col89).HasColumnName("col89");

                entity.Property(e => e.Col9).HasColumnName("col9");

                entity.Property(e => e.Col90).HasColumnName("col90");

                entity.Property(e => e.Col91).HasColumnName("col91");

                entity.Property(e => e.Col92).HasColumnName("col92");

                entity.Property(e => e.Col93).HasColumnName("col93");

                entity.Property(e => e.Col94).HasColumnName("col94");

                entity.Property(e => e.Col95).HasColumnName("col95");

                entity.Property(e => e.Col96).HasColumnName("col96");

                entity.Property(e => e.Col97).HasColumnName("col97");

                entity.Property(e => e.Col98).HasColumnName("col98");

                entity.Property(e => e.Col99).HasColumnName("col99");
            });

            modelBuilder.Entity<FacturasCliente>(entity =>
            {
                entity.HasKey(e => e.IdFacturaCliente);

                entity.HasComment("Facturas de Clientes");

                entity.Property(e => e.IdFacturaCliente).HasColumnName("ID_FacturaCliente");

                entity.Property(e => e.Base).HasColumnType("money");

                entity.Property(e => e.Descuento)
                    .HasColumnType("smallmoney")
                    .HasComment("descuento global del albaran");

                entity.Property(e => e.DescuentoPp)
                    .HasColumnType("smallmoney")
                    .HasColumnName("DescuentoPP");

                entity.Property(e => e.Estado).HasComment("0 -> normal, 1 -> Pagada");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFactura).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdClienteFactura).HasColumnName("ID_ClienteFactura");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NumeroFactura)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.PrecioVenta).HasColumnType("smallmoney");

                entity.Property(e => e.RecargoEq).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("money");
            });

            modelBuilder.Entity<FacturasClientesLinea>(entity =>
            {
                entity.HasKey(e => e.IdFacturaClienteLinea);

                entity.Property(e => e.IdFacturaClienteLinea).HasColumnName("ID_FacturaClienteLinea");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdFacturaCliente).HasColumnName("ID_FacturaCliente");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.ImpuestosPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeBonificado).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeDescuentoCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PrecioVenta).HasColumnType("smallmoney");

                entity.Property(e => e.PuntoVerde).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<FacturasDistribuidora>(entity =>
            {
                entity.HasKey(e => e.IdFacturaDistribuidora);

                entity.HasComment("Facturas emitidas por las distribuidoras");

                entity.Property(e => e.IdFacturaDistribuidora).HasColumnName("ID_FacturaDistribuidora");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFactura).HasColumnType("date");

                entity.Property(e => e.FechaFin).HasColumnType("date");

                entity.Property(e => e.FechaInicio).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdDistribuidora).HasColumnName("ID_Distribuidora");

                entity.Property(e => e.IdTablaPorcentajeDistribuidora).HasColumnName("ID_TablaPorcentajeDistribuidora");

                entity.Property(e => e.NumeroFactura)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<FacturasDistribuidorasLinea>(entity =>
            {
                entity.HasKey(e => e.IdFacturaDistribuidoraLinea);

                entity.Property(e => e.IdFacturaDistribuidoraLinea).HasColumnName("ID_FacturaDistribuidoraLinea");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("descripcion de la linea");

                entity.Property(e => e.Descripcion2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin).HasColumnType("date");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("date")
                    .HasComment("rango de facturacion");

                entity.Property(e => e.IdEspectaculo)
                    .HasColumnName("ID_Espectaculo")
                    .HasComment("espectaculo facturado");

                entity.Property(e => e.IdFacturaDistribuidora).HasColumnName("ID_FacturaDistribuidora");

                entity.Property(e => e.IdFormato).HasColumnName("ID_Formato");

                entity.Property(e => e.IdTablaPorcentajeDistribuidora).HasColumnName("ID_TablaPorcentajeDistribuidora");

                entity.Property(e => e.ImporteEspectador)
                    .HasColumnType("smallmoney")
                    .HasComment("importe a facturar por espectador");

                entity.Property(e => e.Impuestos)
                    .HasColumnType("smallmoney")
                    .HasComment("porcentaje impuestos");

                entity.Property(e => e.Porcentaje)
                    .HasColumnType("smallmoney")
                    .HasComment("porcentaje de recaudacion de la distribuidora");

                entity.Property(e => e.PorcentajeImpuestos)
                    .HasColumnType("smallmoney")
                    .HasComment("porcentaje impuestos");

                entity.Property(e => e.PorcentajeImpuestosTickets).HasColumnType("smallmoney");

                entity.Property(e => e.RecaudacionBruta)
                    .HasColumnType("smallmoney")
                    .HasComment("Recaudacion total del periodo");

                entity.Property(e => e.RecaudacionNeta)
                    .HasColumnType("smallmoney")
                    .HasComment("recaudacion sin impuestos");

                entity.Property(e => e.TipoLinea).HasComment("0 recaudacion, 1 der autor, 2 embalajes, 3 afiches, 4 cargo acarreos, 5 fijo por pelicula, 6 otros");

                entity.Property(e => e.TotalBase)
                    .HasColumnType("smallmoney")
                    .HasComment("resultante de aplicar porcentaje o importe espectador a recaudacionneta o espectadores");

                entity.Property(e => e.TotalLinea)
                    .HasColumnType("smallmoney")
                    .HasComment("total base mas impuestos");
            });

            modelBuilder.Entity<FacturasProveedore>(entity =>
            {
                entity.HasKey(e => e.IdFacturaProveedor);

                entity.HasComment("Facturas de proveedores de barra");

                entity.Property(e => e.IdFacturaProveedor).HasColumnName("ID_FacturaProveedor");

                entity.Property(e => e.Base).HasColumnType("money");

                entity.Property(e => e.Descuento)
                    .HasColumnType("smallmoney")
                    .HasComment("descuento global del albaran");

                entity.Property(e => e.DescuentoPp)
                    .HasColumnType("smallmoney")
                    .HasColumnName("DescuentoPP");

                entity.Property(e => e.Estado).HasComment("0 -> normal, 1 -> Pagada");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFactura).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NumeroFactura)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.RecargoEq).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("money");
            });

            modelBuilder.Entity<FacturasProveedoresAlbarane>(entity =>
            {
                entity.HasKey(e => e.IdFacturaProveedorAlbaran);

                entity.HasComment("Detalla los albaranes qeu forman parte de una factura");

                entity.Property(e => e.IdFacturaProveedorAlbaran).HasColumnName("ID_FacturaProveedorAlbaran");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdAlbaran).HasColumnName("ID_Albaran");

                entity.Property(e => e.IdFactura).HasColumnName("ID_Factura");
            });

            modelBuilder.Entity<FacturasProveedoresLinea>(entity =>
            {
                entity.HasKey(e => e.IdFacturaProveedorLinea);

                entity.Property(e => e.IdFacturaProveedorLinea).HasColumnName("ID_FacturaProveedorLinea");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descuento).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdFactura).HasColumnName("ID_Factura");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.ImpuestosPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeBonificado).HasColumnType("smallmoney");

                entity.Property(e => e.PorcentajeDescuentoCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PrecioCompra).HasColumnType("smallmoney");

                entity.Property(e => e.PuntoVerde).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<FamiliaAnalisi>(entity =>
            {
                entity.HasKey(e => e.IdFamiliaAnalisis);

                entity.ToTable("FamiliaAnalisis");

                entity.Property(e => e.IdFamiliaAnalisis).HasColumnName("ID_FamiliaAnalisis");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdFamiliaPadre).HasColumnName("ID_FamiliaPadre");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Formato>(entity =>
            {
                entity.HasKey(e => e.IdFormato);

                entity.HasComment("Esta tabla recoge los distintos formatos en qe se puede proyectar un espectaculo");

                entity.Property(e => e.IdFormato).HasColumnName("ID_Formato");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Formato3D)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("3D Format, from Formatos3D, nullable");

                entity.Property(e => e.FormatoAudio)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("audio format, from formatosaudio, nullable");

                entity.Property(e => e.FormatoUsoEnImagen).HasComment("format used in image format");

                entity.Property(e => e.FormatoVideo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("video format, from formatosvideo, nullable");

                entity.Property(e => e.LetraVersionAudio)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasComment("en blanco implica audio castellano, doblada o original, si audio original=1 => S(orig cast), si audiorig<>1 =>w");

                entity.Property(e => e.LogoFormato)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("format logo, nullable");

                entity.Property(e => e.NombreFormato)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("Format Name");

                entity.Property(e => e.Subtitulos)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("subtitle format, nullable");

                entity.Property(e => e.Sufijo)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("suffix to apply to the name of film, nullable");

                entity.Property(e => e.VersionAudio)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("audio version, tabla del ministerio, doblada, original cast, original gal ..., from FormatosVersionAudio, nullable");
            });

            modelBuilder.Entity<Formatos3D>(entity =>
            {
                entity.HasKey(e => e.IdFormato3D);

                entity.ToTable("Formatos3D");

                entity.HasComment("Distintos formatos de proyeccion 3D que pueden encontrarse");

                entity.Property(e => e.IdFormato3D).HasColumnName("ID_Formato3D");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.NombreFormato3D)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FormatosAudio>(entity =>
            {
                entity.HasKey(e => e.IdFormatoAudio);

                entity.ToTable("FormatosAudio");

                entity.HasComment("Distintos formatos de sonido que podemos encontrar");

                entity.Property(e => e.IdFormatoAudio).HasColumnName("ID_FormatoAudio");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.NombreFormatoAudio)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FormatosSubtitulo>(entity =>
            {
                entity.HasKey(e => e.IdFormatoSubtitulo);

                entity.Property(e => e.IdFormatoSubtitulo).HasColumnName("ID_FormatoSubtitulo");

                entity.Property(e => e.CaracterFormatoSubtitulo)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasComment("char defining format");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.NombreFormatoSubtitulo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("format name");
            });

            modelBuilder.Entity<FormatosVersionAudio>(entity =>
            {
                entity.HasKey(e => e.IdVersionAudio)
                    .HasName("PK_FormatoVersionAudio");

                entity.ToTable("FormatosVersionAudio");

                entity.HasComment("Hace referencia al idioma del espectaculo");

                entity.Property(e => e.IdVersionAudio).HasColumnName("ID_VersionAudio");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.LetraVersionAudio)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasComment("segun tabla del ministerio");

                entity.Property(e => e.NombreVersionAudio)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("nombre, segun tabla del ministerio");
            });

            modelBuilder.Entity<FormatosVideo>(entity =>
            {
                entity.HasKey(e => e.IdFormatoVideo);

                entity.ToTable("FormatosVideo");

                entity.HasComment("Especifica los Formatos de Imagen, no los 3D, como digital, 35mm, blueray ...");

                entity.Property(e => e.IdFormatoVideo).HasColumnName("ID_FormatoVideo");

                entity.Property(e => e.CaracterFormatoVideo)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasComment("segun tabla del ministerio");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.NombreFormatoVideo)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("Nombre del formato, segun tabla del ministerio");
            });

            modelBuilder.Entity<Genero>(entity =>
            {
                entity.HasKey(e => e.IdGenero);

                entity.HasComment("Generos de los espectaculos");

                entity.Property(e => e.IdGenero).HasColumnName("ID_Genero");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.NombreGenero)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Impuesto>(entity =>
            {
                entity.HasKey(e => e.IdImpuesto);

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.Descripcion1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.NombreImpuesto1)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.NombreImpuesto2)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Porcentaje1).HasColumnType("smallmoney");

                entity.Property(e => e.Porcentaje2).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalArticulosCinesStock>(entity =>
            {
                entity.ToTable("_Local_ArticulosCinesStock");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdArticuloCineStock).HasColumnName("ID_ArticuloCineStock");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.StockActual).HasColumnType("numeric(10, 3)");
            });

            modelBuilder.Entity<LocalCierre>(entity =>
            {
                entity.ToTable("_Local_Cierres");

                entity.HasComment("guarda los cierres de caja de los trabajadores del centro");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descuadre).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEfectiva).HasColumnType("date");

                entity.Property(e => e.FechaHoraCierre).HasColumnType("datetime");

                entity.Property(e => e.Final).HasColumnName("final");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdCierre).HasColumnName("ID_Cierre");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.ImporteDevoluciones).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteEntradaEfectivo).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteMetalico).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteMetalicoBarra).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteMetalicoTaquilla).HasColumnType("smallmoney");

                entity.Property(e => e.ImportePromocional).HasColumnType("smallmoney");

                entity.Property(e => e.ImportePromocionalBarra).HasColumnType("smallmoney");

                entity.Property(e => e.ImportePromocionalTaquilla).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteRecaudado).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteSalidaEfectivo).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteTarjeta).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteTarjetaBarra).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteTarjetaTaquilla).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteVendido).HasColumnType("smallmoney");

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.Remanente).HasColumnType("smallmoney");

                entity.Property(e => e.Retirada).HasColumnType("smallmoney");

                entity.Property(e => e.TotalBilletes).HasColumnType("smallmoney");

                entity.Property(e => e.TotalMonedas).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalConfiguracionesCorreo>(entity =>
            {
                entity.ToTable("_Local_ConfiguracionesCorreos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EnabledSsl).HasColumnName("EnabledSSL");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdConfiguracionCorreo).HasColumnName("ID_ConfiguracionCorreo");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServidorSmtp)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("ServidorSMTP");

                entity.Property(e => e.Usuario)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalControlPresencial>(entity =>
            {
                entity.ToTable("_Local_ControlPresencial");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Comentarios)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Final).HasColumnType("datetime");

                entity.Property(e => e.IdControlPresencial).HasColumnName("ID_ControlPresencial");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Inicio).HasColumnType("datetime");
            });

            modelBuilder.Entity<LocalErrore>(entity =>
            {
                entity.ToTable("_Local_Errores");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHora).HasColumnType("datetime");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdError).HasColumnName("ID_Error");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");
            });

            modelBuilder.Entity<LocalFamilia>(entity =>
            {
                entity.ToTable("_Local_Familias");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdFamilia).HasColumnName("ID_Familia");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MostrarEnPos).HasColumnName("MostrarEnPOS");

                entity.Property(e => e.NombreFamilia)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreFamiliaAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OrdenEnPos).HasColumnName("OrdenEnPOS");

                entity.Property(e => e.TipoMovForSync).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<LocalFamiliasArticulo>(entity =>
            {
                entity.ToTable("_Local_FamiliasArticulos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdFamilia).HasColumnName("ID_Familia");

                entity.Property(e => e.IdFamiliaArticulo).HasColumnName("ID_FamiliaArticulo");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoMovForSync).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<LocalFestivo>(entity =>
            {
                entity.ToTable("_Local_Festivos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFestivo).HasColumnType("date");

                entity.Property(e => e.IdFestivo).HasColumnName("ID_Festivo");
            });

            modelBuilder.Entity<LocalInventario>(entity =>
            {
                entity.ToTable("_Local_Inventarios");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaInventario).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdInventario).HasColumnName("ID_Inventario");

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalInventariosLinea>(entity =>
            {
                entity.ToTable("_Local_InventariosLineas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CantidadEstimada)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CantidadReal)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdInventario).HasColumnName("ID_Inventario");

                entity.Property(e => e.IdInventarioLinea).HasColumnName("ID_InventarioLinea");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.ValoracionCompra).HasColumnType("smallmoney");

                entity.Property(e => e.ValoracionVenta).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMacro>(entity =>
            {
                entity.ToTable("_Local_Macros");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin).HasColumnType("date");

                entity.Property(e => e.FechaInicio).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdMacro).HasColumnName("ID_Macro");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMacrosArticulo>(entity =>
            {
                entity.ToTable("_Local_MacrosArticulos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdMacro).HasColumnName("ID_Macro");

                entity.Property(e => e.IdMacroArticulo).HasColumnName("ID_MacroArticulo");

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMacrosPromo>(entity =>
            {
                entity.ToTable("_Local_MacrosPromos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdMacro).HasColumnName("ID_Macro");

                entity.Property(e => e.IdMacroPromo).HasColumnName("ID_MacroPromo");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");
            });

            modelBuilder.Entity<LocalMensajesPromocionale>(entity =>
            {
                entity.ToTable("_Local_MensajesPromocionales");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin).HasColumnType("date");

                entity.Property(e => e.FechaInicio).HasColumnType("date");

                entity.Property(e => e.IdMensajesPromocionales).HasColumnName("ID_MensajesPromocionales");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Texto).IsUnicode(false);
            });

            modelBuilder.Entity<LocalMovimientosAlmacen>(entity =>
            {
                entity.ToTable("_Local_MovimientosAlmacen");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EntradaSalida)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraMovimiento).HasColumnType("datetime");

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdInventario).HasColumnName("ID_Inventario");

                entity.Property(e => e.IdLineaVenta).HasColumnName("ID_LineaVenta");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.PrecioUnidad).HasColumnType("smallmoney");

                entity.Property(e => e.Total).HasColumnType("smallmoney");

                entity.Property(e => e.Unidades).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMovimientosCajaCine>(entity =>
            {
                entity.ToTable("_Local_MovimientosCajaCine");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraMovimiento).HasColumnType("datetime");

                entity.Property(e => e.FechaValidez).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdCierre).HasColumnName("ID_Cierre");

                entity.Property(e => e.IdMovimientoCajaCine).HasColumnName("ID_MovimientoCajaCine");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMovimientosCajaPv>(entity =>
            {
                entity.ToTable("_Local_MovimientosCajaPV");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraMovimiento).HasColumnType("datetime");

                entity.Property(e => e.FechaValidez).HasColumnType("date");

                entity.Property(e => e.IdCierre).HasColumnName("ID_Cierre");

                entity.Property(e => e.IdMovimientoCajaPv).HasColumnName("ID_MovimientoCajaPV");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMovimientosPromosCine>(entity =>
            {
                entity.ToTable("_Local_MovimientosPromosCine");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraMovimiento).HasColumnType("datetime");

                entity.Property(e => e.FechaValidez).HasColumnType("date");

                entity.Property(e => e.IdCierre).HasColumnName("ID_Cierre");

                entity.Property(e => e.IdMovimientoPromoCine).HasColumnName("ID_MovimientoPromoCine");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalMovimientosTpv>(entity =>
            {
                entity.ToTable("_Local_MovimientosTPV");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraMovimiento).HasColumnType("datetime");

                entity.Property(e => e.FechaValidez).HasColumnType("date");

                entity.Property(e => e.IdCierre).HasColumnName("ID_Cierre");

                entity.Property(e => e.IdMovimientoTpv).HasColumnName("ID_MovimientoTPV");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalObservacione>(entity =>
            {
                entity.ToTable("_Local_Observaciones");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Detalle).IsUnicode(false);

                entity.Property(e => e.DiaObservacion).HasColumnType("date");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHora).HasColumnType("datetime");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdObservacion).HasColumnName("ID_Observacion");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");
            });

            modelBuilder.Entity<LocalPase>(entity =>
            {
                entity.ToTable("_Local_Pases");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CodigoInesala)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CodigoINESala");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Franja)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HoraCine).HasColumnType("datetime");

                entity.Property(e => e.HoraReal).HasColumnType("datetime");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdEspectaculo).HasColumnName("ID_Espectaculo");

                entity.Property(e => e.IdFormato).HasColumnName("ID_Formato");

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdSala).HasColumnName("ID_Sala");

                entity.Property(e => e.ImagenPase)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogoFormato)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ncopia)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("NCopia");

                entity.Property(e => e.NombreEspectaculo)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.NombreFormato)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.NombreSala)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalPreciosPasesZona>(entity =>
            {
                entity.ToTable("_Local_PreciosPasesZonas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdPrecio).HasColumnName("ID_Precio");

                entity.Property(e => e.IdPrecioPaseZona).HasColumnName("ID_PrecioPaseZona");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.PrecioApp).HasColumnName("PrecioAPP");
            });

            modelBuilder.Entity<LocalPreciosTemporada>(entity =>
            {
                entity.ToTable("_Local_PreciosTemporadas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPrecio).HasColumnName("ID_Precio");

                entity.Property(e => e.IdPrecioTemporada).HasColumnName("ID_PrecioTemporada");

                entity.Property(e => e.IdTemporada).HasColumnName("ID_Temporada");
            });

            modelBuilder.Entity<LocalPuntosVentaFisico>(entity =>
            {
                entity.ToTable("_Local_PuntosVentaFisicos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BalanzaConexion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ComandoAperturaCajon)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ComandoGuillotina)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionRedImpresora)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPuntoVentaFisico).HasColumnName("ID_PuntoVentaFisico");

                entity.Property(e => e.IdTeamviewer)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("ID_Teamviewer");

                entity.Property(e => e.Mac)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("MAC");

                entity.Property(e => e.NombreFisico)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadClaveFirma)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadCodComercio)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadCodTerminal)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadConfigPuerto)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadNombreBanco)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadNumeroSerie)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadVersion)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrinterLabel)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PrinterTickets)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PuertoImpresora)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.PuertoSerieCajon)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.TipoBalanza)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.VisorFinLinea)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VisorInicializar)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VisorLineaInferior)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VisorLineaSuperior)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VisorPuerto)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalPuntosVentaPinpad>(entity =>
            {
                entity.ToTable("_Local_PuntosVentaPinpads");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPuntoVentaPinpad).HasColumnName("ID_PuntoVentaPinpad");

                entity.Property(e => e.PinpadClaveFirma)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadCodComercio)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadCodTerminal)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadConfigPuerto)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadNombreBanco)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadNumeroSerie)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.PinpadVersion)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalReimpresione>(entity =>
            {
                entity.ToTable("_Local_Reimpresiones");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraReimpresion).HasColumnType("datetime");

                entity.Property(e => e.IdReimpresion).HasColumnName("ID_Reimpresion");

                entity.Property(e => e.IdUsuarioReimpresion).HasColumnName("ID_UsuarioReimpresion");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalReimpresionesObservacione>(entity =>
            {
                entity.ToTable("_Local_ReimpresionesObservaciones");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdReimpresionObservacion).HasColumnName("ID_ReimpresionObservacion");

                entity.Property(e => e.Texto)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalReserva>(entity =>
            {
                entity.ToTable("_Local_Reservas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHora).HasColumnType("datetime");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdReserva).HasColumnName("ID_Reserva");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombrePuntoVenta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreUsuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenciaCompra)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalReservasLinea>(entity =>
            {
                entity.ToTable("_Local_ReservasLineas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoReserva)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraAnulacion).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraVenta).HasColumnType("datetime");

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdPrecio).HasColumnName("ID_Precio");

                entity.Property(e => e.IdReserva).HasColumnName("ID_Reserva");

                entity.Property(e => e.IdReservaLinea).HasColumnName("ID_ReservaLinea");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.NombrePrecio)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalReubicacione>(entity =>
            {
                entity.ToTable("_Local_Reubicaciones");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaReubicacion).HasColumnType("datetime");

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdReubicacion).HasColumnName("ID_Reubicacion");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.Observaciones).IsUnicode(false);
            });

            modelBuilder.Entity<LocalReubicacionesLinea>(entity =>
            {
                entity.ToTable("_Local_ReubicacionesLineas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdReubicacion).HasColumnName("ID_Reubicacion");

                entity.Property(e => e.IdReubicacionLinea).HasColumnName("ID_ReubicacionLinea");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");
            });

            modelBuilder.Entity<LocalRollosManuale>(entity =>
            {
                entity.ToTable("_Local_RollosManuales");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdRolloManual).HasColumnName("ID_RolloManual");

                entity.Property(e => e.IdSala).HasColumnName("ID_Sala");
            });

            modelBuilder.Entity<LocalSocio>(entity =>
            {
                entity.ToTable("_Local_Socios");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Apellidos)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoSocio)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdSocio).HasColumnName("ID_Socio");

                entity.Property(e => e.Localidad)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Provincia)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Tlfno)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalTemporada>(entity =>
            {
                entity.ToTable("_Local_Temporadas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin).HasColumnType("date");

                entity.Property(e => e.FechaInicio).HasColumnType("date");

                entity.Property(e => e.IdTemporada).HasColumnName("ID_Temporada");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.NombreTemporada)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalTemporadasLinea>(entity =>
            {
                entity.ToTable("_Local_TemporadasLineas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdTemporada).HasColumnName("ID_Temporada");

                entity.Property(e => e.IdTemporadaLinea).HasColumnName("ID_TemporadaLinea");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");
            });

            modelBuilder.Entity<LocalTemporadasSocio>(entity =>
            {
                entity.ToTable("_Local_TemporadasSocios");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPrecio).HasColumnName("ID_Precio");

                entity.Property(e => e.IdSala).HasColumnName("ID_Sala");

                entity.Property(e => e.IdSocio).HasColumnName("ID_Socio");

                entity.Property(e => e.IdTemporada).HasColumnName("ID_Temporada");

                entity.Property(e => e.IdTemporadaSocio).HasColumnName("ID_TemporadaSocio");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalVenta>(entity =>
            {
                entity.ToTable("_Local_Ventas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoPromocionNominal)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHora).HasColumnType("datetime");

                entity.Property(e => e.IdAnulacion).HasColumnName("ID_Anulacion");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdCierre).HasColumnName("ID_Cierre");

                entity.Property(e => e.IdFactura).HasColumnName("ID_Factura");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NombrePuntoVenta)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreUsuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PorcDescuento).HasColumnType("smallmoney");

                entity.Property(e => e.ReferenciaCompra)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Total).HasColumnType("smallmoney");

                entity.Property(e => e.TotalDescuento).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalVentasLinea>(entity =>
            {
                entity.ToTable("_Local_VentasLineas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Aimprimir).HasColumnName("AImprimir");

                entity.Property(e => e.ApagarCliente)
                    .HasColumnType("smallmoney")
                    .HasColumnName("APagarCliente");

                entity.Property(e => e.Cantidad).HasColumnType("smallmoney");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoEntrada)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraAnulacion).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraUso).HasColumnType("datetime");

                entity.Property(e => e.IdArticuloBarra).HasColumnName("ID_ArticuloBarra");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdPrecio).HasColumnName("ID_Precio");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");

                entity.Property(e => e.IdUsuarioAnulacion).HasColumnName("ID_UsuarioAnulacion");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.IdVentaLinea).HasColumnName("ID_VentaLinea");

                entity.Property(e => e.IdVentaManual).HasColumnName("ID_VentaManual");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NombreArticuloBarra)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombrePrecio)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombrePromocion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.Precio).HasColumnType("smallmoney");

                entity.Property(e => e.PrecioCosteArticulo).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalVentasManuale>(entity =>
            {
                entity.ToTable("_Local_VentasManuales");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraVenta).HasColumnType("datetime");

                entity.Property(e => e.IdPase).HasColumnName("ID_Pase");

                entity.Property(e => e.IdRolloManual).HasColumnName("ID_RolloManual");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.IdVentaManual).HasColumnName("ID_VentaManual");

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalVentasPago>(entity =>
            {
                entity.ToTable("_Local_VentasPagos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdPago).HasColumnName("ID_Pago");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.IdVale).HasColumnName("ID_Vale");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");

                entity.Property(e => e.NautorizacionBancaria)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("NAutorizacionBancaria");

                entity.Property(e => e.ObservacionDevolucion)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PedidoPinpad)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rtspinpad)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RTSPinpad");

                entity.Property(e => e.TarjetaRecibo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ValidacionPromocion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.XmlResp)
                    .IsUnicode(false)
                    .HasColumnName("xmlResp");
            });

            modelBuilder.Entity<LocalVentasSuplemento>(entity =>
            {
                entity.ToTable("_Local_VentasSuplementos");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.IdVentaSuplemento).HasColumnName("ID_VentaSuplemento");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<LocalZonasCentro>(entity =>
            {
                entity.ToTable("_Local_ZonasCentros");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdZonaCentro).HasColumnName("ID_ZonaCentro");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LocalZonasCentrosPuntosVentum>(entity =>
            {
                entity.ToTable("_Local_ZonasCentrosPuntosVenta");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdZonaCentro).HasColumnName("ID_ZonaCentro");

                entity.Property(e => e.IdZonaPuntoVenta).HasColumnName("ID_ZonaPuntoVenta");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.ToTable("Permission");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("code");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("description");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Precio>(entity =>
            {
                entity.HasKey(e => e.IdPrecio);

                entity.Property(e => e.IdPrecio).HasColumnName("ID_Precio");

                entity.Property(e => e.Activado).HasComment("price available to use");

                entity.Property(e => e.Codigo).HasComment("price code");

                entity.Property(e => e.CodigoGeneralitat)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("code apply to price in goverment report");

                entity.Property(e => e.ComisionFacturacionCanales)
                    .HasColumnType("smallmoney")
                    .HasComment("comision applied to online ticket by ticketing company");

                entity.Property(e => e.ComisionFijaPrecio)
                    .HasColumnType("smallmoney")
                    .HasComment("applied fixed comision to the price");

                entity.Property(e => e.ComisionPorcentajePrecio)
                    .HasColumnType("smallmoney")
                    .HasComment("applied percent comision to the price");

                entity.Property(e => e.ExplicacionUsoPrecio)
                    .IsUnicode(false)
                    .HasComment("price description");

                entity.Property(e => e.FechaActivacion)
                    .HasColumnType("date")
                    .HasComment("activation day");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCambio).HasColumnType("date");

                entity.Property(e => e.FechaDesactivacion)
                    .HasColumnType("date")
                    .HasComment("deactivation date");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("tax applied to the price");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("image linked to the price");

                entity.Property(e => e.Importe)
                    .HasColumnType("smallmoney")
                    .HasComment("price value");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("price name");

                entity.Property(e => e.NuevoImporte)
                    .HasColumnType("smallmoney")
                    .HasComment("new programmed price value");

                entity.Property(e => e.NumEntradasNecesarias).HasComment("amount of seat needed to apply the price");

                entity.Property(e => e.PrecioApp)
                    .HasColumnName("PrecioAPP")
                    .HasComment("price available for app");

                entity.Property(e => e.PrecioKiosko).HasComment("price available for kiosk ");

                entity.Property(e => e.TextoPantalla)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("name of price to show on screens");
            });

            modelBuilder.Entity<PreciosCine>(entity =>
            {
                entity.HasKey(e => e.IdPrecioCine);

                entity.Property(e => e.IdPrecioCine).HasColumnName("ID_PrecioCine");

                entity.Property(e => e.CentroExcluido).HasComment("cinema  where price is not available");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCambio).HasColumnType("date");

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("link to centros table");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("link to impuestos table");

                entity.Property(e => e.IdPrecio)
                    .HasColumnName("ID_Precio")
                    .HasComment("link to precios table");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");

                entity.Property(e => e.NuevoImporte)
                    .HasColumnType("smallmoney")
                    .HasComment("new price value is update is pending");
            });

            modelBuilder.Entity<Promocione>(entity =>
            {
                entity.HasKey(e => e.IdPromocion);

                entity.HasComment("Detalla las promociones disponibles para aplicar a la venta");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.Acumulable).HasComment("Cumulative Promo");

                entity.Property(e => e.DetallarComoPrecio).HasComment("show promo in reports as price");

                entity.Property(e => e.DtoEuros)
                    .HasColumnType("smallmoney")
                    .HasComment("money dto to apply");

                entity.Property(e => e.DtoPorcentaje)
                    .HasColumnType("smallmoney")
                    .HasComment("dto percent to apply");

                entity.Property(e => e.DtoPrecioFijo)
                    .HasColumnType("smallmoney")
                    .HasComment("fixed price to apply");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaFin)
                    .HasColumnType("date")
                    .HasComment("final date");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("date")
                    .HasComment("start date");

                entity.Property(e => e.IdArticulo)
                    .HasColumnName("ID_Articulo")
                    .HasComment("promo available for one item");

                entity.Property(e => e.IdArticuloNecesario)
                    .HasColumnName("ID_ArticuloNecesario")
                    .HasComment("item necessary to apply promo");

                entity.Property(e => e.IdEspectaculo)
                    .HasColumnName("ID_Espectaculo")
                    .HasComment("promo available for one film");

                entity.Property(e => e.IdImpuesto)
                    .HasColumnName("ID_Impuesto")
                    .HasComment("tax applied in promo");

                entity.Property(e => e.IdProveedor)
                    .HasColumnName("ID_Proveedor")
                    .HasComment("link to proveedores table");

                entity.Property(e => e.ImprimirComprobante).HasComment("print promo ticket");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("promo´s name");

                entity.Property(e => e.NumeroActual).HasComment("actual number of promo tickets");

                entity.Property(e => e.NumeroMaximo).HasComment("max number of tickets available");

                entity.Property(e => e.OrdenMostrar).HasComment("order to show on list");

                entity.Property(e => e.PrecioArticulo)
                    .HasColumnType("smallmoney")
                    .HasComment("item price in promo");

                entity.Property(e => e.PromoConChequeoOnline).HasComment("Promo with online check");

                entity.Property(e => e.PromoConValorContableDistintoDelPrecio).HasComment("promo with account value different of client price value");

                entity.Property(e => e.PromoReserva).HasComment("promo not to sell, just to reserve");

                entity.Property(e => e.PromoVenta).HasComment("promo available to sell");

                entity.Property(e => e.PromocionAutomatica).HasComment("promo applied when conditions are true");

                entity.Property(e => e.PromocionNominal).HasComment("when promo is used ask for name of client");

                entity.Property(e => e.PromocionOnline).HasComment("promo available online");

                entity.Property(e => e.PromocionaSuplementos).HasComment("dto applied to supplements");

                entity.Property(e => e.TipoPromocion).HasComment("promo type: 0 percent dto, 1 apply fixed price, 2 money dto");

                entity.Property(e => e.UnidadesNecesarias).HasComment("amount of item necessary to apply promo");

                entity.Property(e => e.ValeInterno).HasComment("internal promo");

                entity.Property(e => e.ValidaEnBarra).HasComment("promo available for refreshments");

                entity.Property(e => e.ValidaEnEntradas).HasComment("promo available for tickets");

                entity.Property(e => e.ValorContable)
                    .HasColumnType("smallmoney")
                    .HasComment("price value for the cinema account");

                entity.Property(e => e.XmlRespuesta).HasColumnType("xml");
            });

            modelBuilder.Entity<PromocionesCine>(entity =>
            {
                entity.HasKey(e => e.IdPromocionCine);

                entity.Property(e => e.IdPromocionCine).HasColumnName("ID_PromocionCine");

                entity.Property(e => e.CentroExcluido).HasComment("Promo not available in this Cinema ");

                entity.Property(e => e.DtoEuros).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPorcentaje).HasColumnType("smallmoney");

                entity.Property(e => e.DtoPrecioFijo).HasColumnType("smallmoney");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.PrecioArticulo).HasColumnType("smallmoney");

                entity.Property(e => e.ValorContable).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<Proveedore>(entity =>
            {
                entity.HasKey(e => e.IdProveedor);

                entity.HasComment("proveedores de articulos barra");

                entity.Property(e => e.IdProveedor).HasColumnName("ID_Proveedor");

                entity.Property(e => e.Cif)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("CIF")
                    .HasComment("Tax Id");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP")
                    .HasComment("postcode");

                entity.Property(e => e.CuentaContable)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("account for accounting");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("address");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Localidad)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("city");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Name");

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.Pcontacto)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PContacto")
                    .HasComment("person to contact");

                entity.Property(e => e.Provincia)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasComment("state");

                entity.Property(e => e.Tlfno1)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasComment("phone");

                entity.Property(e => e.WebSite)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Sala>(entity =>
            {
                entity.HasKey(e => e.IdSala);

                entity.HasComment("Salas pertenecientes a cada centro");

                entity.Property(e => e.IdSala).HasColumnName("ID_Sala");

                entity.Property(e => e.Aforo).HasComment("room´s gauging ");

                entity.Property(e => e.CodigoIne)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CodigoINE")
                    .HasComment("room´s code for goverment");

                entity.Property(e => e.CodigoSala)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasComment("room´s code");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("room´s cinema");

                entity.Property(e => e.NombreSala)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("room´s name");

                entity.Property(e => e.Nplantas)
                    .HasColumnName("NPlantas")
                    .HasComment("floor´s number");

                entity.Property(e => e.SubNombreCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("subname of the room");
            });

            modelBuilder.Entity<SalasZona>(entity =>
            {
                entity.HasKey(e => e.IdZonaSala)
                    .HasName("PK_ZonasSalas");

                entity.HasComment("Zonas en que se divide una Sala");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");

                entity.Property(e => e.Aforo).HasComment("gauging for the zone");

                entity.Property(e => e.Asientos)
                    .IsUnicode(false)
                    .HasComment("string with the seat type of every seat, 3 digits per seat, this helps us to show the seat map");

                entity.Property(e => e.Calidades)
                    .IsUnicode(false)
                    .HasComment("cadena que contiene la calidad del asiento de cada celda de la parrilla, 3 digitos por butaca");

                entity.Property(e => e.Columnas).HasComment("number of seat cols");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Filas).HasComment("number of seats rows");

                entity.Property(e => e.IdSala)
                    .HasColumnName("ID_Sala")
                    .HasComment("link to salas");

                entity.Property(e => e.NombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("name of the zone");

                entity.Property(e => e.Numero).HasComment("number of the room´s zone");

                entity.Property(e => e.NumerosAsientos)
                    .IsUnicode(false)
                    .HasComment("cadena que contiene los numeros de cada una de las butacas, el numero que sustituye a numero columna");

                entity.Property(e => e.NumerosColumnas)
                    .IsUnicode(false)
                    .HasComment("string with the number of the cols in the room´s zone. This will be the default value for the seats, even we can change it later, and will be save in numerosasientos");

                entity.Property(e => e.NumerosFilas)
                    .IsUnicode(false)
                    .HasComment("string with the number of the room´s zone rows");
            });

            modelBuilder.Entity<SeriesTicket>(entity =>
            {
                entity.HasKey(e => e.IdSerieTicket);

                entity.Property(e => e.IdSerieTicket).HasColumnName("ID_SerieTicket");

                entity.Property(e => e.SerieBarra)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SerieMermas)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SerieSuplementos)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SerieTaquilla)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Suplemento>(entity =>
            {
                entity.HasKey(e => e.IdSuplemento);

                entity.HasComment("Suplementos displonibles");

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<SuplementosButaca>(entity =>
            {
                entity.HasKey(e => e.IdSuplementoButaca);

                entity.Property(e => e.IdSuplementoButaca).HasColumnName("ID_SuplementoButaca");

                entity.Property(e => e.ColumnaMatriz).HasComment("to link with a seat, refer to the col");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FilaMatriz).HasComment("to link with a seat, refer to the row");

                entity.Property(e => e.IdSuplemento)
                    .HasColumnName("ID_Suplemento")
                    .HasComment("link to suplementos table");

                entity.Property(e => e.IdZonaSala).HasColumnName("ID_ZonaSala");
            });

            modelBuilder.Entity<SuplementosCine>(entity =>
            {
                entity.HasKey(e => e.IdSuplementoCine);

                entity.Property(e => e.IdSuplementoCine).HasColumnName("ID_SuplementoCine");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdImpuesto).HasColumnName("ID_Impuesto");

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");

                entity.Property(e => e.Precio).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<SuplementosEspectaculo>(entity =>
            {
                entity.HasKey(e => e.IdSuplementoEspectaculo);

                entity.Property(e => e.IdSuplementoEspectaculo).HasColumnName("ID_SuplementoEspectaculo");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdEspectaculo).HasColumnName("ID_Espectaculo");

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");
            });

            modelBuilder.Entity<SuplementosFormato>(entity =>
            {
                entity.HasKey(e => e.IdSuplementoFormato)
                    .HasName("PK_SuplementosFormatoss");

                entity.Property(e => e.IdSuplementoFormato).HasColumnName("ID_SuplementoFormato");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdFormato).HasColumnName("ID_Formato");

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");
            });

            modelBuilder.Entity<SuplementosPrecio>(entity =>
            {
                entity.HasKey(e => e.IdSuplementoPrecio);

                entity.Property(e => e.IdSuplementoPrecio).HasColumnName("ID_SuplementoPrecio");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdPrecio)
                    .HasColumnName("ID_Precio")
                    .HasComment("link to precios table");

                entity.Property(e => e.IdSuplemento)
                    .HasColumnName("ID_Suplemento")
                    .HasComment("link to suplementos table");
            });

            modelBuilder.Entity<TablasPorcentajeDistribuidora>(entity =>
            {
                entity.HasKey(e => e.IdTablaPorcentajeDistribuidora)
                    .HasName("PK_TablasPorcentajeEspectadores");

                entity.Property(e => e.IdTablaPorcentajeDistribuidora).HasColumnName("ID_TablaPorcentajeDistribuidora");

                entity.Property(e => e.Categoria)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Name of the category ");

                entity.Property(e => e.FacturacionPorcentaje).HasComment("Applied by percent or by fixed price per ticket");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdDistribuidora)
                    .HasColumnName("ID_Distribuidora")
                    .HasComment("Link to Distribuidoras Table");

                entity.Property(e => e.Semana1).HasColumnType("smallmoney");

                entity.Property(e => e.Semana10).HasColumnType("smallmoney");

                entity.Property(e => e.Semana2)
                    .HasColumnType("smallmoney")
                    .HasComment("value on second week");

                entity.Property(e => e.Semana3).HasColumnType("smallmoney");

                entity.Property(e => e.Semana4).HasColumnType("smallmoney");

                entity.Property(e => e.Semana5).HasColumnType("smallmoney");

                entity.Property(e => e.Semana6).HasColumnType("smallmoney");

                entity.Property(e => e.Semana7).HasColumnType("smallmoney");

                entity.Property(e => e.Semana8).HasColumnType("smallmoney");

                entity.Property(e => e.Semana9).HasColumnType("smallmoney");

                entity.Property(e => e.SemanaPro)
                    .HasColumnType("smallmoney")
                    .HasComment("value over 10 weeks");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("Type");
            });

            modelBuilder.Entity<TarjetasPrepago>(entity =>
            {
                entity.HasKey(e => e.IdTarjeta);

                entity.ToTable("TarjetasPrepago");

                entity.Property(e => e.IdTarjeta).HasColumnName("ID_Tarjeta");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("0 -> todos");

                entity.Property(e => e.IdClienteCadena).HasColumnName("ID_ClienteCadena");

                entity.Property(e => e.ImporteConsumido).HasColumnType("smallmoney");

                entity.Property(e => e.ImporteTotal).HasColumnType("smallmoney");

                entity.Property(e => e.TipoTarjeta).HasComment("0 por entradas, 1 por importe");
            });

            modelBuilder.Entity<TarjetasPrepagoOperacione>(entity =>
            {
                entity.HasKey(e => e.IdTarjetaPrepagoOperacion);

                entity.Property(e => e.IdTarjetaPrepagoOperacion).HasColumnName("ID_TarjetaPrepagoOperacion");

                entity.Property(e => e.CanalUso).HasComment("canal uso, 1 taquilla, 2 kiosko, 3 internet");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraOperacion).HasColumnType("datetime");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdTarjetaPrepago).HasColumnName("ID_TarjetaPrepago");

                entity.Property(e => e.IdVenta)
                    .HasColumnName("ID_Venta")
                    .HasComment("si la operacion es de consumo 0, si es de recarga se indica el id_venta");

                entity.Property(e => e.PrecioEntradas)
                    .HasColumnType("smallmoney")
                    .HasComment("Canal de pago, 1 metalico, 2 tarjeta presencial, 3 tarjeta virtual, 4 tarjetas prepago");
            });

            modelBuilder.Entity<TipoUsuario>(entity =>
            {
                entity.HasKey(e => e.IdTipoUsuarios);

                entity.Property(e => e.IdTipoUsuarios).HasColumnName("ID_TipoUsuarios");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.PedirCp).HasColumnName("PedirCP");

                entity.Property(e => e.Permisos).IsUnicode(false);
            });

            modelBuilder.Entity<TipoUsuariosPermission>(entity =>
            {
                entity.ToTable("TipoUsuariosPermission");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdPermission).HasColumnName("ID_Permission");

                entity.Property(e => e.IdTipoUsuario).HasColumnName("ID_TipoUsuario");
            });

            modelBuilder.Entity<TiposButaca>(entity =>
            {
                entity.HasKey(e => e.IdTipoButaca);

                entity.HasComment("Tipos de butacas disponibles para el diseño de sales");

                entity.Property(e => e.IdTipoButaca)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID_TipoButaca");

                entity.Property(e => e.CodigoButaca)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasComment("3 digits code for a seat type");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IconoButaca)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("seat icon");

                entity.Property(e => e.NombreTipoButaca)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("name of the seat type");

                entity.Property(e => e.TipoCambiable).HasComment("indica si es un tipo de butaca que pueda cambiarse una vez los pases esten a la venta");

                entity.Property(e => e.Vendible).HasComment("seat available to sell");
            });

            modelBuilder.Entity<TiposButacasEstado>(entity =>
            {
                entity.HasKey(e => e.IdTipoButacaEstado);

                entity.ToTable("TiposButacasEstado");

                entity.Property(e => e.IdTipoButacaEstado)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID_TipoButacaEstado");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IconoButaca)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreEstado)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Traspaso>(entity =>
            {
                entity.HasKey(e => e.IdTraspasos)
                    .HasName("PK_Traspaso");

                entity.Property(e => e.IdTraspasos).HasColumnName("ID_Traspasos");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasComment("0 - Pendiente , 1 - Aceptador , 2 - Rechazado");

                entity.Property(e => e.FechaAceptado).HasColumnType("date");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaTraspaso).HasColumnType("date");

                entity.Property(e => e.IdCentroDestino).HasColumnName("ID_CentroDestino");

                entity.Property(e => e.IdCentroOrigen).HasColumnName("ID_CentroOrigen");

                entity.Property(e => e.NumeroTraspaso)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);
            });

            modelBuilder.Entity<TraspasosLinea>(entity =>
            {
                entity.HasKey(e => e.IdTraspasoLinea);

                entity.Property(e => e.IdTraspasoLinea).HasColumnName("ID_TraspasoLinea");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdArticulo).HasColumnName("ID_Articulo");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.IdTraspaso).HasColumnName("ID_Traspaso");

                entity.Property(e => e.Unidades).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Imagen)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PedirCp).HasColumnName("PedirCP");

                entity.Property(e => e.Permisos)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WebPass)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UsuariosCentro>(entity =>
            {
                entity.HasKey(e => e.IdUsuarioCentro);

                entity.Property(e => e.IdUsuarioCentro).HasColumnName("ID_UsuarioCentro");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");
            });

            modelBuilder.Entity<UsuariosPermission>(entity =>
            {
                entity.ToTable("UsuariosPermission");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.PermissionId).HasColumnName("permission_id");
            });

            modelBuilder.Entity<ValesPago>(entity =>
            {
                entity.HasKey(e => e.IdValePago);

                entity.Property(e => e.IdValePago).HasColumnName("ID_ValePago");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoValeAntiguo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaUsado).HasColumnType("datetime");

                entity.Property(e => e.FechaVale).HasColumnType("datetime");

                entity.Property(e => e.FechaValidez).HasColumnType("datetime");

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");

                entity.Property(e => e.IdCliente).HasColumnName("ID_Cliente");

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.IdPuntoVenta).HasColumnName("ID_PuntoVenta");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.Importe).HasColumnType("smallmoney");
            });

            modelBuilder.Entity<VentasLineasTemp>(entity =>
            {
                entity.HasKey(e => e.IdVentaLinea);

                entity.ToTable("VentasLineasTemp");

                entity.HasComment("Cada elemento que se incluye en la venta tiene un registro en esta tabla");

                entity.Property(e => e.IdVentaLinea).HasColumnName("ID_VentaLinea");

                entity.Property(e => e.Aimprimir).HasColumnName("AImprimir");

                entity.Property(e => e.Anticipada).HasComment("Especifica si una Venta es anticipada o no");

                entity.Property(e => e.ApagarCliente)
                    .HasColumnType("smallmoney")
                    .HasColumnName("APagarCliente")
                    .HasComment("Cantidad a pagar por el cliente, una vez aplicados los descuentos");

                entity.Property(e => e.Butaca)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasComment("butaca segun el centro");

                entity.Property(e => e.Cantidad).HasColumnType("smallmoney");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .HasComment("Codigo de barras asignado a cada entrada");

                entity.Property(e => e.CodigoEntrada)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("Codigo asignado a una entrada, ver documentacion ministerio");

                entity.Property(e => e.ColumnaMatriz).HasComment("columna en la cuadricula de la zonasala");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHoraAnulacion).HasColumnType("datetime");

                entity.Property(e => e.FechaHoraUso)
                    .HasColumnType("datetime")
                    .HasComment("fecha y hora de uso de la entrada");

                entity.Property(e => e.Fila)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasComment("fila segun el centro");

                entity.Property(e => e.FilaMatriz).HasComment("fila en la cuadricula de la zonasala");

                entity.Property(e => e.IdArticuloBarra)
                    .HasColumnName("ID_ArticuloBarra")
                    .HasComment("Articulo de barra vendido en esta linea concreta, enlaza con ID_Articulo");

                entity.Property(e => e.IdFactura).HasColumnName("ID_Factura");

                entity.Property(e => e.IdMovimientoAlmacen).HasColumnName("ID_MovimientoAlmacen");

                entity.Property(e => e.IdPase)
                    .HasColumnName("ID_Pase")
                    .HasComment("si es una venta de entrada, especifica el pase al que pertenece, enlaza con Pases");

                entity.Property(e => e.IdPrecio)
                    .HasColumnName("ID_Precio")
                    .HasComment("Precio de la venta de la entrada, enlaza con Precios");

                entity.Property(e => e.IdPromocion)
                    .HasColumnName("ID_Promocion")
                    .HasComment("promocion que se aplica a la venta, enlaza con Promociones");

                entity.Property(e => e.IdSuplemento).HasColumnName("ID_Suplemento");

                entity.Property(e => e.IdUsuarioAnulacion).HasColumnName("ID_UsuarioAnulacion");

                entity.Property(e => e.IdVenta)
                    .HasColumnName("ID_Venta")
                    .HasComment("Venta a la que pertenece la linea, enlaza con VentasTemp");

                entity.Property(e => e.IdVentaManual)
                    .HasColumnName("ID_VentaManual")
                    .HasComment("a que grupo de venta manual pertenece esta venta");

                entity.Property(e => e.IdZonaSala)
                    .HasColumnName("ID_ZonaSala")
                    .HasComment("especifica la ZonaSala de la venta, enlaza con ZonasSalas");

                entity.Property(e => e.Impuestos)
                    .HasColumnType("smallmoney")
                    .HasComment("Importe de impuestos");

                entity.Property(e => e.Manual).HasComment("indica si es una venta manual");

                entity.Property(e => e.NombreArticuloBarra)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("nombre del articulo barra vendido, heredado de Articulos");

                entity.Property(e => e.NombrePrecio)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("heredado de Precios");

                entity.Property(e => e.NombrePromocion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("heredado de Promociones");

                entity.Property(e => e.NombreZona)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("heredado de ZonasSalas");

                entity.Property(e => e.NumeroZona).HasComment("heredado de ZonasSalas");

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.Precio)
                    .HasColumnType("smallmoney")
                    .HasComment("Importe, heredado de Precios");

                entity.Property(e => e.PrecioCosteArticulo).HasColumnType("smallmoney");

                entity.Property(e => e.TipoLinea).HasComment("tipo de articulo incluido en esta linea, 1 entrada, 2 articulobarra");

                entity.Property(e => e.TipoMovForSync).HasDefaultValueSql("((1))");

                entity.Property(e => e.Utilizada).HasComment("si una entradas es virtual o necesita validación mecanica, este campo indica si la entrada ya ha sido utilizada");
            });

            modelBuilder.Entity<VentasPagosTemp>(entity =>
            {
                entity.HasKey(e => e.IdPago)
                    .HasName("PK_PagosTemp");

                entity.ToTable("VentasPagosTemp");

                entity.HasComment("Pagos parciales que completan una operacion de venta");

                entity.Property(e => e.IdPago).HasColumnName("ID_Pago");

                entity.Property(e => e.CanalPago).HasComment("Canal de pago, 1 metalico, 2 tarjeta presencial, 3 tarjeta virtual, 4 tarjetas prepago 5 Vales 6 Promociones");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdPromocion).HasColumnName("ID_Promocion");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_Usuario");

                entity.Property(e => e.IdVale).HasColumnName("ID_Vale");

                entity.Property(e => e.IdVenta)
                    .HasColumnName("ID_Venta")
                    .HasComment("Venta asociada a los pagos, enlaza con Ventas");

                entity.Property(e => e.Importe)
                    .HasColumnType("smallmoney")
                    .HasComment("Importe de cada pago parcial o total");

                entity.Property(e => e.NautorizacionBancaria)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("NAutorizacionBancaria")
                    .HasComment("numero de autorizacion bancaria si el pago es con tarjeta");

                entity.Property(e => e.ObservacionDevolucion)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PedidoPinpad)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("datos de pago por pinpad");

                entity.Property(e => e.Rtspinpad)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("RTSPinpad")
                    .HasComment("datos de pago por pinpad");

                entity.Property(e => e.TarjetaRecibo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TipoMovForSync).HasDefaultValueSql("((1))");

                entity.Property(e => e.TipoPago)
                    .HasDefaultValueSql("((0))")
                    .HasComment("0 - Pago 1 Devolución");

                entity.Property(e => e.ValidacionPromocion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.XmlResp)
                    .IsUnicode(false)
                    .HasColumnName("xmlResp");
            });

            modelBuilder.Entity<VentasTemp>(entity =>
            {
                entity.HasKey(e => e.IdVenta)
                    .HasName("PK_Ventas");

                entity.ToTable("VentasTemp");

                entity.HasComment("Cada una de las ventas que se ha realizado en el centro.");

                entity.Property(e => e.IdVenta).HasColumnName("ID_Venta");

                entity.Property(e => e.Base).HasColumnType("smallmoney");

                entity.Property(e => e.Cerrado).HasComment("indica si la venta ya está en un cierre de caja");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Codigo de barras de la venta, para ventas online y puede que en local");

                entity.Property(e => e.CodigoPromocionNominal)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("CP")
                    .HasComment("CP del cliente, puede solicitarlo el sistema o no");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Email del comprador en venta online");

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.FechaHora)
                    .HasColumnType("datetime")
                    .HasComment("Fecha Hora de la venta");

                entity.Property(e => e.IdAnulacion).HasColumnName("ID_Anulacion");

                entity.Property(e => e.IdCentro)
                    .HasColumnName("ID_Centro")
                    .HasComment("Centro de la venta");

                entity.Property(e => e.IdCierre)
                    .HasColumnName("ID_Cierre")
                    .HasComment("cierre que incluye la venta");

                entity.Property(e => e.IdFactura).HasColumnName("ID_Factura");

                entity.Property(e => e.IdPuntoVenta)
                    .HasColumnName("ID_PuntoVenta")
                    .HasComment("Punto donde se ha vendido, enlaza con PuntosVentaFisicos");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("ID_Usuario")
                    .HasComment("Usuario de la venta, enlaza con Usuarios");

                entity.Property(e => e.Impuestos).HasColumnType("smallmoney");

                entity.Property(e => e.NombrePuntoVenta)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Nombre del punto de venta, heredado de PuntosVentaFisicos");

                entity.Property(e => e.NombreUsuario)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("nombre del usuario, heredado de Usuarios");

                entity.Property(e => e.PorcDescuento).HasColumnType("smallmoney");

                entity.Property(e => e.Procesada).HasComment("0 Sin Procesar, 1 Procesada Valida, 2 Procesada Invalida");

                entity.Property(e => e.ReferenciaCompra)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasComment("Referencia de la compra, para cobros bancarios");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasComment("Telefono del comprador, en venta online");

                entity.Property(e => e.TipoMovForSync).HasDefaultValueSql("((1))");

                entity.Property(e => e.Total).HasColumnType("smallmoney");

                entity.Property(e => e.TotalDescuento).HasColumnType("smallmoney");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
