﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class ArticulosSeries
    {
        public int IdArticuloSeries { get; set; }
        public int? IdArticulo { get; set; }
        public string NumeroSerie { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
