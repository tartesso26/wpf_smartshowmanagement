﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class SalasZona
    {
        public int IdZonaSala { get; set; }
        public int? IdSala { get; set; }
        public byte? Numero { get; set; }
        public string NombreZona { get; set; }
        public short? Aforo { get; set; }
        public short? Columnas { get; set; }
        public short? Filas { get; set; }
        public string Asientos { get; set; }
        public string Calidades { get; set; }
        public string NumerosColumnas { get; set; }
        public string NumerosFilas { get; set; }
        public string NumerosAsientos { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
