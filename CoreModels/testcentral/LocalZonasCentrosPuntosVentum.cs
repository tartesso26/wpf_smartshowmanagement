﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalZonasCentrosPuntosVentum
    {
        public int Id { get; set; }
        public int? IdZonaPuntoVenta { get; set; }
        public int? IdZonaCentro { get; set; }
        public int? IdPuntoVenta { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
