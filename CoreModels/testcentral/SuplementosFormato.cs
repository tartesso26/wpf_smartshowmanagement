﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class SuplementosFormato
    {
        public int IdSuplementoFormato { get; set; }
        public int? IdFormato { get; set; }
        public int? IdSuplemento { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
