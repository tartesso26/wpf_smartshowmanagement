﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FacturasCliente
    {
        public int IdFacturaCliente { get; set; }
        public int? IdClienteFactura { get; set; }
        public DateTime? FechaFactura { get; set; }
        public decimal? Descuento { get; set; }
        public decimal? DescuentoPp { get; set; }
        public int? IdCentro { get; set; }
        public decimal? PrecioVenta { get; set; }
        public decimal? Base { get; set; }
        public decimal? Impuestos { get; set; }
        public decimal? RecargoEq { get; set; }
        public decimal? Total { get; set; }
        public string Observaciones { get; set; }
        public short? Estado { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string NumeroFactura { get; set; }
        public int? IdImpuesto { get; set; }
    }
}
