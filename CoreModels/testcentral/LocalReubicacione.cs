﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalReubicacione
    {
        public int Id { get; set; }
        public int? IdReubicacion { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public DateTime? FechaReubicacion { get; set; }
        public string Observaciones { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdUsuario { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
