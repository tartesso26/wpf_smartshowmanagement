﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FormatosSubtitulo
    {
        public int IdFormatoSubtitulo { get; set; }
        public string NombreFormatoSubtitulo { get; set; }
        public string CaracterFormatoSubtitulo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
