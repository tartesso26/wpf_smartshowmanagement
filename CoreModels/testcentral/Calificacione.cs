﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class Calificacione
    {
        public int IdCalificacion { get; set; }
        public string NombreCalificacion { get; set; }
        public string AbreviaturaCalificacion { get; set; }
        public string LogoCalificacion { get; set; }
        public string IconoCalificacion { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
