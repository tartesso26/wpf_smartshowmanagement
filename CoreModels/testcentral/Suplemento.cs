﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class Suplemento
    {
        public int IdSuplemento { get; set; }
        public string Descripcion { get; set; }
        public decimal? Precio { get; set; }
        public int? IdImpuesto { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public byte? ImprimeRecogida { get; set; }
    }
}
