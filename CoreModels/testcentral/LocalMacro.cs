﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class LocalMacro
    {
        public int Id { get; set; }
        public int? IdMacro { get; set; }
        public int? IdCentro { get; set; }
        public int? ModificadoPor { get; set; }
        public string Nombre { get; set; }
        public short? CodigoPrecio { get; set; }
        public decimal? Precio { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public bool? Activo { get; set; }
        public bool? ActivoTaquilla { get; set; }
        public bool? ActivoInternet { get; set; }
        public bool? ActivoKioskos { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
