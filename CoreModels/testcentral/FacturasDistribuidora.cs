﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CoreModels.testcentral
{
    public partial class FacturasDistribuidora
    {
        public int IdFacturaDistribuidora { get; set; }
        public string NumeroFactura { get; set; }
        public int? IdDistribuidora { get; set; }
        public int? IdCentro { get; set; }
        public DateTime? FechaFactura { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public decimal? Total { get; set; }
        public bool? Exportada { get; set; }
        public int? IdTablaPorcentajeDistribuidora { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
