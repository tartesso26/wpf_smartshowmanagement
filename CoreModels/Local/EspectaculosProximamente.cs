﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class EspectaculosProximamente
    {
        public int IdProximamente { get; set; }
        public int? IdEspectaculo { get; set; }
        public int? ModificadoPor { get; set; }
        public string FechaActualizacion { get; set; }
    }
}
