﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class VentasLineas
    {
        public int IdVentaLinea { get; set; }
        public int? IdVenta { get; set; }
        public byte? TipoLinea { get; set; }
        public bool? Anticipada { get; set; }
        public string CodigoEntrada { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public short? NumeroZona { get; set; }
        public string NombreZona { get; set; }
        public int? IdPrecio { get; set; }
        public string NombrePrecio { get; set; }
        public decimal? Precio { get; set; }
        public int? IdPromocion { get; set; }
        public string NombrePromocion { get; set; }
        public decimal? Impuestos { get; set; }
        public decimal? ApagarCliente { get; set; }
        public short? FilaMatriz { get; set; }
        public short? ColumnaMatriz { get; set; }
        public string Fila { get; set; }
        public string Butaca { get; set; }
        public bool? Manual { get; set; }
        public int? IdVentaManual { get; set; }
        public string CodigoBarras { get; set; }
        public bool? Utilizada { get; set; }
        public DateTime? FechaHoraUso { get; set; }
        public int? IdArticuloBarra { get; set; }
        public string NombreArticuloBarra { get; set; }
        public bool? Anulada { get; set; }
        public DateTime? FechaHoraAnulacion { get; set; }
        public int? IdUsuarioAnulacion { get; set; }
        public string FechaActualizacion { get; set; }
        public bool? Aimprimir { get; set; }
        public int? ModificadoPor { get; set; }
        public int? IdMovimientoAlmacen { get; set; }
        public decimal? Cantidad { get; set; }
        public string Observaciones { get; set; }
        public int? IdSuplemento { get; set; }
        public decimal? PrecioCosteArticulo { get; set; }
        public short? TipoMovForSync { get; set; }
        public int? IdFactura { get; set; }
    }
}
