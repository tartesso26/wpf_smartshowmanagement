﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Familias
    {
        public int IdFamilia { get; set; }
        public string NombreFamilia { get; set; }
        public string NombreFamiliaAbreviado { get; set; }
        public string Imagen { get; set; }
        public bool? MostrarEnPos { get; set; }
        public byte? OrdenEnPos { get; set; }
        public bool? FamiliaWeb { get; set; }
        public bool? FamiliaKiosko { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public bool? FamiliaConDiseno { get; set; }
    }
}
