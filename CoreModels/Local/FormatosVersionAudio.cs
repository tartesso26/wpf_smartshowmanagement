﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class FormatosVersionAudio
    {
        public int IdVersionAudio { get; set; }
        public string NombreVersionAudio { get; set; }
        public string LetraVersionAudio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
