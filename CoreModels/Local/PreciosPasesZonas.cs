﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class PreciosPasesZonas
    {
        public int IdPrecioPaseZona { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public int? IdPrecio { get; set; }
        public byte? Orden { get; set; }
        public bool? PrecioWeb { get; set; }
        public bool? PrecioKiosko { get; set; }
        public bool? PrecioApp { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
