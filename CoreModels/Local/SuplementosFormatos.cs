﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class SuplementosFormatos
    {
        public int IdSuplementoFormato { get; set; }
        public int? IdFormato { get; set; }
        public int? IdSuplemento { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
