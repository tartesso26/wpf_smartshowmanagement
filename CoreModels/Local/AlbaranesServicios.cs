﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class AlbaranesServicios
    {
        public int IdAlbaranServicio { get; set; }
        public int? IdProveedor { get; set; }
        public string NumeroAlbaran { get; set; }
        public DateTime? FechaAlbaran { get; set; }
        public decimal? Descuento { get; set; }
        public decimal? DescuentoPp { get; set; }
        public int? IdCentro { get; set; }
        public decimal? Base { get; set; }
        public int? IdImpuesto { get; set; }
        public decimal? RecargoEq { get; set; }
        public decimal? Total { get; set; }
        public string Observaciones { get; set; }
        public short? Estado { get; set; }
        public bool? Astock { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public decimal? Impuestos { get; set; }
    }
}
