﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class TemporadasLineas
    {
        public int IdTemporadaLinea { get; set; }
        public int? IdTemporada { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdPase { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
