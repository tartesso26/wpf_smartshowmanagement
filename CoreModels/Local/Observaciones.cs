﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Observaciones
    {
        public int IdObservacion { get; set; }
        public string Detalle { get; set; }
        public DateTime? FechaHora { get; set; }
        public DateTime? DiaObservacion { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
        public int? IdSala { get; set; }
        public int? Tipo { get; set; }
    }
}
