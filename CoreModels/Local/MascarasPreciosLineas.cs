﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class MascarasPreciosLineas
    {
        public int IdMascaraPrecioLinea { get; set; }
        public int? IdMascaraPrecio { get; set; }
        public int? IdPrecio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
