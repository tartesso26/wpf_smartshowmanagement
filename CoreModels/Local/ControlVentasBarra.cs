﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ControlVentasBarra
    {
        public int IdControlVentaBarra { get; set; }
        public DateTime? FechaHora { get; set; }
        public bool? AperturaCajon { get; set; }
        public int? IdUsuario { get; set; }
        public bool? LineaBorrada { get; set; }
        public int? IdArticulo { get; set; }
        public bool? CobroCancelado { get; set; }
        public decimal? TotalTicket { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string Tipo { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
