﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ControlSelecciones
    {
        public long IdControlSeleccion { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public short? FilaMatriz { get; set; }
        public short? ColumnaMatriz { get; set; }
        public byte? Estado { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
