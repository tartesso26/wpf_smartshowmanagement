﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class VRecaudacionMinisterio
    {
        public string Cine { get; set; }
        public string Sala { get; set; }
        public DateTime? Fecha { get; set; }
        public string Hora { get; set; }
        public string Nexpediente { get; set; }
        public string Titulo { get; set; }
        public int? Espectadores { get; set; }
        public decimal? Importe { get; set; }
    }
}
