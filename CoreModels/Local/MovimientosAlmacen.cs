﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class MovimientosAlmacen
    {
        public int IdMovimientoAlmacen { get; set; }
        public DateTime? FechaHoraMovimiento { get; set; }
        public int? IdArticulo { get; set; }
        public string EntradaSalida { get; set; }
        public string Descripcion { get; set; }
        public decimal? Unidades { get; set; }
        public decimal? PrecioUnidad { get; set; }
        public decimal? Total { get; set; }
        public int? Documento { get; set; }
        public bool? Menu { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public int? IdInventario { get; set; }
        public int? IdLineaVenta { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
