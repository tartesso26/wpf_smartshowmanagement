﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class EtlconfigTableList
    {
        public int Id { get; set; }
        public string SrcTableName { get; set; }
        public string DestTableName { get; set; }
        public string StgTablename { get; set; }
        public string LastExecutionAttemp { get; set; }
        public string LastExecutionTime { get; set; }
    }
}
