﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ZonasCentrosPuntosVenta
    {
        public int IdZonaPuntoVenta { get; set; }
        public int? IdZonaCentro { get; set; }
        public int? IdPuntoVenta { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
