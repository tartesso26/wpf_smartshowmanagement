﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Reubicaciones
    {
        public int IdReubicacion { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public DateTime? FechaReubicacion { get; set; }
        public string Observaciones { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdUsuario { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
