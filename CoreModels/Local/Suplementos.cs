﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Suplementos
    {
        public int IdSuplemento { get; set; }
        public string Descripcion { get; set; }
        public decimal? Precio { get; set; }
        public int? IdImpuesto { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public byte? ImprimeRecogida { get; set; }
    }
}
