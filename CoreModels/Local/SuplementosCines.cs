﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class SuplementosCines
    {
        public int IdSuplementoCine { get; set; }
        public int? IdSuplemento { get; set; }
        public int? IdCentro { get; set; }
        public decimal? Precio { get; set; }
        public int? IdImpuesto { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
