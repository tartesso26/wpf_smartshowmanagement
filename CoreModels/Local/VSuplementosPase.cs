﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class VSuplementosPase
    {
        public int Id { get; set; }
        public int IdTipo { get; set; }
        public long? IdButaca { get; set; }
        public int? IdSuplemento { get; set; }
        public string Descripcion { get; set; }
        public decimal? Importe { get; set; }
        public decimal Porcentaje1 { get; set; }
        public int? IdPase { get; set; }
    }
}
