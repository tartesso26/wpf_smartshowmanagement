﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class VentasPagos
    {
        public int IdPago { get; set; }
        public int? IdVenta { get; set; }
        public int? IdUsuario { get; set; }
        public byte? CanalPago { get; set; }
        public decimal? Importe { get; set; }
        public string NautorizacionBancaria { get; set; }
        public string PedidoPinpad { get; set; }
        public string Rtspinpad { get; set; }
        public string FechaActualizacion { get; set; }
        public byte? TipoPago { get; set; }
        public string ObservacionDevolucion { get; set; }
        public int? ModificadoPor { get; set; }
        public string TarjetaRecibo { get; set; }
        public string XmlResp { get; set; }
        public int? IdVale { get; set; }
        public int? IdPromocion { get; set; }
        public string ValidacionPromocion { get; set; }
        public short? TipoMovForSync { get; set; }
        public int? IdVentaManual { get; set; }
    }
}
