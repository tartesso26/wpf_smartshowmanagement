﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Recogidas
    {
        public int IdRecogida { get; set; }
        public int? IdVenta { get; set; }
        public int? IdCentro { get; set; }
        public DateTime? FechaHoraRecogida { get; set; }
        public int? ArticulosSeleccionables { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public int? IdPuntoVenta { get; set; }
        public int? IdUsuario { get; set; }
    }
}
