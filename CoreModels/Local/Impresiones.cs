﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Impresiones
    {
        public int IdImpresion { get; set; }
        public int? IdCentro { get; set; }
        public string Informe { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime? FechaHora { get; set; }
        public string EmailDestino { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
