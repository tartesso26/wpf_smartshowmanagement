﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class MacrosPromos
    {
        public int IdMacroPromo { get; set; }
        public int? IdMacro { get; set; }
        public int? IdPromocion { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
