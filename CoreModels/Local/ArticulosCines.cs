﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ArticulosCines
    {
        public int IdArticuloCine { get; set; }
        public int? IdArticulo { get; set; }
        public int? IdCentro { get; set; }
        public decimal? Precio { get; set; }
        public int? IdImpuesto { get; set; }
        public int? ModificadoPor { get; set; }
        public string FechaActualizacion { get; set; }
        public decimal? StockMinimo { get; set; }
        public decimal? StockMaximo { get; set; }
        public decimal? StockActual { get; set; }
    }
}
