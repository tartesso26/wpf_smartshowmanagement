﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class VProgramacion
    {
        public string Sala { get; set; }
        public DateTime? HoraReal { get; set; }
        public DateTime? HoraCine { get; set; }
        public int Codigo { get; set; }
        public int CodigoSesion { get; set; }
        public bool? VentaRemota { get; set; }
        public string Formato { get; set; }
    }
}
