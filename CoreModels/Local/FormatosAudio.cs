﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class FormatosAudio
    {
        public int IdFormatoAudio { get; set; }
        public string NombreFormatoAudio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
