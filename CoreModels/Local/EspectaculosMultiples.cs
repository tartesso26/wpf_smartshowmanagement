﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class EspectaculosMultiples
    {
        public int IdEspectaculoMultiple { get; set; }
        public int? IdEspectaculoPadre { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdEspectaculoHijo { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
