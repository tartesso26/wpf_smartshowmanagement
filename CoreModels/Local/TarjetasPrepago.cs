﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class TarjetasPrepago
    {
        public int IdTarjeta { get; set; }
        public int? CodigoTarjeta { get; set; }
        public short? NumEntradas { get; set; }
        public decimal? ImporteTotal { get; set; }
        public short? EntradasConsumidas { get; set; }
        public decimal? ImporteConsumido { get; set; }
        public byte? TipoTarjeta { get; set; }
        public int? IdCentro { get; set; }
        public int? ModificadoPor { get; set; }
        public int IdClienteCadena { get; set; }
        public string FechaActualizacion { get; set; }
    }
}
