﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class FamiliaAnalisis
    {
        public int IdFamiliaAnalisis { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? Nivel { get; set; }
        public int? IdFamiliaPadre { get; set; }
    }
}
