﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class RollosManuales
    {
        public short IdRolloManual { get; set; }
        public string Descripcion { get; set; }
        public int? NumeroActual { get; set; }
        public int? NumeroInicial { get; set; }
        public int? NumeroFinal { get; set; }
        public int? IdSala { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
