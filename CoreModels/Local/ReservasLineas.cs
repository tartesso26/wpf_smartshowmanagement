﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ReservasLineas
    {
        public int IdReservaLinea { get; set; }
        public int? IdReserva { get; set; }
        public string CodigoReserva { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public byte? NumeroZona { get; set; }
        public string NombreZona { get; set; }
        public int? IdPrecio { get; set; }
        public string NombrePrecio { get; set; }
        public decimal? Precio { get; set; }
        public byte? FilaMatriz { get; set; }
        public byte? ColumnaMatriz { get; set; }
        public string Fila { get; set; }
        public string Butaca { get; set; }
        public string CodigoBarras { get; set; }
        public bool? Vendida { get; set; }
        public bool? Anulada { get; set; }
        public DateTime? FechaHoraAnulacion { get; set; }
        public DateTime? FechaHoraVenta { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
