﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class CodigosPais
    {
        public int IdCodigoPais { get; set; }
        public string NombrePais { get; set; }
        public string Zona { get; set; }
        public string Codigo { get; set; }
    }
}
