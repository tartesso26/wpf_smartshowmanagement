﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class RecogidaArticulos
    {
        public int IdRecogidaArticulo { get; set; }
        public int? IdRecogidaLinea { get; set; }
        public int? IdArticulo { get; set; }
        public int? IdVentaLinea { get; set; }
        public int? Seleccionables { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
