﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class InventariosLineas
    {
        public int IdInventarioLinea { get; set; }
        public int? IdInventario { get; set; }
        public int? IdArticulo { get; set; }
        public decimal? ValoracionVenta { get; set; }
        public decimal? ValoracionCompra { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string CantidadEstimada { get; set; }
        public string CantidadReal { get; set; }
        public int? IdMovimientoAlmacen { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
