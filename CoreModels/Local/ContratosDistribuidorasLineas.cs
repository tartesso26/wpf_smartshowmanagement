﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ContratosDistribuidorasLineas
    {
        public int IdContratoDistribuidoraLinea { get; set; }
        public int? IdContrato { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public decimal? PorcentajeLunes { get; set; }
        public decimal? PorcentajeMartes { get; set; }
        public decimal? PorcentajeMiercoles { get; set; }
        public decimal? PorcentajeJueves { get; set; }
        public decimal? PorcentajeViernes { get; set; }
        public decimal? PorcentajeSabado { get; set; }
        public decimal? PorcentajeDomingo { get; set; }
        public decimal? ImporteLunes { get; set; }
        public decimal? ImporteMartes { get; set; }
        public decimal? ImporteMiercoles { get; set; }
        public decimal? ImporteJueves { get; set; }
        public decimal? ImporteViernes { get; set; }
        public decimal? ImporteSabado { get; set; }
        public decimal? ImporteDomingo { get; set; }
        public bool? FacturacionPorcentaje { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string Nombre { get; set; }
    }
}
