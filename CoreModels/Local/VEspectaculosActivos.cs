﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class VEspectaculosActivos
    {
        public int IdEspectaculo { get; set; }
        public string Titulo { get; set; }
        public string TituloOriginal { get; set; }
        public string NombreGenero { get; set; }
        public string NombreCalificacion { get; set; }
        public string AbreviaturaCalificacion { get; set; }
        public string IconoCalificacion { get; set; }
        public string Nacionalidad { get; set; }
        public DateTime? FechaEstreno { get; set; }
        public short? Duracion { get; set; }
        public string Ano { get; set; }
        public int? IdDistribuidora { get; set; }
        public string NombreDistribuidora { get; set; }
        public string Siglas { get; set; }
        public string CodigoIcaadistribuidora { get; set; }
        public string Director { get; set; }
        public string Interpretes { get; set; }
        public string Nexpediente { get; set; }
        public bool? Comunitaria { get; set; }
        public string Ncalificacion { get; set; }
        public short? Nrollos { get; set; }
        public bool? Largometraje { get; set; }
        public bool? PeliculaX { get; set; }
        public string Cartel { get; set; }
        public string Cartel2 { get; set; }
        public string Cartel3 { get; set; }
        public string Video { get; set; }
        public string VideoSecundario { get; set; }
        public string Sinopsis { get; set; }
        public string SinopsisAlternativa { get; set; }
        public string Sinopsisalternativa2 { get; set; }
        public bool? Pelicula { get; set; }
        public short? TiempoTrailer { get; set; }
        public short? TiempoLimpieza { get; set; }
        public string FechaActualizacion { get; set; }
        public string IdiomaOriginal { get; set; }
        public string LetraIdiomaOriginal { get; set; }
        public string Pdf { get; set; }
        public int? ModificadoPor { get; set; }
        public bool? WhatsCine { get; set; }
        public string TipoEventoCodigo { get; set; }
        public string TipoEventoDescripcion { get; set; }
        public string CodigoPaisPrincipal { get; set; }
        public string NombreDirectorPrincipal { get; set; }
        public string ApellidosDirectorPrincipal { get; set; }
        public string Lenguaje { get; set; }
        public int? Numeropases { get; set; }
    }
}
