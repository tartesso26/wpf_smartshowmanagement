﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class FormatosSubtitulos
    {
        public int IdFormatoSubtitulo { get; set; }
        public string NombreFormatoSubtitulo { get; set; }
        public string CaracterFormatoSubtitulo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
