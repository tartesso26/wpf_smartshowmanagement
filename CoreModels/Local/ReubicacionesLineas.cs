﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class ReubicacionesLineas
    {
        public int IdReubicacionLinea { get; set; }
        public int? IdReubicacion { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public short? FilaMatrizInicio { get; set; }
        public short? ColumnaMatrizInicio { get; set; }
        public short? FilaMatrizFin { get; set; }
        public short? ColumnaMatrizFin { get; set; }
        public string FechaActualizacion { get; set; }
        public string FilaInicio { get; set; }
        public string FilaFin { get; set; }
        public string ButacaInicio { get; set; }
        public string ButacaFin { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
