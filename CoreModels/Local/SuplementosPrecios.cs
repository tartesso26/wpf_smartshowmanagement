﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class SuplementosPrecios
    {
        public int IdSuplementoPrecio { get; set; }
        public int? IdPrecio { get; set; }
        public int? IdSuplemento { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
