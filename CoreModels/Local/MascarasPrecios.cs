﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class MascarasPrecios
    {
        public int IdMascaraPrecio { get; set; }
        public string NombreMascara { get; set; }
        public string ExplicacionMascara { get; set; }
        public bool? AplicarLunes { get; set; }
        public bool? AplicarMartes { get; set; }
        public bool? AplicarMiercoles { get; set; }
        public bool? AplicarJueves { get; set; }
        public bool? AplicarViernes { get; set; }
        public bool? AplicarSabado { get; set; }
        public bool? AplicarDomingo { get; set; }
        public bool? AplicarFestivos { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public bool? Golfa { get; set; }
        public bool? Matinal { get; set; }
    }
}
