﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class TiposButacas
    {
        public byte IdTipoButaca { get; set; }
        public string CodigoButaca { get; set; }
        public string NombreTipoButaca { get; set; }
        public string IconoButaca { get; set; }
        public bool? TipoCambiable { get; set; }
        public bool? Vendible { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public int? TipoReal { get; set; }
        public int? Estado { get; set; }
    }
}
