﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Proveedores
    {
        public int IdProveedor { get; set; }
        public string Nombre { get; set; }
        public string Cif { get; set; }
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Cp { get; set; }
        public string Tlfno1 { get; set; }
        public string Email { get; set; }
        public string Pcontacto { get; set; }
        public string Observaciones { get; set; }
        public string WebSite { get; set; }
        public string CuentaContable { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
