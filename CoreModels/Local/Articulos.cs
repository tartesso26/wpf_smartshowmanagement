﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Articulos
    {
        public int IdArticulo { get; set; }
        public string NombreCompleto { get; set; }
        public string NombreCorto { get; set; }
        public string Codigo { get; set; }
        public string CodigoBarras { get; set; }
        public bool? ControlarStock { get; set; }
        public decimal? StockMinimo { get; set; }
        public decimal? StockMaximo { get; set; }
        public decimal? StockActual { get; set; }
        public int? IdImpuesto { get; set; }
        public decimal? Precio { get; set; }
        public decimal? PrecioCompra { get; set; }
        public bool? UsarPeso { get; set; }
        public bool? PedirPeso { get; set; }
        public byte? NumeroArticulosSeleccionables { get; set; }
        public bool? PedirPrecio { get; set; }
        public string Imagen { get; set; }
        public string Observaciones { get; set; }
        public decimal? PorcentajeDtoCompra { get; set; }
        public decimal? PorcentajeBonificado { get; set; }
        public decimal? PuntoVerde { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public bool? MateriaPrima { get; set; }
        public int? IdArticuloAlmacen { get; set; }
        public bool? EnBaja { get; set; }
        public decimal? ConversionUnidades { get; set; }
        public string TipoUnidad { get; set; }
        public decimal? Rappel { get; set; }
        public decimal? DtoMarketing { get; set; }
        public int? IdFamiliaAnalisis { get; set; }
        public decimal? PrecioIntercambio { get; set; }
        public string NumeroSerie { get; set; }
        public byte? EsCombo { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public bool? VentaWeb { get; set; }
    }
}
