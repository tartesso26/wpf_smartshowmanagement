﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Inventarios
    {
        public int IdInventario { get; set; }
        public DateTime? FechaInventario { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdCentro { get; set; }
        public string Observaciones { get; set; }
        public int? ModificadoPor { get; set; }
        public byte? Cerrado { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
