﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Agendas
    {
        public int IdAgenda { get; set; }
        public string Nombre { get; set; }
        public string Mail { get; set; }
        public string Telefono { get; set; }
        public string Empresa { get; set; }
        public string Observaciones { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
