﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class Calificaciones
    {
        public int IdCalificacion { get; set; }
        public string NombreCalificacion { get; set; }
        public string AbreviaturaCalificacion { get; set; }
        public string LogoCalificacion { get; set; }
        public string IconoCalificacion { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
