﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class TipoUsuarios
    {
        public int IdTipoUsuarios { get; set; }
        public int? IdCentro { get; set; }
        public int? Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Permisos { get; set; }
        public byte? MaxSeleccionButacas { get; set; }
        public byte? CopiasEntradas { get; set; }
        public byte? CopiasExtras { get; set; }
        public byte? CopiasBarra { get; set; }
        public bool? UsarFidelizacion { get; set; }
        public bool? UsarTarjetasPrepago { get; set; }
        public bool? PedirCp { get; set; }
        public bool? UsarTarjetaAbonado { get; set; }
        public int? ModificadoPor { get; set; }
        public string FechaActualizacion { get; set; }
    }
}
