﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class TraspasosLineas
    {
        public int IdTraspasoLinea { get; set; }
        public int? IdTraspaso { get; set; }
        public int? IdArticulo { get; set; }
        public string Descripcion { get; set; }
        public decimal? Unidades { get; set; }
        public int? IdMovimientoAlmacen { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
