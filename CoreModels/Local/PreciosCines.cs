﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel.Local
{
    public partial class PreciosCines
    {
        public int IdPrecioCine { get; set; }
        public int? IdPrecio { get; set; }
        public int? IdCentro { get; set; }
        public decimal? Importe { get; set; }
        public decimal? NuevoImporte { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public int? IdImpuesto { get; set; }
        public bool? CentroExcluido { get; set; }
    }
}
