﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class TablasPorcentajeDistribuidoras
    {
        public int IdTablaPorcentajeDistribuidora { get; set; }
        public int? IdDistribuidora { get; set; }
        public string Categoria { get; set; }
        public string Tipo { get; set; }
        public bool? FacturacionPorcentaje { get; set; }
        public decimal? Semana1 { get; set; }
        public decimal? Semana2 { get; set; }
        public decimal? Semana3 { get; set; }
        public decimal? Semana4 { get; set; }
        public decimal? Semana5 { get; set; }
        public decimal? Semana6 { get; set; }
        public decimal? Semana7 { get; set; }
        public decimal? Semana8 { get; set; }
        public decimal? Semana9 { get; set; }
        public decimal? Semana10 { get; set; }
        public decimal? SemanaPro { get; set; }
        public string FechaActualizacion { get; set; }
    }
}
