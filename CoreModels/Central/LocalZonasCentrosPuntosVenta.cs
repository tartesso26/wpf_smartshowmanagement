﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalZonasCentrosPuntosVenta
    {
        public int Id { get; set; }
        public int? IdZonaPuntoVenta { get; set; }
        public int? IdZonaCentro { get; set; }
        public int? IdPuntoVenta { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
