﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalReubicacionesLineas
    {
        public int Id { get; set; }
        public int? IdReubicacionLinea { get; set; }
        public int? IdReubicacion { get; set; }
        public int? IdPase { get; set; }
        public int? IdZonaSala { get; set; }
        public short? FilaMatrizInicio { get; set; }
        public short? ColumnaMatrizInicio { get; set; }
        public short? FilaMatrizFin { get; set; }
        public short? ColumnaMatrizFin { get; set; }
        public string FechaActualizacion { get; set; }
        public int? FilaInicio { get; set; }
        public int? FilaFin { get; set; }
        public int? ButacaInicio { get; set; }
        public int? ButacaFin { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
