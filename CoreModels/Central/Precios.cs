﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class Precios
    {
        public int IdPrecio { get; set; }
        public short? Codigo { get; set; }
        public string Nombre { get; set; }
        public decimal? Importe { get; set; }
        public byte? NumEntradasNecesarias { get; set; }
        public DateTime? FechaCambio { get; set; }
        public decimal? NuevoImporte { get; set; }
        public string TextoPantalla { get; set; }
        public bool? Activado { get; set; }
        public DateTime? FechaActivacion { get; set; }
        public DateTime? FechaDesactivacion { get; set; }
        public string CodigoGeneralitat { get; set; }
        public bool? PrecioWeb { get; set; }
        public bool? PrecioKiosko { get; set; }
        public bool? PrecioApp { get; set; }
        public decimal? ComisionFijaPrecio { get; set; }
        public decimal? ComisionPorcentajePrecio { get; set; }
        public decimal? ComisionFacturacionCanales { get; set; }
        public string ExplicacionUsoPrecio { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public int? IdImpuesto { get; set; }
        public string Imagen { get; set; }
        public bool? PrecioFidelity { get; set; }
        public byte? TipoButaca { get; set; }
    }
}
