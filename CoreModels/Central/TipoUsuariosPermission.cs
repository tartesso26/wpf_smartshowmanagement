﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class TipoUsuariosPermission
    {
        public int? IdTipoUsuario { get; set; }
        public int? IdPermission { get; set; }
        public int Id { get; set; }
    }
}
