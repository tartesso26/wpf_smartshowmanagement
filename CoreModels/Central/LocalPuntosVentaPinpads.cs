﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalPuntosVentaPinpads
    {
        public int Id { get; set; }
        public int? IdPuntoVentaPinpad { get; set; }
        public string PinpadNumeroSerie { get; set; }
        public string PinpadNombreBanco { get; set; }
        public string PinpadCodComercio { get; set; }
        public string PinpadCodTerminal { get; set; }
        public string PinpadClaveFirma { get; set; }
        public string PinpadConfigPuerto { get; set; }
        public string PinpadVersion { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
