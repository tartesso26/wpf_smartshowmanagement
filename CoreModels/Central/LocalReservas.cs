﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalReservas
    {
        public int Id { get; set; }
        public int? IdReserva { get; set; }
        public int? IdCentro { get; set; }
        public DateTime? FechaHora { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string ReferenciaCompra { get; set; }
        public string Nombre { get; set; }
        public int? IdPuntoVenta { get; set; }
        public int? IdUsuario { get; set; }
        public string NombrePuntoVenta { get; set; }
        public string NombreUsuario { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
