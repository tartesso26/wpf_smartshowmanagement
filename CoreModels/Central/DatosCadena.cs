﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class DatosCadena
    {
        public int Id { get; set; }
        public string NombreCadena { get; set; }
        public string DireccionCadena { get; set; }
        public string LocalidadCadena { get; set; }
        public string ProvinciaCadena { get; set; }
        public string Cpcadena { get; set; }
        public string TelefonoCadena { get; set; }
        public string EmailCadena { get; set; }
        public string ImagenCadena { get; set; }
    }
}
