﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalMacrosPromos
    {
        public int Id { get; set; }
        public int? IdMacroPromo { get; set; }
        public int? IdMacro { get; set; }
        public int? IdPromocion { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
