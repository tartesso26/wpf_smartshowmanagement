﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalPreciosTemporadas
    {
        public int Id { get; set; }
        public int? IdPrecioTemporada { get; set; }
        public int? IdTemporada { get; set; }
        public int? IdPrecio { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
