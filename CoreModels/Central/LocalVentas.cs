﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalVentas
    {
        public int Id { get; set; }
        public int? IdVenta { get; set; }
        public int? IdCentro { get; set; }
        public DateTime? FechaHora { get; set; }
        public string CodigoBarras { get; set; }
        public string Email { get; set; }
        public string ReferenciaCompra { get; set; }
        public string Telefono { get; set; }
        public bool? Cerrado { get; set; }
        public int? IdCierre { get; set; }
        public int? IdPuntoVenta { get; set; }
        public string NombrePuntoVenta { get; set; }
        public int? IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Cp { get; set; }
        public string FechaActualizacion { get; set; }
        public int? NumeroVenta { get; set; }
        public int? ModificadoPor { get; set; }
        public string CodigoPromocionNominal { get; set; }
        public decimal? Base { get; set; }
        public decimal? Impuestos { get; set; }
        public decimal? Total { get; set; }
        public decimal? PorcDescuento { get; set; }
        public decimal? TotalDescuento { get; set; }
        public int? IdAnulacion { get; set; }
        public bool? EsMerma { get; set; }
        public int? IdFactura { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
