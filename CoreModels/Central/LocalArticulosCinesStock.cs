﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalArticulosCinesStock
    {
        public int Id { get; set; }
        public int? IdArticuloCineStock { get; set; }
        public int? IdArticulo { get; set; }
        public int? IdCentro { get; set; }
        public int? ModificadoPor { get; set; }
        public string FechaActualizacion { get; set; }
        public decimal? StockActual { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
