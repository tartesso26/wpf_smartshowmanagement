﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class Usuarios
    {
        public int IdUsuario { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Permisos { get; set; }
        public string CodigoBarras { get; set; }
        public byte? TipoUsuario { get; set; }
        public byte? MaxSeleccionButacas { get; set; }
        public byte? CopiasEntradas { get; set; }
        public byte? CopiasExtras { get; set; }
        public byte? CopiasBarra { get; set; }
        public bool? EnUso { get; set; }
        public bool? UsarFidelizacion { get; set; }
        public bool? UsarTarjetasPrepago { get; set; }
        public bool? PedirCp { get; set; }
        public bool? UsarTarjetaAbonado { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string WebPass { get; set; }
        public string Imagen { get; set; }
        public int? ModificadoPor { get; set; }
        public string FechaActualizacion { get; set; }
    }
}
