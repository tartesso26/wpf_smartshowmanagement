﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class UsuariosCentros
    {
        public int IdUsuarioCentro { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
