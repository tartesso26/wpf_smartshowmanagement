﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class ClientesCadena
    {
        public int IdClienteCadena { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Cp { get; set; }
        public string CodigoSocio { get; set; }
        public string Mail { get; set; }
        public string Tlfno { get; set; }
        public int? IdCentro { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string Dni { get; set; }
        public string Pwd { get; set; }
    }
}
