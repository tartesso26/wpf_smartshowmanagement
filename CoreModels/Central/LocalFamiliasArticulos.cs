﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalFamiliasArticulos
    {
        public int Id { get; set; }
        public short? IdFamiliaArticulo { get; set; }
        public int? IdArticulo { get; set; }
        public int? IdFamilia { get; set; }
        public byte? Orden { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TamanoX { get; set; }
        public short? TamanoY { get; set; }
        public short? PosicionX { get; set; }
        public short? PosicionY { get; set; }
        public int? ModificadoPor { get; set; }
        public string Imagen { get; set; }
        public int? Color { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
