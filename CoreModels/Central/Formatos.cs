﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class Formatos
    {
        public int IdFormato { get; set; }
        public string NombreFormato { get; set; }
        public string LogoFormato { get; set; }
        public string FormatoVideo { get; set; }
        public string FormatoAudio { get; set; }
        public string VersionAudio { get; set; }
        public string LetraVersionAudio { get; set; }
        public string Formato3D { get; set; }
        public string Subtitulos { get; set; }
        public string FechaActualizacion { get; set; }
        public bool? FormatoUsoEnImagen { get; set; }
        public string Sufijo { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
