﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalMacrosArticulos
    {
        public int Id { get; set; }
        public int? IdMacroArticulo { get; set; }
        public int? IdMacro { get; set; }
        public int? IdArticulo { get; set; }
        public decimal? Precio { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
