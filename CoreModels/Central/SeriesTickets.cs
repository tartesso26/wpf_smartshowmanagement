﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class SeriesTickets
    {
        public int IdSerieTicket { get; set; }
        public string SerieTaquilla { get; set; }
        public string SerieBarra { get; set; }
        public string SerieSuplementos { get; set; }
        public string SerieMermas { get; set; }
    }
}
