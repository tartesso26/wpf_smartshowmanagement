﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalVentasSuplementos
    {
        public int Id { get; set; }
        public int? IdVentaSuplemento { get; set; }
        public int? IdVenta { get; set; }
        public int? IdSuplemento { get; set; }
        public decimal? Precio { get; set; }
        public decimal? Impuestos { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
