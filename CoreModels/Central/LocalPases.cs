﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalPases
    {
        public int Id { get; set; }
        public short? IdPase { get; set; }
        public string Franja { get; set; }
        public int? IdSala { get; set; }
        public string NombreSala { get; set; }
        public string CodigoInesala { get; set; }
        public long? IdEspectaculo { get; set; }
        public string NombreEspectaculo { get; set; }
        public int? IdFormato { get; set; }
        public string NombreFormato { get; set; }
        public string LogoFormato { get; set; }
        public DateTime? HoraReal { get; set; }
        public DateTime? HoraCine { get; set; }
        public bool? Numerada { get; set; }
        public bool? MostrarEnPantalla { get; set; }
        public short? Cerrada { get; set; }
        public bool? VentaRemota { get; set; }
        public bool? CierreAutomatico { get; set; }
        public bool? Exportable { get; set; }
        public bool? VisibleEnWeb { get; set; }
        public int? IdCentro { get; set; }
        public string Ncopia { get; set; }
        public bool? ExcluirSuplementosSala { get; set; }
        public bool? ExcluirSuplementosEspectaculo { get; set; }
        public bool? ExcluirSuplementosPrecio { get; set; }
        public string FechaActualizacion { get; set; }
        public string ImagenPase { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
