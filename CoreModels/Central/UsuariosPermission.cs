﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class UsuariosPermission
    {
        public int? PermissionId { get; set; }
        public int? IdUsuario { get; set; }
        public int Id { get; set; }
    }
}
