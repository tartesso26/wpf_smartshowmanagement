﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalTemporadasSocios
    {
        public int Id { get; set; }
        public int? IdTemporadaSocio { get; set; }
        public int? IdTemporada { get; set; }
        public int? IdSocio { get; set; }
        public int? IdPrecio { get; set; }
        public decimal? Precio { get; set; }
        public short? FilaMatriz { get; set; }
        public short? ColumnaMatriz { get; set; }
        public short? Fila { get; set; }
        public short? Butaca { get; set; }
        public int? IdZonaSala { get; set; }
        public int? IdSala { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
