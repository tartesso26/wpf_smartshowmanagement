﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class Promociones
    {
        public int IdPromocion { get; set; }
        public string Nombre { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public short? NumeroMaximo { get; set; }
        public short? NumeroActual { get; set; }
        public byte? TipoPromocion { get; set; }
        public decimal? DtoPorcentaje { get; set; }
        public decimal? DtoPrecioFijo { get; set; }
        public decimal? DtoEuros { get; set; }
        public decimal? ValorContable { get; set; }
        public bool? PromocionaSuplementos { get; set; }
        public bool? PromocionOnline { get; set; }
        public int? IdEspectaculo { get; set; }
        public bool? ImprimirComprobante { get; set; }
        public bool? DetallarComoPrecio { get; set; }
        public bool? ValidaEnEntradas { get; set; }
        public bool? ValidaEnBarra { get; set; }
        public bool? PromocionAutomatica { get; set; }
        public int? IdArticulo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public decimal? PrecioArticulo { get; set; }
        public int? IdImpuesto { get; set; }
        public int? IdArticuloNecesario { get; set; }
        public short? UnidadesNecesarias { get; set; }
        public bool? PromocionNominal { get; set; }
        public bool? PromoConValorContableDistintoDelPrecio { get; set; }
        public short? OrdenMostrar { get; set; }
        public bool? PromoReserva { get; set; }
        public bool? PromoVenta { get; set; }
        public int? IdProveedor { get; set; }
        public bool? PromoConChequeoOnline { get; set; }
        public string XmlRespuesta { get; set; }
        public bool? Acumulable { get; set; }
        public bool? ValeInterno { get; set; }
        public bool? GeneraVale { get; set; }
        public byte? ValidoPara { get; set; }
        public byte? DiasValidezPromocion { get; set; }
        public bool? PromocionVales { get; set; }
    }
}
