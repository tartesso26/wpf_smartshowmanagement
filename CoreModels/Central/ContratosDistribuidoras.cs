﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class ContratosDistribuidoras
    {
        public int IdContratoDistribuidora { get; set; }
        public string NumeroContrato { get; set; }
        public int? IdCentro { get; set; }
        public int? IdDistribuidora { get; set; }
        public long? IdEspectaculo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
