﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalTemporadasLineas
    {
        public int Id { get; set; }
        public int? IdTemporadaLinea { get; set; }
        public int? IdTemporada { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdPase { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
