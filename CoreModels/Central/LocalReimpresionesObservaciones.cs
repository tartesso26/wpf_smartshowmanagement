﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalReimpresionesObservaciones
    {
        public int Id { get; set; }
        public int? IdReimpresionObservacion { get; set; }
        public string Texto { get; set; }
        public string FechaActualizacion { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
