﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class SuplementosEspectaculos
    {
        public int IdSuplementoEspectaculo { get; set; }
        public int? IdSuplemento { get; set; }
        public int? IdEspectaculo { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
