﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalMovimientosTpv
    {
        public int Id { get; set; }
        public int? IdMovimientoTpv { get; set; }
        public string Descripcion { get; set; }
        public decimal? Importe { get; set; }
        public DateTime? FechaHoraMovimiento { get; set; }
        public DateTime? FechaValidez { get; set; }
        public int? IdUsuario { get; set; }
        public bool? Tarjeta { get; set; }
        public string FechaActualizacion { get; set; }
        public int? IdCierre { get; set; }
        public int? IdPuntoVenta { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
