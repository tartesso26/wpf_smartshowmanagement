﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class FacturasProveedoresAlbaranes
    {
        public int IdFacturaProveedorAlbaran { get; set; }
        public int? IdFactura { get; set; }
        public int? IdAlbaran { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
