﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class ArticulosInmobilizado
    {
        public int IdArticuloInmobilizado { get; set; }
        public string NombreCompleto { get; set; }
        public string NombreCorto { get; set; }
        public string CodigoBarras { get; set; }
        public bool? ControlarStock { get; set; }
        public short? StockMinimo { get; set; }
        public short? StockMaximo { get; set; }
        public short? StockActual { get; set; }
        public int? IdImpuesto { get; set; }
        public decimal? Precio { get; set; }
        public decimal? PrecioCompra { get; set; }
        public bool? PedirPrecio { get; set; }
        public string Imagen { get; set; }
        public string Imagen2 { get; set; }
        public string Imagen3 { get; set; }
        public string Localizacion { get; set; }
        public string Observaciones { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public int? IdCentro { get; set; }
        public bool? EnBaja { get; set; }
        public string TipoUnidad { get; set; }
        public decimal? Rappel { get; set; }
        public decimal? DtoMarketing { get; set; }
        public int? IdFamiliaAnalisis { get; set; }
        public decimal? PrecioIntercambio { get; set; }
        public string NumeroSerie { get; set; }
        public byte? EsCombo { get; set; }
    }
}
