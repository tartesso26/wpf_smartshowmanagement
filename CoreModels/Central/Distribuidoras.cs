﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class Distribuidoras
    {
        public int IdDistribuidora { get; set; }
        public string NombreDistribuidora { get; set; }
        public string Siglas { get; set; }
        public string CodigoIcaa { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string CuentaContable { get; set; }
        public string Cif { get; set; }
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }
        public string Cp { get; set; }
        public bool? FacturacionPorPorcentaje { get; set; }
        public decimal? PorcentajeImpuestosFacturacion { get; set; }
        public decimal? PorcentajeDerechosAutor { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public bool Totalizar { get; set; }
    }
}
