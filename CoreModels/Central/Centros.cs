﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class Centros
    {
        public int IdCentro { get; set; }
        public string Nombre { get; set; }
        public string NombreAbreviado { get; set; }
        public string DireccionCentro { get; set; }
        public string LocalidadCentro { get; set; }
        public string ProvinciaCentro { get; set; }
        public string Cpcentro { get; set; }
        public string Circuito { get; set; }
        public string Empresa { get; set; }
        public string Cif { get; set; }
        public string DireccionEmpresa { get; set; }
        public string LocalidadEmpresa { get; set; }
        public string ProvinciaEmpresa { get; set; }
        public string Cpempresa { get; set; }
        public short? CodProvincia { get; set; }
        public string Tlfno1 { get; set; }
        public string Tlfno2 { get; set; }
        public string Email { get; set; }
        public string NregEmpresa { get; set; }
        public string CodigoIne { get; set; }
        public decimal? Ivaentradas { get; set; }
        public byte? NumSalas { get; set; }
        public string LineasMetro { get; set; }
        public string LineasBus { get; set; }
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
        public string Ip { get; set; }
        public string NombreBd { get; set; }
        public bool? Activado { get; set; }
        public bool? ExportarIcaa { get; set; }
        public bool? ExportarIcaag { get; set; }
        public bool? Venta { get; set; }
        public bool? Visualizacion { get; set; }
        public string Banco { get; set; }
        public string PalabraClave { get; set; }
        public string Desofuscar { get; set; }
        public string CodTerminal { get; set; }
        public string CodComercio { get; set; }
        public string LogoCine { get; set; }
        public string Gps { get; set; }
        public short? Orden { get; set; }
        public string Parking { get; set; }
        public bool? AccesoMinusvalido { get; set; }
        public bool? ButacaMinusvalido { get; set; }
        public string Caracteristica1 { get; set; }
        public string Caracteristica2 { get; set; }
        public string Caracteristica3 { get; set; }
        public string Caracteristica4 { get; set; }
        public string Caracteristica5 { get; set; }
        public string Caracteristica6 { get; set; }
        public string Caracteristica7 { get; set; }
        public string Caracteristica8 { get; set; }
        public string Icono1 { get; set; }
        public string Icono2 { get; set; }
        public string Icono3 { get; set; }
        public string Icono4 { get; set; }
        public string Icono5 { get; set; }
        public string Icono6 { get; set; }
        public string Icono7 { get; set; }
        public string Icono8 { get; set; }
        public string Icono9 { get; set; }
        public string CondicionesRecogida { get; set; }
        public bool? VentaAmbigu { get; set; }
        public decimal? ImporteMinimoPorOperacion { get; set; }
        public short TiempoFinSesion { get; set; }
        public short? TiempoFinVentaRemota { get; set; }
        public short? TiempoFinReserva { get; set; }
        public string DiaEspectador { get; set; }
        public bool? ImprimeReservas { get; set; }
        public string Lengua { get; set; }
        public string FechaActualizacion { get; set; }
        public string Codigo { get; set; }
        public decimal? PorcentajeImpuestosFacturacion { get; set; }
        public int? ModificadoPor { get; set; }
        public string LogoImpresion { get; set; }
        public string EmpresaBar { get; set; }
        public string Cifbar { get; set; }
        public string DireccionEmpresaBar { get; set; }
        public string LocalidadEmpresaBar { get; set; }
        public string ProvinciaEmpresaBar { get; set; }
        public string CpempresaBar { get; set; }
        public byte? Legacy { get; set; }
        public string TextoPieTicket { get; set; }
        public string WarningMsg { get; set; }
    }
}
