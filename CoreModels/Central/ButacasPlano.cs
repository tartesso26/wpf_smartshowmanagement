﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class ButacasPlano
    {
        public long IdButacaPlano { get; set; }
        public int? IdZonaSala { get; set; }
        public int? FilaMatriz { get; set; }
        public int? ColumnaMatriz { get; set; }
        public string Fila { get; set; }
        public string Butaca { get; set; }
        public string TipoButaca { get; set; }
        public bool? Vendible { get; set; }
        public byte? Estado { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
