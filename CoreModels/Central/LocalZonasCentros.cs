﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class LocalZonasCentros
    {
        public int Id { get; set; }
        public int? IdZonaCentro { get; set; }
        public int? IdCentro { get; set; }
        public string Nombre { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
        public short? TipoMovForSync { get; set; }
    }
}
