﻿using System;
using System.Collections.Generic;

namespace WPF_SmartShowManagement.DataModel
{
    public partial class ArticulosEscandallos
    {
        public int IdArticuloEscandallo { get; set; }
        public int? IdArticuloPadre { get; set; }
        public decimal? Cantidad { get; set; }
        public int? IdArticuloDescontar { get; set; }
        public string FechaActualizacion { get; set; }
        public int? ModificadoPor { get; set; }
    }
}
