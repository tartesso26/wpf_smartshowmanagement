﻿using SSM.Domain.Models;
using System;
using System.Collections.Generic;

namespace SSM.Domain.Models.Central
{
    public class Permission : DomainObject
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
