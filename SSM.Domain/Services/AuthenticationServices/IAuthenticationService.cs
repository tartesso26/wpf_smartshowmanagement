﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSM.Domain.Models.Central;

namespace SSM.Domain.Services.AuthenticationServices
{
    public interface IAuthenticationService
    {
        //Task<bool> Register(string Login, string Password, string RePassword);
        Task<Usuario> Login(string login, string password);
    }
}
