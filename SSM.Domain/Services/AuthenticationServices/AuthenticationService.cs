﻿using SSM.Domain.Exceptions;
using SSM.Domain.Models.Central;
using SSM.Domain.Services.CentralServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BC = BCrypt.Net.BCrypt;

namespace SSM.Domain.Services.AuthenticationServices
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUsuarioService _usuarioService;

        public AuthenticationService(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        public async Task<Usuario> Login(string login, string password)
        {
            Usuario storedUser = await _usuarioService.GetByLogin(login);

            if (storedUser == null)
            {
                throw new UserNotFoundException(login);
            }

            bool passwordResult = BC.Verify(password, storedUser.Password);
            
            if (!passwordResult)
            {
                throw new InvalidPasswordException(login, password);
            }

            return storedUser;
        }
    }
}
