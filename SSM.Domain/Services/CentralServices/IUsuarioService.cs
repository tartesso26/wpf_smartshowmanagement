﻿using SSM.Domain.Models.Central;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSM.Domain.Services.CentralServices
{
    public interface IUsuarioService : IDataService<Usuario>
    {
        Task<Usuario> GetByLogin(string login);

        Task<Usuario> GetByEmail(string email);
    }
}
