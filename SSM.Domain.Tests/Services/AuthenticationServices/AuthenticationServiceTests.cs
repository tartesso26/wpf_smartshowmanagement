﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using SSM.Domain.Services.AuthenticationServices;
using Moq;
using SSM.Domain.Services.CentralServices;
using SSM.Domain.Models.Central;
using System.Threading.Tasks;
using BC = BCrypt.Net.BCrypt;
using SSM.Domain.Exceptions;

namespace SSM.Domain.Tests.Services.AuthenticationServices
{
    [TestFixture]
    public class AuthenticationServiceTests
    {
        private Mock<IUsuarioService> _mockUsuarioService;
        private AuthenticationService _authenticationService;

        [SetUp]
        public void SetUp()
        {
            _mockUsuarioService = new Mock<IUsuarioService>();
            _authenticationService = new AuthenticationService(_mockUsuarioService.Object);
        }

        [Test]
        public async Task Login_WithCorrectPasswordForExistingLogin_ReturnsUsuarioForCorrectLogin()
        {
            // Arrange
            string expectedLogin = "admin";
            string password = "0182";
            string passwordHashed = "$2a$12$MY58TiK/ltpPXHJXQKirouRQ4IQ3TMGNTnf0P3NckVUSgqAzWRMNq";

            _mockUsuarioService.Setup(s => s.GetByLogin(expectedLogin)).ReturnsAsync(new Usuario() { Login = expectedLogin, Password = passwordHashed });

            // Act
            Usuario usuario = await _authenticationService.Login(expectedLogin, password);

            // Assert
            string actualLogin = usuario.Login;
            Assert.AreEqual(expectedLogin, actualLogin);
        }

        [Test]
        public void Login_WithInCorrectPasswordForExistingLogin_ThrowsInvalidPasswordExceptionForLogin()
        {
            // Arrange
            string expectedLogin = "admin";
            string password = "0182";
            string passwordHashed = "passwordnotmatch";

            _mockUsuarioService.Setup(s => s.GetByLogin(expectedLogin)).ReturnsAsync(new Usuario() { Login = expectedLogin, Password = passwordHashed });

            // Act
            InvalidPasswordException exception = Assert.ThrowsAsync<InvalidPasswordException>(() => _authenticationService.Login(expectedLogin, password));

            // Assert
            string actualLogin = exception.Login;
            Assert.AreEqual(expectedLogin, actualLogin);
        }

        [Test]
        public void Login_WithNonExistingLogin_ThrowsInvalidPasswordExceptionForLogin()
        {
            // Arrange
            string expectedLogin = "admin";
            string password = "0182";

            //without setup means usuario returned is null

            // Act
            UserNotFoundException exception = Assert.ThrowsAsync<UserNotFoundException>(() => _authenticationService.Login(expectedLogin, password));

            // Assert
            string actualLogin = exception.Login;
            Assert.AreEqual(expectedLogin, actualLogin);
        }


    }
}
