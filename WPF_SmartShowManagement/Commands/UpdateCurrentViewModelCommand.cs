﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WPF_SmartShowManagement.State.Navigators;
using WPF_SmartShowManagement.State.Windows;
using WPF_SmartShowManagement.ViewModels;
using WPF_SmartShowManagement.ViewModels.Factories;

namespace WPF_SmartShowManagement.Commands
{
    public class UpdateCurrentViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private readonly INavigator _navigator;
        private readonly IRootSSMViewModelFactory _viewModelFactory;

        public UpdateCurrentViewModelCommand(INavigator navigator, IRootSSMViewModelFactory viewModelFactory)
        {
            _navigator = navigator;
            _viewModelFactory = viewModelFactory;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is MainViewType)
            {
                MainViewType viewType = (MainViewType)parameter;

                _navigator.CurrentViewModel = _viewModelFactory.CreateViewModel(viewType);

                /* if (viewType == MainViewType.Login)
                {
                    IWindowService windowService = new WindowNavService();
                    windowService.CreateWindow(_navigator.CurrentViewModel);
                } */
            }
        }



    }
}
