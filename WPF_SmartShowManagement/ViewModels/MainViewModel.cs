﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_SmartShowManagement.State.Navigators;

namespace WPF_SmartShowManagement.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public INavigator Navigator { get; set; }

        public MainViewModel(INavigator navigator)
        {
            Navigator = navigator;
            
            Navigator.UpdateCurrentViewModelCommand.Execute(MainViewType.TicketOffice); //MainViewType.Login
        }
    }
}
