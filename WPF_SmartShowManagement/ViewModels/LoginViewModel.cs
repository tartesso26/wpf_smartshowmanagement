﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WPF_SmartShowManagement.Commands;
using WPF_SmartShowManagement.State.Authenticators;

namespace WPF_SmartShowManagement.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private string _login = "admin";

        public string Login
        {
            get
            {
                return _login;
            }
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        public MessageViewModel ErrorMessageViewModel { get; }

        public string ErrorMessage
        {
            set => ErrorMessageViewModel.Message = value;
        }

        public ICommand LoginCommand { get; }
        //ICommand ConfigCommand { get; }

        public LoginViewModel(IAuthenticator authenticator)
        {
            ErrorMessageViewModel = new MessageViewModel();

            LoginCommand = new LoginCommand(this, authenticator);
            //ConfigCommand = new ConfigCommand();
        }
    }
}
