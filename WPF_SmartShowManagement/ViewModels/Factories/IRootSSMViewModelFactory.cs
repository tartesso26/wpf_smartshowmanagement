﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_SmartShowManagement.State.Navigators;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public interface IRootSSMViewModelFactory
    {
        ViewModelBase CreateViewModel(MainViewType mainViewType);
    }
}
