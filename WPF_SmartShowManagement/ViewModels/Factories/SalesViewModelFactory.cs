﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class SalesViewModelFactory : ISSMViewModelFactory<MainSalesViewModel>
    {
        public MainSalesViewModel CreateViewModel()
        {
            return new MainSalesViewModel();
        }
    }
}
