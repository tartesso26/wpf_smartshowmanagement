﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class BarViewModelFactory : ISSMViewModelFactory<MainBarViewModel>
    {
        public MainBarViewModel CreateViewModel()
        {
            return new MainBarViewModel();
        }
    }
}
