﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_SmartShowManagement.State.Navigators;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class RootSSMViewModelFactory : IRootSSMViewModelFactory
    {
        private readonly ISSMViewModelFactory<MainResumeViewModel> _mainResumeViewModelFactory;
        private readonly ISSMViewModelFactory<MainTicketOfficeViewModel> _mainTicketOfficeViewModelFactory;
        private readonly ISSMViewModelFactory<MainControlViewModel> _mainControlViewModelFactory;
        private readonly ISSMViewModelFactory<MainBarViewModel> _mainBarViewModelFactory;
        private readonly ISSMViewModelFactory<MainSalesViewModel> _mainSalesViewModelFactory;

        private readonly ISSMViewModelFactory<LoginViewModel> _loginViewModelFactory;

        public RootSSMViewModelFactory(ISSMViewModelFactory<MainResumeViewModel> mainResumeViewModelFactory, 
            ISSMViewModelFactory<MainTicketOfficeViewModel> mainTicketOfficeViewModelFactory, 
            ISSMViewModelFactory<MainControlViewModel> mainControlViewModelFactory, 
            ISSMViewModelFactory<MainBarViewModel> mainBarViewModelFactory, 
            ISSMViewModelFactory<MainSalesViewModel> mainSalesViewModelFactory,
            ISSMViewModelFactory<LoginViewModel> loginViewModelFactory)
        {
            _mainResumeViewModelFactory = mainResumeViewModelFactory;
            _mainTicketOfficeViewModelFactory = mainTicketOfficeViewModelFactory;
            _mainControlViewModelFactory = mainControlViewModelFactory;
            _mainBarViewModelFactory = mainBarViewModelFactory;
            _mainSalesViewModelFactory = mainSalesViewModelFactory;

            _loginViewModelFactory = loginViewModelFactory;
        }

        public ViewModelBase CreateViewModel(MainViewType mainViewType)
        {
            switch (mainViewType)
            {
                case MainViewType.Resume:
                    return _mainResumeViewModelFactory.CreateViewModel();
                case MainViewType.TicketOffice:
                    return _mainTicketOfficeViewModelFactory.CreateViewModel();
                case MainViewType.Control:
                    return _mainControlViewModelFactory.CreateViewModel();
                case MainViewType.Bar:
                    return _mainBarViewModelFactory.CreateViewModel();
                case MainViewType.Sales:
                    return _mainSalesViewModelFactory.CreateViewModel();

                /* case MainViewType.Login:
                    return _loginViewModelFactory.CreateViewModel(); */

                default:
                    throw new ArgumentException("MainViewType value does not exist as a ViewModel", "MainViewType");
            }
        }
    }
}
