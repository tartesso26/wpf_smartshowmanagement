﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public interface ISSMViewModelFactory<T> where T : ViewModelBase
    {
        T CreateViewModel();
    }
}
