﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_SmartShowManagement.State.Authenticators;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class LoginViewModelFactory : ISSMViewModelFactory<LoginViewModel>
    {
        private readonly IAuthenticator _authenticator;

        public LoginViewModelFactory(IAuthenticator authenticator)
        {
            _authenticator = authenticator;
        }

        public LoginViewModel CreateViewModel()
        {
            return new LoginViewModel(_authenticator);
        }
    }
}
