﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class TicketOfficeViewModelFactory : ISSMViewModelFactory<MainTicketOfficeViewModel>
    {
        public MainTicketOfficeViewModel CreateViewModel()
        {
            return new MainTicketOfficeViewModel();
        }
    }
}
