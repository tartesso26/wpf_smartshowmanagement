﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class ControlViewModelFactory : ISSMViewModelFactory<MainControlViewModel>
    {
        public MainControlViewModel CreateViewModel()
        {
            return new MainControlViewModel();
        }
    }
}
