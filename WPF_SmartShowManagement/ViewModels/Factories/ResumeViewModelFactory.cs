﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_SmartShowManagement.ViewModels.Factories
{
    public class ResumeViewModelFactory : ISSMViewModelFactory<MainResumeViewModel>
    {
        public MainResumeViewModel CreateViewModel()
        {
            return new MainResumeViewModel();
        }
    }
}
