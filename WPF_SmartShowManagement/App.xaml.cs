﻿using Microsoft.Extensions.DependencyInjection;
using SSM.Domain.Services.AuthenticationServices;
using SSM.Domain.Services.CentralServices;
using SSM.EntityFramework;
using SSM.EntityFramework.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WPF_SmartShowManagement.State.Authenticators;
using WPF_SmartShowManagement.State.Navigators;
using WPF_SmartShowManagement.ViewModels;
using WPF_SmartShowManagement.ViewModels.Factories;
using WPF_SmartShowManagement.Views;

namespace WPF_SmartShowManagement
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            IServiceProvider serviceProvider = CreateServiceProvider();

            Window window = serviceProvider.GetRequiredService<LoginView>(); //serviceProvider.GetRequiredService<MainWindow>();
            window.DataContext = serviceProvider.GetRequiredService<LoginViewModel>();
            window.Show();

            base.OnStartup(e);
        }

        private IServiceProvider CreateServiceProvider()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddSingleton<CentralDbContextFactory>();
            services.AddSingleton<IAuthenticationService, AuthenticationService>();
            services.AddSingleton<IUsuarioService, UsuarioDataService>();

            services.AddSingleton<IRootSSMViewModelFactory, RootSSMViewModelFactory>();
            services.AddSingleton<ISSMViewModelFactory<MainResumeViewModel>, ResumeViewModelFactory>();
            services.AddSingleton<ISSMViewModelFactory<MainTicketOfficeViewModel>, TicketOfficeViewModelFactory>();
            services.AddSingleton<ISSMViewModelFactory<MainControlViewModel>, ControlViewModelFactory>();
            services.AddSingleton<ISSMViewModelFactory<MainBarViewModel>, BarViewModelFactory>();
            services.AddSingleton<ISSMViewModelFactory<MainSalesViewModel>, SalesViewModelFactory>();
            services.AddSingleton<ISSMViewModelFactory<LoginViewModel>, LoginViewModelFactory>();

            services.AddScoped<INavigator, Navigator>();
            services.AddScoped<IAuthenticator, Authenticator>();
            services.AddScoped<MainViewModel>();
            services.AddScoped<LoginViewModel>();

            //services.AddScoped<MainWindow>(s => new MainWindow(s.GetRequiredService<MainViewModel>()));
            services.AddScoped<LoginView>();

            return services.BuildServiceProvider();
        }
        
    }
}
