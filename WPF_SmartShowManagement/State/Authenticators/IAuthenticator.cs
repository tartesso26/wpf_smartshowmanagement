﻿using SSM.Domain.Models.Central;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WPF_SmartShowManagement.State.Authenticators
{
    public interface IAuthenticator
    {
        Usuario CurrentUser { get; }
        bool IsLoggedIn { get; }

        Task<bool> Login(string login, string password);
        void Logout();
    }
}
