﻿using SSM.Domain.Models.Central;
using SSM.Domain.Services.AuthenticationServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WPF_SmartShowManagement.State.Authenticators
{
    public class Authenticator : IAuthenticator
    {
        private readonly IAuthenticationService _authenticationService;

        public Authenticator()
        {
        }

        public Authenticator(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        public Usuario CurrentUser { get; private set; }

        public bool IsLoggedIn => CurrentUser != null;

        public async Task<bool> Login(string login, string password)
        {
            bool success = false;

            try
            {
                CurrentUser = await _authenticationService.Login(login, password);
                success = true;
            }
            catch (Exception)
            {

                success = false;
            }
            return success;
        }

        public void Logout()
        {
            CurrentUser = null;
        }
    }
}
