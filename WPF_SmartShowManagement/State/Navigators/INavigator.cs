﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using WPF_SmartShowManagement.ViewModels;

namespace WPF_SmartShowManagement.State.Navigators
{
    public enum MainViewType
    {
        //main window sections
        Resume,
        Base,
        TicketOffice,
        Control,
        Bar,
        Sales

        //login
        //-Login
    }
    public interface INavigator
    {
        ViewModelBase CurrentViewModel { get; set; }
        ICommand UpdateCurrentViewModelCommand { get; }
    }
}
