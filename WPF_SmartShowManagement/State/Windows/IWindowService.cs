﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_SmartShowManagement.ViewModels;

namespace WPF_SmartShowManagement.State.Windows
{
    public enum WindowsType
    {
        //windows able to load
        Login,
        Main

    }

    public interface IWindowService
    {
        void CreateWindow(ViewModelBase viewModel, WindowsType window);

    }
}
