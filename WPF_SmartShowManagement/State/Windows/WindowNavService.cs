﻿using System;
using System.Collections.Generic;
using System.Text;
using WPF_SmartShowManagement.State.Authenticators;
using WPF_SmartShowManagement.ViewModels;
using WPF_SmartShowManagement.Views;

namespace WPF_SmartShowManagement.State.Windows
{
    public class WindowNavService : IWindowService
    {
        public void CreateWindow(ViewModelBase viewModel, WindowsType window)
        {
            switch (window) {
                case WindowsType.Login:
                    LoginView loginView = new LoginView
                    {
                        DataContext = (LoginViewModel)viewModel
                    };
                    loginView.Show();
                    break;
                case WindowsType.Main:
                    MainWindow mainView = new MainWindow((MainViewModel)viewModel);
                    mainView.Show();
                    break;
            }
        }
    }
    
}
