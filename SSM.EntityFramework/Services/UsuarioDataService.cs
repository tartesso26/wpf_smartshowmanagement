﻿using Microsoft.EntityFrameworkCore;
using SSM.Domain.Models.Central;
using SSM.Domain.Services.CentralServices;
using SSM.EntityFramework.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSM.EntityFramework.Services
{
    public class UsuarioDataService : IUsuarioService
    {
        private readonly CentralDbContextFactory _contextFactory;
        private readonly CentralNonQueryDataService<Usuario> _nonQueryDataService;

        public UsuarioDataService(CentralDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
            _nonQueryDataService = new CentralNonQueryDataService<Usuario>(contextFactory);
        }

        public async Task<Usuario> Create(Usuario entity)
        {
            return await _nonQueryDataService.Create(entity);
        }

        public async Task<bool> Delete(int id)
        {
            return await _nonQueryDataService.Delete(id);
        }

        public async Task<Usuario> Get(int id)
        {
            using (CentralDbContext context = _contextFactory.CreateDbContext())
            {
                Usuario entity = await context.Usuarios
                    .Include(a => a.Centros)
                    .Include(a => a.Permissions)
                    .FirstOrDefaultAsync((e) => e.Id == id);

                return entity;
            }
        }

        public async Task<IEnumerable<Usuario>> GetAll()
        {
            using (CentralDbContext context = _contextFactory.CreateDbContext())
            {
                IEnumerable<Usuario> entities = await context.Usuarios
                    .Include(a => a.Centros)
                    .Include(a => a.Permissions)
                    .ToListAsync();

                return entities;
            }
        }

        public async Task<Usuario> GetByEmail(string email)
        {
            using (CentralDbContext context = _contextFactory.CreateDbContext())
            {
                return await context.Usuarios
                    .Include(a => a.Centros)
                    .Include(a => a.Permissions)
                    .FirstOrDefaultAsync(a => a.Email == email);
            }
        }

        public async Task<Usuario> GetByLogin(string login)
        {
            try
            {
                using (CentralDbContext context = _contextFactory.CreateDbContext())
                {
                    Usuario entity = await context.Usuarios
                        .FirstOrDefaultAsync(a => a.Login == login);

                    return entity;
                }
            }catch(Exception ex)
            {
                string a = ex.Message;
                return null;
            }
        }

        public async Task<Usuario> Update(int id, Usuario entity)
        {
            return await _nonQueryDataService.Update(id, entity);
        }
    }
}
