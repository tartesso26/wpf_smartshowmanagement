﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSM.EntityFramework
{
    public class CentralDbContextFactory : IDesignTimeDbContextFactory<CentralDbContext>
    {
        public CentralDbContext CreateDbContext(string[] args = null)
        {
            var options = new DbContextOptionsBuilder<CentralDbContext>();
            options.UseSqlServer("Data Source=82.223.82.121;User ID=sa;Password=G2st4r!0182;Initial Catalog=Centros;Persist Security Info = True;");
            
            return new CentralDbContext(options.Options);
        }
    }
}
