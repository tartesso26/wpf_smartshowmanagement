﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using SSM.Domain.Models.Central;

namespace SSM.EntityFramework
{
    public class CentralDbContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Centro> Centros { get; set; }
        public DbSet<Permission> Permissions { get; set; }

        public CentralDbContext(DbContextOptions options): base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Centro>(entity =>
            {
                /*entity.HasKey(e => e.IdCentro);

                entity.Property(e => e.IdCentro).HasColumnName("ID_Centro");*/

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("ID_Centro");

                entity.Property(e => e.Banco)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica1)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica2)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica3)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica4)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica5)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica6)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica7)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Caracteristica8)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Cif)
                    .HasColumnName("CIF")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Cifbar)
                    .HasColumnName("CIFBar")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Circuito)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodComercio)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CodTerminal)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoIne)
                    .HasColumnName("CodigoINE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CondicionesRecogida).IsUnicode(false);

                entity.Property(e => e.Contrasena)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cpcentro)
                    .HasColumnName("CPCentro")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Cpempresa)
                    .HasColumnName("CPEmpresa")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CpempresaBar)
                    .HasColumnName("CPEmpresaBar")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Desofuscar)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DiaEspectador)
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionEmpresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionEmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Empresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExportarIcaa).HasColumnName("ExportarICAA");

                entity.Property(e => e.ExportarIcaag).HasColumnName("ExportarICAAG");

                /*entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);*/

                entity.Property(e => e.Gps)
                    .HasColumnName("GPS")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Icono1)
                    .HasColumnName("icono1")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Icono2)
                    .HasColumnName("icono2")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Icono3)
                    .HasColumnName("icono3")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Icono4)
                    .HasColumnName("icono4")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Icono5)
                    .HasColumnName("icono5")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Icono6)
                    .HasColumnName("icono6")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Icono7)
                    .HasColumnName("icono7")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Icono8)
                    .HasColumnName("icono8")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Icono9)
                    .HasColumnName("icono9")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ImporteMinimoPorOperacion).HasColumnType("smallmoney");

                entity.Property(e => e.Ip)
                    .HasColumnName("IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ivaentradas)
                    .HasColumnName("IVAEntradas")
                    .HasColumnType("smallmoney");

                entity.Property(e => e.Legacy).HasColumnName("legacy");

                entity.Property(e => e.Lengua)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LineasBus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LineasMetro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadEmpresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalidadEmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogoCine)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogoImpresion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NombreAbreviado)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NombreBd)
                    .HasColumnName("NombreBD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NregEmpresa)
                    .HasColumnName("NRegEmpresa")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PalabraClave)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Parking)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PorcentajeImpuestosFacturacion).HasColumnType("smallmoney");

                entity.Property(e => e.ProvinciaCentro)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinciaEmpresa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinciaEmpresaBar)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tlfno1)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Tlfno2)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("ID_Usuario");

                entity.Property(e => e.CodigoBarras)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Imagen)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PedirCp).HasColumnName("PedirCP");

                entity.Property(e => e.Permisos)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WebPass)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.ToTable("Permission");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("code");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("description");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });


            base.OnModelCreating(modelBuilder);
        }

    }
}
